/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@Authors: Annebelle Almås, Mathias Braathen, William Chakroun Jacobsen & Terje Haugum
@Date submitted: 23.04.2018
@Summary:

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////**/
package test.JUnittSimulator;
import main.objects.Customer;
import main.objects.Docking;
import main.simulator.BikeEmulator;
import main.simulator.SimulatorRunner;
import main.simulator.Trip;
import main.simulator.TripRunner;
import org.junit.*;
import main.objects.Bike;
import static org.junit.Assert.*;
import org.junit.Test;
import java.sql.Date;
import java.util.ArrayList;

/**
 * Class for testing the simulation class in JUnit
 */
public class JUnitSimulatorSimRun {
    private SimulatorRunner simRun = new SimulatorRunner(1);;
    private Trip trip;
    private BikeEmulator bikeEmulator;
    private ArrayList<Docking> docks = new ArrayList<>();
    private ArrayList<Bike> bikes = new ArrayList<>();
    private Docking dock1;
    private Docking dock2;
    private Bike bike1;
    private Bike bike2;
    private Bike bike3;
    private Customer customer;
    private Bike[] bikeTabel;
    private TripRunner tripRunner = new TripRunner();

    /**
     * for creating dock and bike objects used in the test
     */
    @Before
    public void beforeTest(){
        //Make dates, bike1 og bike2 have the same date, bike3 have date3 and date4
        Date date1 = new Date(2018-01-02);
        Date date2 = new Date(2017-01-01);
        Date date3 = new Date(2018-04-01);
        Date date4 = new Date(2010-10-10);
        customer = new Customer("Hanne", "Olsen", 12345678);
        //Make bikes and put them into an array
        bike1 = new Bike(1,date1, 1000, 1,date2);
        bike2 = new Bike(2,date1,500,1,date2);
        bike3 = new Bike(3,date3,200,2,date4);
        bikes.add(bike1);
        bikes.add(bike2);
        bikes.add(bike3);
        //Make dockings and put them in to an array
        dock1 = new Docking(1,"Prinsen Kino",63.427057, 10.392525100000057);
        dock2 = new Docking(2, "Voll studentby", 63.409521, 10.444628999999964);
        docks.add(dock1);
        docks.add(dock2);
        Bike[] tableBike = {bike1,bike2,bike3};
        Docking[] tableDock = {dock1,dock2};
        bikes.get(0).setDocked(1);
        bikes.get(1).setDocked(1);
        bikes.get(2).setDocked(2);
        simRun.setTestArray(bikes,docks);
        bikeEmulator = new BikeEmulator();
    }

    /**
     * clear objects after test
     */
    @After
    public void afterTest(){
        simRun = null;
        docks = null;
        bikes = null;
    }

    /**
     * for testing the starting station
     */
    @Test
    public void testnoe(){
        //assertEquals(1,1);
        Docking d = simRun.findStartStation();
        //dock1 = new Docking(1,"Prinsen Kino",63.427057, 10.392525100000057);
        //dock2 = new Docking(2, "Voll studentby", 63.409521, 10.444628999999964);
        assertNotNull(d);
    }


    /**
     * for testing what bike to get
      */
    @Test
    public void testChooseBike(){
        bike2.setCharge(50);
        Bike theBike = simRun.chooseBike(dock1);
        assertEquals(bike2,theBike);
    }


    /**
     * for testing start dock
     */
    @Test
    public void testBookKeepingStart(){
        simRun.bookKeepingTripStart(dock1, bike1);
        assertEquals(3, bike1.getStatus());
        assertEquals( 1,dock1.getAntDocked());
    }

    /**
     * for testing end dock of trip
     */
    @Test
    public void testBookKeepingEnd(){
        simRun.bookKeepingTripStart(dock1, bike1);
        trip = new Trip(simRun, dock1, dock2, customer, bike1);
        tripRunner.addTrip(trip);
        simRun.bookKeepingTripEnd(trip);

        //The bike can be put to repair or in the dock when the trip is done the value can be 1 or 4.
        if(trip.getBike().getStatus() == 1 ) {
            assertEquals(1, trip.getBike().getStatus());
        }
        else{
            assertEquals(4, trip.getBike().getStatus());
        }


    }

    /**
     * testing the decharging function when a bike ios running
     */
    @Test
    public void testDeChargeBike(){
        bike1.setCharge(80);
        simRun.deChargeBikeRun(bike1);
        assertEquals(79, bike1.getCharge());
    }


    /**
     * testing the km calc when a bike is running
     */
    @Test
    public void testkmToPoints(){
        int km = bikeEmulator.kmToPoints( 60.17513558580817, 10.331689932453022,63.44130615565537, 10.22869809123346);
        assertEquals(364,km,2);
    }


    @Test
    public void test(){
    }

    public static void main(String[]args){
        org.junit.runner.JUnitCore.main(JUnitSimulatorSimRun.class.getName());
    }
}
