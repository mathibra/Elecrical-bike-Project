package test.JUnitDatabase;

import main.database.DatabasePool;
import main.database.Executor;
import main.objects.Bike;
import main.objects.Customer;
import main.objects.Docking;
import main.objects.Type;

import org.junit.*;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

/**
 * Class for testing the database
 * OBS! OBS! OBS!
 * Need to run each test individually. This is because the connection with the DB might be slow.
 *
 */
public class DBtest {
    private static Test_data test_data_creator;
    private static ArrayList<Type> types;
    private static ArrayList<Bike> bikes;
    private static ArrayList<Docking> docks;
    private static ArrayList<Customer> customers;

    /**
     * Method for starting the test of the database
     */
    @BeforeClass
     public static void startingTest(){
        test_data_creator = new Test_data();
        test_data_creator.dropTable();
        test_data_creator.createDB();
        System.out.println("--------------------------------------Create objects--------------------------------------");
        types = new ArrayList<>(test_data_creator.getTypes());
        test_data_creator.insertTypes(types);
        docks = new ArrayList<>(test_data_creator.getDockings());
        test_data_creator.insertDocking(docks);
        bikes = new ArrayList<>(test_data_creator.getBikes());
        test_data_creator.insertBikes(bikes);
        customers = new ArrayList<>(test_data_creator.getCustomers());
        test_data_creator.insertCustomers(customers);
        System.out.println("--------------------------------------ALL ARRAYS CREATED--------------------------------------\n--------------------------------------ALL OBJECTS IN test_DB--------------------------------------");
        System.out.println(Executor.getAllBikes());
        System.out.println(Executor.getAllTypes());
        System.out.println(Executor.getAllDockings());
        System.out.println(Executor.getallCustomers());
        Executor.newAdmin("wil@h.b");
        Executor.changePassword("wil@h.b","12345");
    }

    /**
     * Method for indicating the start of a test
     */
    @Before
    public void setUp(){
        System.out.println("--------------------------------------NEW TEST--------------------------------------");
    }

    /**
     * Method for indicating the end of a test
     */
    @After
    public void tearDown(){
        System.out.println("--------------------------------------TEAR DOWN--------------------------------------");
    }

    /**
     * Method for testing a customer
     */
    @Test
    public void Customer_tester(){
        System.out.println("Testing customer connections and methods");
        assertEquals(true,Executor.newCustomer("firstname","surname",1234556));
        assertEquals(customers.size(),Executor.getallCustomers().size());
        assertEquals(1,Executor.getCustomerID(customers.get(1)));
    }

    /**
     * Method for testing bike operations for a dock
     */
    @Test
    public void dockingstation_bikes_connection(){
        System.out.println("Testing collaboration between bikes and Dockingstations");
        System.out.println("Adding bike 2 in Dock 1");
        assertEquals(true,Executor.dockingBike(2,1));
        assertEquals(1,Executor.getBikesFromDock(1).size());
        assertEquals(1,Executor.retriveDock(2));
        System.out.println("Undocking bike... ");
        assertEquals(true,Executor.unDockBike(1,1,1,Executor.dateNow()));
    }

    /**
     * This checks all the type methods in executor.
     *
     * this takes the first object in the ArrayList of types and the first id in the Database.
     */
    @Test
    public void Type_testing(){
        System.out.println("Testing id_to_name... ");
        assertEquals(types.get(0).getName(),Executor.idtoName(1));
        assertEquals(Executor.getAllTypes().get(0).getType_id(),Executor.nametoID(Executor.getAllTypes().get(0).getName()));
        System.out.println("Creating and checking types... ");
        assertEquals(true,Executor.newType("newType"));
        assertEquals(false,Executor.editType("Nonexistant","wont_work"));
        assertEquals(true,Executor.editType("newType","but_i_will"));
        assertEquals(false,Executor.deleteType("Nonexistant"));
       // assertEquals(true,Executor.deleteType("but_i_will"));
        }
    /**
     * This checks if the correct input is there and if there is an other admin with that same username.
     * Line 177, in Class: Executor
     * It inputs a Email and checks if it is legit and if it is used before.
     */
    @Test
    public void Admin_Testing(){
        System.out.println("Testing Create Admin... ");
        assertEquals(true,Executor.newAdmin("willia2210@hotmail.com"));
        assertEquals(false,Executor.newAdmin("willia2210@hotmail.com"));
        assertEquals(false,Executor.newAdmin("willia2210hotmail.no"));
        assertEquals(false,Executor.newAdmin("redbull@Gmail"));
        System.out.println("Testing New Password... ");
        assertEquals(true,Executor.newPassword("willia2210@hotmail.com"));
        assertEquals(false,Executor.newPassword("willia2210hotmail.com"));
        assertEquals(false,Executor.newPassword("willia2210@hotmailno"));
        assertEquals(false,Executor.newPassword("willia2210@hotmail.no"));
        System.out.println("Testing change password... ");
        assertEquals(true,Executor.changePassword("willia2210@hotmail.com","bestPassword"));
        assertEquals(false,Executor.changePassword("willia2210hotmail.com","false"));
        System.out.println("Testing Loggin");
        assertEquals(true,Executor.loginA("wil@h.b","12345"));
        assertEquals(false,Executor.loginA("willia2210@hotmail.com","NotAgoodpassword"));
        }

    /**
     * Method for testing bike operations
     */
    @Test
        public void bike_testing(){
        System.out.println("Testing Bike methods... ");
        assertEquals(bikes.size(),Executor.getAllBikes().size());
        System.out.println("Edit bikes... ");
        assertEquals(true,Executor.editBike(1,1,Executor.dateNow(),10,10,100,1,3));
        assertEquals(true,Executor.editBike(2,1,Executor.dateNow(),10,10,100,1,3));
        System.out.println("Done editing!");
        }
        @Test
        public void repair_bike(){
            assertEquals(true,Executor.addRepairBike(3,Executor.dateNow(),"it is broken!"));
            assertEquals(true,Executor.doneRepair(1,Executor.dateNow(),"A bolt were loose",2));
            System.out.println("The bike is in storage!\nWAIT! it broke!");
            assertEquals(true, Executor.brokenBike(1));
            //assertEquals(4,Executor.getSpesificBike(1).getStatus());
        }

    /**
     * Method for testing dock operations
     */
    @Test
        public void dockstation_testing(){
            System.out.println("Testing Dockingstation methods... ");
            assertEquals(docks.size(),Executor.getAllDockings().size());
            assertEquals(true, Executor.newDocking("New Dockingstation",63.1,10.1));
            assertEquals(false,Executor.newDocking("New Dockingstation",63.1,10.1));
            assertEquals(true,Executor.deleteDocking(1));
            assertEquals(true,Executor.editDocking(5,"this_name",1,1));
            assertEquals("New Dockingstation", Executor.returnDockObj("New Dockingstation").getName());
        }

    /**
     * Method for testing operations concerning the connection between a customer and a bike
     */
    @Test
        public void bike_customer_relations(){
        System.out.println("Checking if a customer rents a spesific bike!");
        assertEquals(true,Executor.unDockBike(2,3,1,Executor.dateNow()));
        assertEquals(true,Executor.relationbetweencustandBike(3,2));
        assertEquals(false,Executor.relationbetweencustandBike(1,3));
        }


    /**
     * method to indicate a finished test
     */
    @AfterClass
    public static void destroy_tables(){
        System.out.println("--------------------------------------TEST COMPLETE--------------------------------------");
    }


    public static void main(String[]args){
        org.junit.runner.JUnitCore.main(DBtest.class.getName());
    }
}
