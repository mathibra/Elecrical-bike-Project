package test.JUnitDatabase;

import main.database.DatabasePool;

import main.database.Executor;
import main.objects.Bike;
import main.objects.Customer;
import main.objects.Docking;
import main.objects.Type;

import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
/**
 * Initiator inisiates the database and testing of the Executor class.
 * */
public class Initiator {

    private static DatabasePool dbp;
    private Statement stm;
    private SQL_query sql = new SQL_query();
    /**
     * The Constructor opens the test database and connects with it.
     * */
    protected Initiator() {
        try{
            System.out.println("Starting DB Testing");
            DatabasePool.istest = true;
            dbp = Executor.getDbp();
            System.out.println("Connected database successfully... ");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Method for creating DB
     * @see test.JUnitDatabase.SQL_query
     */
    public void createDB(){
        Connection con = dbp.take();
        try{
            System.out.println("Creating tables... ");
            con.setAutoCommit(false);
            stm = con.createStatement();
            stm.executeUpdate(sql.createTypeTable());
            stm.executeUpdate(sql.createBikeTable());
            stm.executeUpdate(sql.createDockingTable());
            stm.executeUpdate(sql.createRepairsTable());
            stm.executeUpdate(sql.createCustomerTable());
            stm.executeUpdate(sql.createUser_has_bikeTable());
            stm.executeUpdate(sql.createAdminTable());
            stm.executeUpdate(sql.createBike_status());
            stm.executeUpdate(sql.createBike_Docked());
            stm.executeUpdate(sql.createDockingStationHistory());
            //Executor.newAdmin("whore@s.3");
            con.commit();
            stm.close();
            dbp.give(con);
            System.out.println("Creation complete!");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Method for inserting types into the test DB
     * @param types     an arraylist of types to insert
     */
    protected void insertTypes(ArrayList<Type> types){
        Connection con = dbp.take();
        try{
            System.out.println("Inserting to Types... ");
            con.setAutoCommit(false);
            stm = con.createStatement();
            for(Type type: types){
                stm.addBatch(sql.createType(type.getName()));
            }
            stm.executeBatch();
            con.commit();
            System.out.println("Insertion of types complete!");
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            dbp.give(con);
        }
    }

    /**
     * Method for inserting docks into the test DB
     * @param docks     an arraylist of docks to insert
     */
    protected void insertDocking(ArrayList<Docking> docks){ ;
        Connection con = dbp.take();
        try{
            System.out.println("Inserting to Docking... ");
            con.setAutoCommit(false);
            stm = con.createStatement();
            for(Docking dock: docks){
                stm.addBatch(sql.createDocking(dock.getName(),dock.getLatitude(),dock.getLatitude()));
            }
            stm.executeBatch();
            con.commit();
            System.out.println("Insertion of docks complete!");
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            dbp.give(con);
        }
    }

    /**
     * Method for inserting bikes into the test DB
     * @param bikes     an arraylist of bikes to insert
     */
    protected void insertBikes(ArrayList<Bike> bikes){
        Connection con = dbp.take();
        try{
            System.out.println("Inserting to Bike... ");
            con.setAutoCommit(false);
            stm = con.createStatement();
            for(Bike bike: bikes){
                stm.addBatch(sql.createBike(bike.getReg_date(),bike.getPrice(),bike.getCharge(),bike.getProd_date(),bike.getAnt_rep(),bike.getTot_trips(),bike.getTot_km()));
            }
            stm.executeBatch();
            con.commit();
            System.out.println("Insertion of bikes complete!");
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            dbp.give(con);
        }
    }

    /**
     * Method for inserting customers into the test DB
     * @param custumers     an arraylist of customers to insert
     */
    protected void insertCustomers(ArrayList<Customer> custumers){
        System.out.println("inserting Bikes");
        Connection con = dbp.take();
        try{
            System.out.println("Inserting to customers... ");
            con.setAutoCommit(false);
            stm = con.createStatement();
            for(int i = 1; i<custumers.size(); i++){
                stm.addBatch(sql.createCustomer(custumers.get(i).getFirstname(),custumers.get(i).getSurname(),custumers.get(i).getCardNr()));
            }
            stm.executeBatch();
            con.commit();
            dbp.give(con);
            System.out.println("Insertion of customers complete!");
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            dbp.give(con);
        }
    }

    /**
     * Method used for dropping the created test table
     */
    protected void dropTable(){
        Connection con = dbp.take();
        try{
            System.out.println("Dropping Tables... ");
            stm = con.createStatement();
            stm.executeUpdate(sql.dropBannedCustomers());
            stm.executeUpdate(sql.dropHistory());
            stm.executeUpdate(sql.dropBikeStat());
            stm.executeUpdate(sql.dropBike_docked());
            stm.executeUpdate(sql.dropAdmin());
            stm.executeUpdate(sql.dropUser_has());
            stm.executeUpdate(sql.dropRepair());
            stm.executeUpdate(sql.dropCustomer());
            stm.executeUpdate(sql.dropBike());
            stm.executeUpdate(sql.dropType());
            stm.executeUpdate(sql.dropDocking());
            System.out.println("Dropping complete!");
        }catch (Exception e){
            e.printStackTrace();
        }
        finally {
            dbp.give(con);
        }
    }
}
