package test.JUnitDatabase;

import main.database.Executor;
import main.objects.*;
import main.simulator.ObjectCreators;
import main.simulator.RandomGen;

import java.util.ArrayList;

/**
 * Class for creating test data for the JUnit testing in the DB
 */
public class Test_data extends Initiator{
    private ObjectCreators objectCreators = new ObjectCreators();
    private ArrayList<Type> types = new ArrayList<>(objectCreators.createType());
    private ArrayList<Bike> bikes = new ArrayList<>();
    private ArrayList<Docking> docks = new ArrayList<>(objectCreators.createDocks());
    private ArrayList<Customer> customers = new ArrayList<>(objectCreators.createCust());

    /**
     *This method creates all the Bike objects for the test.
     * */
    private void createBikes(){
        for(int i = 0; i<100; i++){
            bikes.add(new Bike(Executor.dateNow(), RandomGen.priceGen(),1, Executor.dateNow()));
        }
    }
    /**
     * This method returns all the bikes created.
     *  @return an array of bike objects.
     * */
    protected ArrayList<Bike> getBikes(){
        createBikes();
        return bikes;
    }
    /**
     * This method returns all the types created.
     *  @return an array of Type objects.
     * */
    protected ArrayList<Type> getTypes() {
        types.add(new Type("Original"));
        types.add(new Type("newBike"));
        return types;
    }
    /**
     * This method returns all the docks created.
     *  @return an array of Docking objects.
     * */
    protected ArrayList<Docking> getDockings() {
        return docks;
    }
    /**
     * This method returns all the customers created.
     *  @return an array of Customer objects.
     * */
    protected ArrayList<Customer> getCustomers() {
        return customers;
    }
}
