package test.JUnitDatabase;

import java.sql.Date;

/**
 * Class for creating SQL queries
 */
public class SQL_query {

    public SQL_query(){}

    /**
     * Method for getting the queries to be executed
     * @return      a String containing the desired SQL queries
     */
    public String createBikeTable(){
        return "CREATE TABLE Bike(\n" +
                "  bike_id INT NOT NULL AUTO_INCREMENT,\n" +
                "  date_registered DATE,\n" +
                "  price INT,\n" +
                "  charge INT,\n" +
                "  produced DATE,\n" +
                "  all_repairs INT,\n" +
                "  total_trips INT,\n" +
                "  total_km INT,\n" +
                "  type_id INT NOT NULL,\n" +
                "  status INT NOT NULL,\n" +
                "  PRIMARY KEY(bike_id),\n" +
                "  FOREIGN KEY (type_id) REFERENCES Type(type_id));\n";
    }

    /**
     * Method for getting the query for creating a type table
     * @return      a String for creating type table in DB
     */
    public String createTypeTable(){
        return "CREATE TABLE Type(\n" +
                "  type_id INT NOT NULL AUTO_INCREMENT,\n" +
                "  name VARCHAR(30) UNIQUE,\n" +
                "  status INT NOT NULL,\n" +
                "  CONSTRAINT type_pk PRIMARY KEY(type_id)\n" +
                ");";
    }

    /**
     * Method for getting the query for creating a dock table
     * @return      a String for creating dock table in DB
     */
    public String createDockingTable(){
        return "CREATE TABLE Docking_station(\n" +
                "  dock_id INT NOT NULL AUTO_INCREMENT,\n" +
                "  name VARCHAR(30),\n" +
                "  latitude DOUBLE,\n" +
                "  longitude DOUBLE,\n" +
                "  power_usage DOUBLE,\n" +
                "  status INT NOT NULL,\n" +
                "  CONSTRAINT docking_station_pk PRIMARY KEY(dock_id));";
    }

    /**
     * Method for getting the query for creating a repair table
     * @return      a String for creating repair table in DB
     */
    public String createRepairsTable(){
        return "CREATE TABLE Repairs(\n" +
                "  repair_id INT NOT NULL AUTO_INCREMENT,\n" +
                "  repair_request VARCHAR(1000),\n" +
                "  date_sendt DATETIME,\n" +
                "  price INT,\n" +
                "  repair_description VARCHAR(1000),\n" +
                "  date_received DATETIME,\n" +
                "  bike_id INT,\n" +
                "  CONSTRAINT repair_pk PRIMARY KEY(repair_id),\n" +
                "  CONSTRAINT bike_id_fk FOREIGN KEY(bike_id) REFERENCES Bike(bike_id));";
    }

    /**
     * Method for getting the query for creating a customer table
     * @return      a String for creating customer table in DB
     */
    public String createCustomerTable(){
        return "CREATE TABLE Customer(\n" +
                "  customer_id INT NOT NULL AUTO_INCREMENT,\n" +
                "  firstname VARCHAR(30),\n" +
                "  surname VARCHAR(30),\n" +
                "  card_number LONG,\n" +
                "  CONSTRAINT customer_pk PRIMARY KEY(customer_id)\n" +
                ");";
    }

    /**
     * Method for getting the query for creating a bike table
     * @return      a String for creating bike table in DB
     */
    public String createUser_has_bikeTable(){
        return "CREATE TABLE User_has_bike(\n" +
                "  customer_id INT NOT NULL,\n" +
                "  bike_id INT NOT NULL,\n" +
                "  date_rented DATE NOT NULL,\n" +
                "  start_dock INT,\n" +
                "  CONSTRAINT user_has_bike_pk1 PRIMARY KEY (bike_id),\n" +
                "  CONSTRAINT user_has_bike_fk1 FOREIGN KEY (customer_id) REFERENCES Customer(customer_id),\n" +
                "  CONSTRAINT user_has_bike_fk2 FOREIGN KEY (bike_id) REFERENCES Bike(bike_id),\n" +
                "  CONSTRAINT user_has_bike_fk3 FOREIGN KEY (start_dock) REFERENCES  Docking_station(dock_id)\n" +
                ");";
    }

    /**
     * Method for getting the query for creating a admin table
     * @return      a String for creating admin table in DB
     */
    public String createAdminTable(){
        return "CREATE TABLE Admin(\n" +
                "  admin_id INT NOT NULL AUTO_INCREMENT,\n" +
                "  email VARCHAR(50) NOT NULL UNIQUE,\n" +
                "  password TEXT(60)NOT NULL,\n" +
                "  CONSTRAINT user_pk PRIMARY KEY(admin_id)\n" +
                ");\n";
    }

    /**
     * Method for getting the query for creating a admin table
     * @return      a String for creating admin table in DB
     */
    public String createBike_status(){
        return "CREATE TABLE Bike_status(\n" +
                "  bike_status_id INT NOT NULL AUTO_INCREMENT,\n" +
                "  latitude DOUBLE,\n" +
                "  longitude DOUBLE,\n" +
                "  charging_level INT,\n" +
                "  date DATE,\n" +
                "  bike_id INT,\n" +
                "  CONSTRAINT bike_status_pk PRIMARY KEY(bike_status_id),\n" +
                "  CONSTRAINT bike_fk FOREIGN KEY (bike_id) REFERENCES Bike(bike_id)\n" +
                ");";
    }

    /**
     * Method for getting the query for creating a docked_bike table
     * @return      a String for creating docked_bike table in DB
     */
    public String createBike_Docked(){
        return "CREATE TABLE Bike_docked(\n" +
                "  dock_id INT NOT NULL,\n" +
                "  bike_id INT NOT NULL,\n" +
                "  date_delivered DATE,\n" +
                "  CONSTRAINT bike_docked_pk1 PRIMARY KEY(bike_id),\n" +
                "  CONSTRAINT bike_docked_fk1 FOREIGN KEY(dock_id) REFERENCES Docking_station(dock_id),\n" +
                "  CONSTRAINT bike_docked_fk2 FOREIGN KEY(bike_id) REFERENCES Bike(bike_id)\n" +
                ");";
    }

    /**
     * Method for getting the query for creating a dock station history table
     * @return      a String for creating dock history table in DB
     */
    protected String createDockingStationHistory(){
        return "CREATE TABLE Docking_station_history(\n" +
                "  docking_station_history_id INT NOT NULL AUTO_INCREMENT,\n" +
                "  date DATE,\n" +
                "  name VARCHAR(30),\n" +
                "  power_usage INT,\n" +
                "  dock_id INT NOT NULL,\n" +
                "  CONSTRAINT docking_station_history_pk PRIMARY KEY(docking_station_history_id),\n" +
                "  CONSTRAINT docking_station_history_fk FOREIGN KEY(dock_id) REFERENCES Docking_station(dock_id)\n" +
                ");";
    }

    public String createBannedCustomers(){
        return "CREATE TABLE banned_Customers(\n" +
                "  customer_id INT,\n" +
                "  reason varchar(100) NOT NULL,\n" +
                "  CONSTRAINT cutomer_pk_2 PRIMARY KEY (customer_id),\n" +
                "  CONSTRAINT customer_fk FOREIGN KEY (customer_id) REFERENCES Customer(customer_id)\n" +
                ");\n";
    }

    /**
     * Method for creating a type in the DB
     * @param name      String for the name of the type
     * @return          a String containing the query for creating a type object with the desired name
     */
    public String createType(String name){
        return "INSERT INTO Type(name, status) VALUES ('"+ name+ "',0);";
    }

    /**
     * Method for creating a bike in the DB
     * @param date_purchased        Date for when the bike was purchased
     * @param price                 int for how much the bike cost
     * @param charge                int for the power level percentage
     * @param date_produced         Date for when the bike was produced
     * @param all_rep               int for number of repairs
     * @param total_trips           int for total number of trips
     * @param total_km              int for total number of km traveled
     * @return                      a String containing the query for creating a bike object with the desired variables
     */
    public String createBike(Date date_purchased, int price, int charge, Date date_produced, int all_rep, int total_trips, int total_km){
        return "INSERT INTO Bike (date_registered, price, charge, produced, all_repairs, total_trips, total_km, type_id, status) VALUES ('"+date_purchased +"',"+price+","+charge+",'"+date_produced+"',"+all_rep+","+total_trips+","+total_km+",1,1);";
    }

    /**
     * Method for creating a dock in the DB
     * @param name      String for the name of the dock
     * @param lat       double for the latitude of the dock
     * @param lon       double for the lonitude of the dock
     * @return          a String containing the query for creating a dock object with the desired variables
     */
    public String createDocking( String name, double lat, double lon){
        return "INSERT INTO Docking_station (name, latitude, longitude, status) VALUES ('"+name+ "',"+ lat+","+ lon+",0);";
    }

    /**
     * Method for creating a customer in th DB
     * @param firstname         String for the first name of the customer
     * @param surname           String containing the last name of the customer
     * @param card_number       long for the card number of the customer
     * @return                  a String containing the query for creating a customer object with the desired variables
     */
    public String createCustomer(String firstname, String surname,long card_number){
        return "INSERT INTO Customer (firstname, surname, card_number) VALUES ('"+firstname+"', '"+surname+"',"+card_number+");";
    }

    public String createAdmin(String email,String password){
        return "INSERT INTO Admin(email, password) VALUES ('"+email+"','"+password+"');";
    }

    public String bike_Docked(int docking_id, int bike_id, Date date){
        return "INSERT INTO Bike_docked(dock_id, bike_id, date_delivered) VALUES ("+docking_id+","+bike_id+",'"+date+"');";
    }

    public String bannedCustomers(int id, String reason){
        return "INSERT INTO banned_Customers(customer_id, reason) VALUES ("+id+",'"+reason+"');";
    }

    public String repairBike(int id, String repair_request,Date date_sendt,int price,String repair_desc,Date date_res, int bike_id){
        return "INSERT INTO Repairs(repair_id, repair_request, date_sendt, price, repair_description, date_received, bike_id) VALUES ("+id+", '"+repair_request+"', '"+date_sendt+"', "+price+", '"+repair_desc+"', '"+date_res+"', "+bike_id+");";
    }

    public String user_has_bike(int customer_id, int bike_id,Date date, int dock_id){
        return "INSERT INTO User_has_bike(customer_id, bike_id, date_rented, start_dock) VALUES ("+customer_id+","+bike_id+",'"+date+"',"+dock_id+");";
    }

    public String bike_Status(int bike_status_id,double lat, double lon,int charge, Date date, int bike_id){
        return "INSERT INTO Bike_status(bike_status_id, latitude, longitude, charging_level, date, bike_id) VALUES ("+bike_status_id+","+lat+","+lon+","+charge+",'"+date+"',"+bike_id+");";
    }

    public String docking_station_history(Date date,String name, int power_usage,int dock_id){
        return "INSERT INTO Docking_station_history(date, name, power_usage, dock_id) VALUES ('"+date+"','"+name+"',"+power_usage+","+dock_id+");";
    }

    public String delete_Type(){
        return "DELETE FROM Type WHERE type_id = type_id";
    }

    public String delete_Bike(){
        return "DELETE FROM Bike WHERE bike_id = bike_id";
    }

    public String delete_Docking(){
        return "DELETE FROM Docking_station WHERE dock_id = dock_id";
    }

    public String dropBannedCustomers(){
        return "DROP TABLE IF EXISTS banned_Customers;";
    }

    /**
     * Method for dropping the history table from the DB
     * @return      a String containing the query for dropping the table
     */
    public String dropHistory(){
        return "DROP TABLE IF EXISTS Docking_station_history;";
    }

    /**
     * Method for dropping the docked_bikes table from the DB
     * @return      a String containing the query for dropping the table
     */
    public String dropBike_docked(){
        return "DROP TABLE IF EXISTS Bike_docked;";
    }

    /**
     * Method for dropping the bike status table from the DB
     * @return      a String containing the query for dropping the table
     */
    public String dropBikeStat(){
        return "DROP TABLE IF EXISTS Bike_status;";
    }

    /**
     * Method for dropping the admin table from the DB
     * @return      a String containing the query for dropping the table
     */
    public String dropAdmin(){
        return "DROP TABLE IF EXISTS Admin;";
    }

    /**
     * Method for dropping the user_has_bike table from the DB
     * @return      a String containing the query for dropping the table
     */
    public String dropUser_has(){
        return "DROP TABLE IF EXISTS User_has_bike;";
    }

    /**
     * Method for dropping the customer table from the DB
     * @return      a String containing the query for dropping the table
     */
    public String dropCustomer(){
        return "DROP TABLE IF EXISTS Customer;";
    }

    /**
     * Method for dropping the repair table from the DB
     * @return      a String containing the query for dropping the table
     */
    public String dropRepair(){
        return "DROP TABLE IF EXISTS Repairs;";
    }

    /**
     * Method for dropping the bike table from the DB
     * @return      a String containing the query for dropping the table
     */
    public String dropBike(){
        return "DROP TABLE IF EXISTS Bike;";
    }

    /**
     * Method for dropping the type table from the DB
     * @return      a String containing the query for dropping the table
     */
    public String dropType(){
        return "DROP TABLE IF EXISTS Type;";
    }

    /**
     * Method for dropping the dock table from the DB
     * @return      a String containing the query for dropping the table
     */
    public String dropDocking(){
        return "DROP TABLE IF EXISTS Docking_station;";
    }
}

