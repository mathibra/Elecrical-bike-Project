package test.JUnitObjects;

import main.objects.Bike;
import main.objects.Docking;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import java.sql.Date;
import java.util.ArrayList;

/**
 * Class for testing the dock object in JUnit
 */
public class JUnitObjectDocking {
    private Docking dock;
    private ArrayList<Bike> bikes;
    private  Bike bike1;
    private Bike bike2;
    private Bike bike3;
    private Bike bike4;

    /**
     * Method for creating bikes and adding to an array inside the dock
     */
    @Before
    public void Before(){

        bikes = new ArrayList<>();

        dock = new Docking(1,"Prinsen Kino",63.427057, 10.392525100000057);
        //Make dates, bike1 og bike2 have the same date, bike3 have date3 and date4
        Date date1 = new Date(2018-01-02);
        Date date2 = new Date(2017-01-01);
        Date date3 = new Date(2018-04-01);
        Date date4 = new Date(2010-10-10);

        //Make bikes and put them into an array
        bike1 = new Bike(1,date1, 1000, 1,date2);
        bike2 = new Bike(2,date1,500,1,date2);
        bike3 = new Bike(3,date3,200,2,date4);
        bike4 = new Bike(4,date1,350,2,date3);
        bikes.add(bike1);
        bikes.add(bike2);
        bikes.add(bike3);
        dock.setBikes(bikes);
    }

    @After
    public void After(){
    }

    /**
     * For testing charging function on the bikes
     */
    @Test
    public void testChargeDockedBikes(){
        dock.chargeDockedBikes();
        assertEquals(3,bikes.get(1).getCharge());
        assertEquals(3,bikes.get(2).getCharge());
    }

    /**
     * for testing the sorting of the bikes in the dock
     */
    @Test
    public void testSortDocked(){
        bikes.get(0).setCharge(50);
        bikes.get(1).setCharge(100);
        //bike1 have 50
        //bike2 have 100
        //bike3 have 0
        dock.setBikes(dock.sortDocked());
        //Check if bike with the moste charging level is the first in the arraylist
        assertEquals(bike2, dock.getBikes().get(0));

    }

    /**
     * for testing that the amount of bikes are right
     */
    @Test
    public void testNumberOfBikes(){
        assertEquals(3,dock.numberOfBikes());
    }

    /**
     * for testing if adding a bike works
     */
    @Test
    public void testAddBike(){
        dock.addBike(bike4);
        assertEquals(4,dock.getAntDocked());
    }


    /**
     * for testing if the amount of docked bikes are correct
     */
    @Test
    public void testGetAntDocked(){
        int ant  = dock.getAntDocked();
        assertEquals(3,ant);
    }

    @Test
    public void test(){
    }
    public static void main(String[]args){
        org.junit.runner.JUnitCore.main(JUnitObjectDocking.class.getName());
    }
}
