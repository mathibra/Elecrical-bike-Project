package test.JUnitObjects;

import test.JUnittSimulator.JUnitSimulatorSimRun;
import main.objects.Bike;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.Date;

import static org.junit.Assert.*;

/**
 * Class for testing the bike object class in JUnit
 */
public class JUnitObjectBike {
    private Bike bike1;
    private Bike bike2;
    @Before
    public void Before(){
        Date date1 = new Date(2018-01-02);
        Date date2 = new Date(2017-01-01);
        bike1 = new Bike(1,date1, 1000, 1,date2);
        bike2 = new Bike(2,date1,500,1,date2);
    }

    @After
    public void After(){
    bike1 = null;
    }

    /**
     * Method for setting end editing values, then checking if they are correct
     */
    @Test
    public void test(){
        int id = bike1.getBike_id();
        assertEquals(1,id);

        int charge =bike1.getCharge();
        assertEquals(0,charge);

        bike1.setCharge(7);
        assertEquals(7, bike1.getCharge());

        bike1.changeChargeLevel(2);
        assertEquals(9,bike1.getCharge());


        bike1.makeATrip();
        assertEquals(1,bike1.getTot_trips());

        bike1.setDocked(1);
        assertEquals(1,bike1.getDocked());
    }

    public static void main(String[]args){
        org.junit.runner.JUnitCore.main(JUnitObjectBike.class.getName());
    }
}
