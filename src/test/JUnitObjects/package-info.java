/**
 * Contains the classes for handling JUnit tests with object classes
 * @see main.objects
 */
package test.JUnitObjects;