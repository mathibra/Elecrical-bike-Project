/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@Authors: Annebelle Almås, Mathias Braathen, William Chakroun Jacobsen & Terje Haugum
@Date submitted: 23.04.2018
@Summary: The class makes an object of a customer. With getters, setters & toString.

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////**/

package main.objects;

/**
 * Class for creating a customer
 */
public class Customer {
    private int customer_id;
    private String firstname;
    private String surname;
    private long cardNr;

    /**
     * Constructor takes in name of the user and creates an object connected to the users card number
     * @param firstname     String containing the first name of the user
     * @param surname       String containing the surname of the user
     * @param cardNr        long containing the car number of the user
     */
    public Customer(String firstname, String surname, long cardNr) {
        this.cardNr = cardNr;
        this.firstname = firstname;
        this.surname = surname;
    }

    /**
     * Second constructor used for easier processing, can relate to customer id instead of a long card number
     * @param customer_id       int representing id of the customer
     * @param firstname         String containing the first name of the user
     * @param surname           String containing the surname of the user
     * @param cardNr            long containing the car number of the user
     */
    public Customer(int customer_id, String firstname, String surname, long cardNr) {
        this.customer_id = customer_id;
        this.cardNr = cardNr;
        this.firstname = firstname;
        this.surname = surname;
    }


    public int getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public long getCardNr() {
        return cardNr;
    }

    public void setCardNr(long cardNr) {
        this.cardNr = cardNr;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "customer_id= " + customer_id +
                ", Firstname: " + firstname +
                ", Surname: " + surname +
                ", cardNr = " + cardNr +
                '}';
    }
}
