package main.objects;

import main.engine.*;

import java.io.Serializable;
import java.sql.Date;
import java.util.*;

/*
* the bike status can have 6 statuses.
* 0. Deleted
* 1. docked
* 2. repair
* 3. Driving
* 4. Need repair but in dock (can be docked?)
* 5. In storage(cant drive around)
* */

/**
 * Class for creating a bike object
 */
public class Bike extends Position implements Comparable<Bike>, Serializable {
    private int bike_id;
    private Date reg_date;
    private int price;
    private int type_id;
    private int charge;
    private Date prod_date;
    private int ant_rep;
    private int tot_trips;
    private int tot_km;
    private Date last_upt;
    private int docked;
    private int status;

    /**
     * A bike can have 6 statuses;
     * 0. Deleted
     * 1. docked
     * 2. repair
     * 3. Driving
     * 4. Need repair but in dock (can be docked?)
     * 5. In storage(cant drive around)
     * @param reg_date      Date when the bike was registered
     * @param price         int representing the price for buying the bike
     * @param type_id       int representing the id of the type of bike it is
     * @param prod_date     Date when the bike was produced
     */
    public Bike(Date reg_date, int price, int type_id, Date prod_date)  {
        this.reg_date = reg_date;
        this.price = price;
        this.type_id = type_id;
        this.prod_date = prod_date;
    }

    /**
     * This constructer is used when you have the ID from AUTO_INCREMENT in SQL
     * @param bike_id       int representing the id for the bike
     * @param reg_date      Date when the bike was registered
     * @param price         int representing the price for buying the bike
     * @param type_id       int representing the id of the type of bike it is
     * @param prod_date     Date when the bike was produced
     */
    public Bike(int bike_id, Date reg_date, int price, int type_id, Date prod_date) {
        this.bike_id = bike_id;
        this.reg_date = reg_date;
        this.price = price;
        this.type_id = type_id;
        this.prod_date = prod_date;
    }

    public int getType_id() {
        return type_id;
    }

    public int getBike_id() {
        return bike_id;
    }

    public Date getReg_date() {
        return reg_date;
    }

    public int getPrice() {
        return price;
    }

    public int getCharge() {
        return charge;
    }

    public Date getProd_date() {
        return prod_date;
    }

    public int getAnt_rep() {
        return ant_rep;
    }

    public int getTot_trips() {
        return tot_trips;
    }

    public int getTot_km() {
        return tot_km;
    }


    public int getDocked(){
        return docked;
    }

    public int getStatus(){
        return status;
    }

    public void setBike_id(int bike_id) {
        this.bike_id = bike_id;
    }

    public void setReg_date(Date reg_date) {
        this.reg_date = reg_date;
    }

    public void setProd_date(Date prod_date) {
        this.prod_date = prod_date;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setCharge(int charge) {
        this.charge = charge;
    }

    /**
     * For increasing the charge level of a bike, up to 100 %
     * @param input     an int representing the increase in charge level
     */
    public void changeChargeLevel(int input){
        if((this.charge + input) >= 100){
            this.charge = 100;
        }else {
            this.charge += input;
        }
    }

    public void setAnt_rep(int ant_rep) {
        this.ant_rep = ant_rep;
    }

    public void newRepair(){
        ant_rep++;
    }

    public void setTot_trips(int tot_trips) {
        this.tot_trips = tot_trips;
    }

    public void makeATrip(){
        this.tot_trips = tot_trips +1;
    }

    public void setTot_km(int tot_km) {
        this.tot_km += tot_km;
    }

    public void addKm(int km){
        tot_km += km;
    }

    public void setLast_upt(Date last_upt) {
        this.last_upt = last_upt;
    }

    public void setDocked(int docked){
        this.docked = docked;
    }

    public void setStatus(int status){
        this.status = status;
    }

    /*
    * @param: 1. Bike Object
    * */

    /**
     * customized the compareTo to make sure when sorting the arraylist of bikes it will sort after charge level, to assure a customer gets one of the bikes with highest charge
     * @param compareBike       Bike object to compare with
     * @return                  an int representing the difference in charge
     * @see Docking#sortDocked()
     */
    public int compareTo(Bike compareBike) {
        int compareCharge=(compareBike).getCharge();
        return compareCharge-this.charge;
    }

    @Override
    public String toString() {
        return "{" + bike_id +
                ", " + reg_date +
                ", " + price +
                ", " + charge +
                ", " + prod_date +
                ", " + ant_rep +
                ", " + tot_trips +
                ", " + tot_km +
                ", " + status +
                '}';
    }
}
