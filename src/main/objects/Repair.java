/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@Authors: Annebelle Almås, Mathias Braathen, William Chakroun Jacobsen & Terje Haugum
@Date submitted: 23.04.2018
@Summary: The class makes an object of a repair. With getters, setters & toString.

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////**/

package main.objects;

import java.util.*;

/**
 * Class for creating a repair object
 */
public class Repair {
    private int repair_id;
    private String description;
    private String request;
    private int price;
    private Date date_received;
    private String repair_description;
    private Date date_done;
    private int bike_id;

    /**
     *Constructor for creating a repair object connected to a bike object
     * @param bike_id           int representing the id of the bike being repaired
     * @param date_received     Date for when the repair request was received
     * @param request           String containing the description for why the repair was requested
     */
    public Repair(int bike_id, Date date_received, String request){
        this.bike_id = bike_id;
        this.date_received = date_received;
        this.request = request;
    }

    public Repair(){}

    /**
     * Method for finishing a repair
     * @param price                     int representing the price for the repair
     * @param repair_description        String containing the repair description
     * @param date_done                 Date for telling when the repair was done
     * @param bike_id                   int representing the id of the bike that was repaired
     */
    public void repairDone(int price,String repair_description,Date date_done ,int bike_id){
        this.bike_id = bike_id;
        this.repair_description = repair_description;
        this.price = price;
        this.date_done = date_done;
        String repairDone = "-----------------Repair done on bike nr: " + bike_id+ " -----------------";
        System.out.println(repairDone);
    }

    public int getRepair_id() {
        return repair_id;
    }

    public void setRepair_id(int repair_id) {
        this.repair_id = repair_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Date getDate_received() {
        return date_received;
    }

    public void setDate_received(Date date_received) {
        this.date_received = date_received;
    }

    public String getRepair_description() {
        return repair_description;
    }

    public void setRepair_description(String repair_description) {
        this.repair_description = repair_description;
    }

    public Date getDate_done() {
        return date_done;
    }

    public void setDate_done(Date date_done) {
        this.date_done = date_done;
    }

    public int getBike_id() {
        return bike_id;
    }

    public void setBike_id(int bike_id) {
        this.bike_id = bike_id;
    }
}
