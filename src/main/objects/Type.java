/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@Authors: Annebelle Almås, Mathias Braathen, William Chakroun Jacobsen & Terje Haugum
@Date submitted: 23.04.2018
@Summary: The class makes an object of a Type. With getters, setters & toString.

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////**/
/*Type can be 2 status
* 0. Active
* 1. Inactive
* */

package main.objects;
import java.io.Serializable;

/**
 * Class for creating a bike type
 */
public class Type implements Serializable{
    private int type_id;
    private String name;
    private int status;

    /**
     * Constructor to make a bike type, used when retrieving from database
     * Status; 0=active, 1=inactive
     * @param type_id       int representing the id fo the type
     * @param name          String containing the name for the type
     */
    public Type(int type_id, String name){
        this.name = name;
        this.type_id = type_id;
        this.status = 0;
    }

    /**
     * Constructor that only takes in a name
     * @param name      String containing the name for the type
     */
    public Type(String name){
        this.name = name;
    }
    
    public int getType_id(){
        return type_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Type{" +
                "type_id=" + type_id +
                ", name='" + name + '\'' +
                ", status = " +status +
                '}';
    }
}
