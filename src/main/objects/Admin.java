package main.objects;

import main.engine.*;
/*//////////////////////////////////////////////////////////////////////////////////////////////////////

@Authors: Annebelle Almås, Mathias Braathen, William Chakroun Jacobsen & Terje Haugum
@Date submitted: 23.04.2018
@Summary: The class makes an object of a the admin.

//////////////////////////////////////////////////////////////////////////////////////////////////////*/


/*
* project : Xcentrion
* @author : Team 16
*
* 
* */

import main.engine.HashPasswords;

/**
 * Class for creating an admin object, with a username and password
 */
public class Admin{
    private String password1,password, e_mail;
    private String salt1 = "kaøsdfjl3øj23j4((/&)(/)(=`?=)(>><-.asdfbnmckriitursthdreywtuyetjdfkhglopyøilhfjkgdhjfghdfsgbvnmjfhliupioyi";
    private HashPasswords hashPasswords = new HashPasswords();

    /**
     * The constructor is used to create a hashed key at once
     * @param e_mail        String containing the email of the admin
     */
    public Admin(String e_mail){
        this.e_mail = e_mail;
        this.password = hashPasswords.passwordgen();
        this.password1 = password;
        try {
            byte[] salt = new byte[salt1.length()];

            for (int i = 0; i < salt1.length(); i++) {
                salt[i] = (byte) salt1.charAt(i);
            }
            byte[] hashpsw = HashPasswords.hash(password.toCharArray(), salt);
            String pswd = "";

            for (int i = 0; i < hashpsw.length; i++) {
                pswd = pswd + (char) hashpsw[i];
            }
            this.password = pswd;
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public String getE_mail() {
            return e_mail;
    }
    public String getPassword(){
        return password;
    }
    public String realpasswords(){
        return password1;
    }

    public void setSalt1(String salt1) {
        this.salt1 = salt1;
    }

    /**
     * Method for checking if a password input is correct
     * @param passwordcheck     String containing the desired password to check
     * @return                  a hashed password
     */
    public String login(String passwordcheck){
        byte[] salt = new byte[salt1.length()];

        for (int i = 0; i < salt1.length(); i++) {
            salt[i] = (byte) salt1.charAt(i);
        }
        byte[] hashpsw = HashPasswords.hash(passwordcheck.toCharArray(), salt);
        String pswd = "";

        for (int i = 0; i < hashpsw.length; i++) {
            pswd = pswd + (char) hashpsw[i];
        }
        return pswd;
        }
}


