package main.objects;

import main.engine.Position;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Class for creating a dock object. Each dock has an arraylist to keep track of bikes in it.
 * A dock can have two statuses; 0=active, 1=inactive. This is to avoid database issues when deleting.
 * Denne vil arve metodene til Position så den trenger ikke å ha setters and getters fra Position
 * This class will inherit the methods from Position, so setters and getters for that is unnecessary
 * @see Position
 */
public class Docking extends Position implements Serializable{
    private int dock_id;
    private String name;
    private ArrayList<Bike> bikes = new ArrayList<Bike>();
    private double power_usage;
    private int status;

    /**
     * Constructor to create dock object without an id
     * @param name          String containing the name for the dock
     * @param latitude      double representing the latitude of the dock
     * @param longitude     double representing the longitude of the dock
     */
    public Docking(String name, double latitude, double longitude) {
        super(latitude,longitude);
        this.name = name;
    }

    /**
     * Constructor to create a dock object and assign it an id for easier processing
     * @param dock_id       int representing id of the dock
     * @param name          String containing the name for the dock
     * @param latitude      double representing the latitude of the dock
     * @param longitude     double representing the longitude of the dock
     */
    public Docking(int dock_id, String name, double latitude, double longitude) {
        super(latitude,longitude);
        this.dock_id = dock_id;
        this.name = name;
    }

    /**
     * Charge the bikes on the dock with less than 100 in charging, and increases the power usage of the dock
     * based on the nr of bikes charging on the dock
     */
    public void chargeDockedBikes(){
        int charge = 2;//Sets how many precent the bike should charge with each update
        int bikeCharging = 0;
        double powerPerBike = 2.5;
        for(int i = 0; i < bikes.size(); i++){
            bikes.get(i).changeChargeLevel(charge);
            if(bikes.get(i).getCharge() <100) {
                System.out.println("Bike: " + bikes.get(i).getBike_id() + ", Charging level: " + bikes.get(i).getCharge());
                bikeCharging++;
            }
        }
        double powerAllBikes = powerPerBike * bikeCharging;
        addPower_usage(powerAllBikes);
    }

    public double getLatitude(){
        return latitude;
    }

    public double getLongitude(){
        return longitude;
    }

    public int getDock_id() {
        return dock_id;
    }

    public void setDock_id(int dock_id) {
        this.dock_id = dock_id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public ArrayList<Bike> getBikes() {
        return bikes;
    }
    public void setBikes(ArrayList<Bike> b){
        bikes.addAll(b);
    }

    public int numberOfBikes(){
        return bikes.size();
    }

    public void setPower_usage(double power_usage) {
        this.power_usage = power_usage;
    }

    public void addPower_usage(double power_usage){
        this.power_usage += power_usage;
    }

    public double getPower_usage() {
        return power_usage;
    }

    /**
     * Method for adding a bike objects to the dock
     * @param bike  a bike object to add
     * @return      true if add went well, false if not
     */
    public boolean addBike(Bike bike) {
        for(int i = 0; i < bikes.size(); i++) {
            if(bikes.get(i) == bike) {
                return false;
            }
        }
        bikes.add(bike);
        return true;
    }

    /**
     * Method for getting number of bike objects currently int he dock
     * @return      a number representing the number of bikes
     */
    public int getAntDocked() {
        int count = 0;
        for(int i = 0; i < bikes.size(); i++) {
            if(bikes.get(i) != null) {
                count++;
            }
        }
        return count;
    }

    public void setStatus(int status){
        this.status = status;
    }

    public int getStatus(){
        return status;
    }

    /**
     * Method for sorting the arraylist of bike based on the power percentage
     * @return      a sorted version of the arraylist
     * @see Bike#compareTo(Bike)
     */
    public ArrayList<Bike> sortDocked() {
        Collections.sort(bikes);
        return bikes;
    }

    @Override
    public String toString() {
        return "Docking{" +
                "dock_id = " + dock_id +
                ", name =' " + name + '\'' +
                ", latitude = " + latitude +
                ", longitude = " + longitude +
                ", power_usage = " + power_usage +
                ", status = " + status +
                '}' + "\n";
    }
}
