package main.engine;

import java.io.*;


import main.database.Executor;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 * class for creating chart for viewing the number of rentals per month
 */
public class LineChart {

    String name = "LineChart.jpeg";
    File lineChart = new File(name);

    public String getName() {
        return name;
    }

    /**
     * Method for making sure the generated chart is deleted, so the program always works with updated version
     */
    public void delete() {
        File fil = new File("LineChart.jpeg");
        if(fil.delete())
        {
            System.out.println("File deleted successfully");
        }
        else
        {
            System.out.println("Failed to delete the file");
        }
    }

    /**
     * Method for creating chart based on data from database, and saves it as a .jpeg for the GUI to use
     * @param year  int representing the year to view
     */
    public void makeChart(int year) {
        delete();
        DefaultCategoryDataset line_chart_dataset = new DefaultCategoryDataset();
        for (int i = 0; i < 12; i++) {
            String mnd = "";
            if (i == 0) { mnd = "January"; }
            if (i == 1) { mnd = "February"; }
            if (i == 2) { mnd = "March"; }
            if (i == 3) { mnd = "April"; }
            if (i == 4) { mnd = "May"; }
            if (i == 5) { mnd = "June"; }
            if (i == 6) { mnd = "July"; }
            if (i == 7) { mnd = "August"; }
            if (i == 8) { mnd = "September"; }
            if (i == 9) { mnd = "October"; }
            if (i == 10) { mnd = "November"; }
            if (i == 11) { mnd = "Desember";}
            line_chart_dataset.addValue(Executor.getAntUtleid(year, i+1), "Number of rentals: ", mnd);
        }

        try {
            JFreeChart lineChartObject = ChartFactory.createLineChart(
                    "Rentals for " + year,"Month",
                    "Number of rentals",
                    line_chart_dataset,PlotOrientation.VERTICAL,
                    true,true,false);

            int width = 1000;    /* Width of the image */
            int height = 480;   /* Height of the image */
            ChartUtilities.saveChartAsJPEG(lineChart ,lineChartObject, width ,height);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}