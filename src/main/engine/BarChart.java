package main.engine;

import java.io.*;


import main.database.Executor;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.chart.ChartUtilities;

/**
 * class for creating a chart for viewing number of repairs per individual type, for product improvement
 */
public class BarChart {


    final DefaultCategoryDataset dataset = new DefaultCategoryDataset();

    /**
     * method for making sure the generated chart is deleted, so the program always works with updated version
     */
    public void delete() {
        File fil = new File("BarChart.jpeg");
        if(fil.delete())
        {
            System.out.println("File deleted successfully");
        }
        else
        {
            System.out.println("Failed to delete the file");
        }
    }

    /**
     * Method for creating chart based on data from database, and saves it as a .jpeg for the GUI to use
     */
    public void makeChart() {
        for (int i = 0; i < Executor.getAllTypes().size(); i++) {
            dataset.addValue(Executor.getAntRepairs(i+1), "Antall Reperasjoner", Executor.getAllTypes().get(i).getName());
        }
        JFreeChart barChart = ChartFactory.createBarChart(
                "Reperasjoner per sykkeltype",
                "Type", "Antall reperasjoner",
                dataset,PlotOrientation.VERTICAL,
                true, true, false);

        int width = 640;    /* Width of the image */
        int height = 480;   /* Height of the image */
        File BarChart = new File( "BarChart.jpeg" );
        try {
            ChartUtilities.saveChartAsJPEG(BarChart , barChart , width , height);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}