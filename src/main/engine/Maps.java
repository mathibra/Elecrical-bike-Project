package main.engine;

import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.events.*;
import com.teamdev.jxbrowser.chromium.swing.BrowserView;
import main.objects.Bike;
import main.objects.Docking;

import javax.swing.*;
import java.awt.*;

public class Maps {

    private Browser browser = new Browser();
    private BrowserView view = new BrowserView(browser);
    private int x = 700;
    private int y = 250;
    private int width = 400;
    private int height = 400;
    private Docking dock = null;
    private StringBuilder script;

    public Maps(JPanel map,int x, int y, int width, int height){
        browser.loadURL(Maps.class.getResource("../resources/Map.html").toExternalForm());
        map.add(view,BorderLayout.CENTER);
        map.setBounds(x,y,width,height);
        map.setVisible(true);
    }

    public Maps(JPanel map, int x, int y, int width, int height, Docking dock){
        this.dock = dock;
        browser.loadURL(Maps.class.getResource("../resources/Map.html").toExternalForm());
        map.add(view,BorderLayout.CENTER);
        map.setBounds(x,y,width,height);
        map.setVisible(true);
    }

    private Docking getDock(){
        return dock;
    }

    private void addBike(Bike bike){
            StringBuilder script = new StringBuilder();
            script.append("document.addBike(");
            script.append(bike.getBike_id());
            script.append(",");
            script.append(bike.getLatitude());
            script.append(",");
            script.append(bike.getLongitude());
            script.append(",");
            script.append(bike.getCharge());
            script.append(");");
            browser.addLoadListener(getLoadingListener(script.toString()));

    }

    private void updateBike(Bike bike){
        if(bike.getStatus() == 3) {
            StringBuilder script = new StringBuilder();
            script.append("document.updateBike(");
            script.append(bike.getBike_id());
            script.append(",");
            script.append(bike.getLatitude());
            script.append(",");
            script.append(bike.getLongitude());
            script.append(",");
            script.append(bike.getCharge());
            script.append(");");
            browser.executeJavaScript(script.toString());
        } else{
            StringBuilder script = new StringBuilder();
            script.append("document.updateBike(");
            script.append(bike.getBike_id());
            script.append(",");
            script.append(10);
            script.append(",");
            script.append(60);
            script.append(",");
            script.append(bike.getCharge());
            script.append(");");
            browser.executeJavaScript(script.toString());
        }
    }

    public void updateBikes(Bike[] bikes){
        for(Bike bike: bikes){
            updateBike(bike);
        }
    }

    public void addBikes(Bike[] bikes){
        for(Bike bike: bikes){
            addBike(bike);
        }
    }

    private void addDock(Docking dock){
        if(dock.getStatus() == 1 ){
            script = new StringBuilder();
            script.append("document.addDock(");
            script.append(dock.getDock_id());
            script.append(",");
            script.append(10);
            script.append(",");
            script.append(10);
            script.append(",");
            script.append(0);
            script.append(",'");
            script.append("INACTIVE!");
            script.append("');");
        }else {
            script = new StringBuilder();
            script.append("document.addDock(");
            script.append(dock.getDock_id());
            script.append(",");
            script.append(dock.getLatitude());
            script.append(",");
            script.append(dock.getLongitude());
            script.append(",");
            script.append(dock.getAntDocked());
            script.append(",'");
            script.append(dock.getName());
            script.append("');");
        }
        browser.addLoadListener(getLoadingListener(script.toString()));
    }

    private void addThisDock(){
        script = new StringBuilder();
        script.append("document.addThisDock(");
        script.append(this.dock.getDock_id());
        script.append(",");
        script.append(this.dock.getLatitude());
        script.append(",");
        script.append(this.dock.getLongitude());
        script.append(",");
        script.append(this.dock.getAntDocked());
        script.append(",'");
        script.append(this.dock.getName());
        script.append("');");
        browser.addLoadListener(getLoadingListener(script.toString()));
    }

    public void addDocks(Docking[] docks){
        if(dock == null) {
            for (Docking dock : docks) {
                addDock(dock);
            }
        }else{
            for (Docking dock : docks) {
                if(dock.getDock_id() == getDock().getDock_id()){
                    addThisDock();
                }else{
                    addDock(dock);
                }

            }
        }
    }


    private LoadListener getLoadingListener(String script){
        return new LoadListener() {
            @Override
            public void onStartLoadingFrame(StartLoadingEvent startLoadingEvent) {
                browser.executeJavaScript(script);
            }

            public void onProvisionalLoadingFrame(ProvisionalLoadingEvent provisionalLoadingEvent) {

            }

            @Override
            public void onFinishLoadingFrame(FinishLoadingEvent finishLoadingEvent) {

            }

            @Override
            public void onFailLoadingFrame(FailLoadingEvent failLoadingEvent) {

            }

            @Override
            public void onDocumentLoadedInFrame(FrameLoadEvent frameLoadEvent) {

            }

            @Override
            public void onDocumentLoadedInMainFrame(LoadEvent loadEvent) {

            }
        };
    }
}
