package main.engine;/*
 *   Author: William Chakroun Jacobsen
 *   06.03.2018
 *
 * */
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.Random;

public class HashPasswords{

    private static final SecureRandom RANDOM = new SecureRandom();
    private static final int ITERATIONS = 1000;
    private static final int KEY_LENGTH = 256;

    public HashPasswords(){}
// This creates a hash of the password.
    public static byte[] hash(char[] password, byte[] salt) {
        PBEKeySpec spec = new PBEKeySpec(password, salt, ITERATIONS, KEY_LENGTH);
        Arrays.fill(password, Character.MIN_VALUE);
        try {
            SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
                return skf.generateSecret(spec).getEncoded();
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new AssertionError("Error while hashing a password: " + e.getMessage(), e);
        } finally {
            spec.clearPassword();
        }
    }

    // this creates a String with 100 characters so you can use it in hashing.
    public static String saltGen(){
        String PN = "ABCDEFG¤%&/HIHJ£$KLMNOPQRSTUVWXYZÆØÅ0123456789!#¤%&/()=/*<>UV@£$€@{{";
        StringBuilder salt = new StringBuilder();

        while(salt.length() < 100){
            Random rand = new Random();
            salt.append(PN.charAt(rand.nextInt(PN.length())));
        }
        return salt.toString();
    }
    // this generates a password.
    public static String passwordgen(){
        String PN = "ABCDEFGHIHJKLMNOPQRSTUVWXYZÆØÅ0123456789";
        String password = "";

        while(password.length() < 10){
            Random rand = new Random();
            password += PN.charAt(rand.nextInt(PN.length()));
        }

        return password;
    }

    public String login(String passwordcheck){
        String salt1 = "kaøsdfjl3øj23j4((/&)(/)(=`?=)(>><-.asdfbnmckriitursthdreywtuyetjdfkhglopyøilhfjkgdhjfghdfsgbvnmjfhliupioyi";
        byte[] salt = new byte[salt1.length()];

        for (int i = 0; i < salt1.length(); i++) {
            salt[i] = (byte) salt1.charAt(i);
        }
        byte[] hashpsw = HashPasswords.hash(passwordcheck.toCharArray(), salt);
        String pswd = "";

        for (int i = 0; i < hashpsw.length; i++) {
            pswd = pswd + (char) hashpsw[i];
        }
        return pswd;
    }
}



