package main.engine;

import main.database.Executor;
import main.objects.Bike;
import main.objects.Customer;
import main.objects.Docking;

import java.time.LocalDate;
import java.util.Calendar;

/*
* This Class is when a cutsomer is going to rent a bike. then the
*
* */

public class CustomerRents {
    private Bike bike;
    private Customer customer;
    private LocalDate date_rented;
    private int km;



    public CustomerRents(){}

    /**
     * The method is for renting.
     * @param dock the dock the rent start from.
     * @param customer  the customer that rent.
     * @return int it returns a positive nr if the customer can rent a bike at the docking station.
     */
    public int rent(Docking dock, Customer customer){
        if(!costumerPay(customer)){
            System.out.println("payment rejected");
            return -2;
        }else{
            for(int i = 0; i<dock.numberOfBikes(); i++){
                if(dock.sortDocked().get(i).getStatus() == 1){
                    Executor.newCustomer(customer.getFirstname(),customer.getSurname(),customer.getCardNr());
                    int customerID = Executor.getCustomerID(customer);
                    int bike_id = dock.sortDocked().get(i).getBike_id();
                    Executor.unDockBike(bike_id, customerID,dock.getDock_id(),Executor.dateNow());
                    dock.sortDocked().remove(i);
                    return bike_id;
                }
            }
            System.out.println("there are no bikes available");
            return -1;
        }
    }

    /**
     * Checks id the customer can  pay
     * @param costumer
     * @return boolean, true if the payment go through and false if it doesn't.
     */
    private boolean costumerPay(Customer costumer){
        if(false){//customer don't have money
            return false;
        }
        //customer has money
        return true;
    }

    /**
     * This method make a receipt of the trip.
     * @param start the docking the trip start on
     * @param end the docking the trip ends on
     * @param customer the customer that rents the bike
     * @param bike the bike the customer rents
     * @return StringBuilder a receipt of the trip
     */
    public StringBuilder receipt(Docking start, Docking end, Customer customer, Bike bike){
        StringBuilder resept = new StringBuilder();
        resept.append("-----------------------------------------------------------------------------------\n");
        resept.append("Customer: " + customer.getFirstname()+ " " + customer.getSurname() +"\n");
        resept.append("Docking start: " + start.getName() + "\n");
        resept.append("Docking end: " + end.getName() + "\n");
        resept.append("Bike : " + bike.getBike_id() + "\n");
        resept.append("Date: " + Executor.dateNow() + "\n");
        resept.append("Thank you for driving with Trondheim Citybike\n");
        resept.append("-----------------------------------------------------------------------------------");
        return resept;
    }
}
