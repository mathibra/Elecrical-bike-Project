package main.engine;

import main.objects.Admin;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


/**
 * This class uses a SMTP google server to send e-mails.
 */
public class Email {

    /**
     * This method sends an email with a password to the email that is inputted.
     * @param a1 the username/email to the admin
     */
    public void sendEmail(Admin a1) {
            Properties props = new Properties();
            props.put("mail.smtp.host", "smtp.gmail.com");
            props.put("mail.smtp.socketFactory.port", "465");
            props.put("mail.smtp.socketFactory.class",
                    "javax.net.ssl.SSLSocketFactory");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.port", "465");

            Session session = Session.getDefaultInstance(props,
                    new javax.mail.Authenticator() {
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication("williamjavaman@gmail.com","HelloWorld");
                        }
                    });
            try {
                Message message = new MimeMessage(session);
                message.setFrom(new InternetAddress("williamjavaman@gmail.com"));
                message.setRecipients(Message.RecipientType.TO,
                        InternetAddress.parse(a1.getE_mail()));
                message.setSubject("Trondheim Citybike");
                message.setText("Hello user: "+ a1.getE_mail()+ "\n\n Your password is : " + a1.realpasswords());

                Transport.send(message);

                System.out.println("Done");

            } catch (MessagingException e) {
                throw new RuntimeException(e);
            }
        }

}

