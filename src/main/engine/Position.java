package main.engine;

/**
 * Sets the position
 */
public class Position {
    public double latitude;
    public double longitude;

    /**
     * Sets the coordinates
     * @param latitude double
     * @param longitude double
     */
    public Position(double latitude, double longitude){
        this.latitude = latitude;
        this.longitude = longitude;
    }
    public Position(){}

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
