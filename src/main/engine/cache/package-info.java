/**
 * Contains classes for creating caches for faster loading times
 */
package main.engine.cache;