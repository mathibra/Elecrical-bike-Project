package main.engine.cache;

/**
* this class takes in a region in the memory to store information about all the bike objects.
* that is so the application is faster.
 * @see main.objects.Bike
* */
import main.database.Executor;
import org.apache.jcs.JCS;
import main.objects.Bike;
import org.apache.jcs.access.exception.CacheException;

import java.util.ArrayList;

public class BikeCache {
    private JCS cache;


    public BikeCache() {
        try {
            cache = JCS.getInstance("bikeCache");
            addAllBikes();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public JCS getCache(){
        return cache;
    }

    /**
     * The method here is used to add all the bikes in the cache/RAM.
     */
    private void addAllBikes(){
        if(cache.getGroupKeys("bikeCache").isEmpty()){
            ArrayList<Bike> bikes = new ArrayList<>(Executor.getAllBikes());
            for (Bike bike: bikes) {
                try {
                    int bikeID = bike.getBike_id();
                    bike.setDocked(Executor.retriveDock(bike.getBike_id()));
                    cache.put(bikeID, bike);
                    cache.putInGroup(bikeID,"bikeCache", bike);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            System.out.println("[Bike loaded to RAM]");
        }
    }

    /**the method here is used to add a bike in the cache/RAM.
     * @param bike A Bike object.
     */
    public void addBikes(Bike bike){
        try {
            cache.put(bike.getBike_id(), bike);
            cache.putInGroup(bike.getBike_id(), "bikeCache", bike);
        } catch (CacheException e) {
            e.printStackTrace();
        }
    }

    /** this gets the bike from cache with the spesific key! in this case its the ID.
     * @param id    the bike id.
     * @return  A bike object.
     */
    public Bike returnBike(int id){
        return (Bike)cache.get(id);
    }

    /**Deletes the bike from the cache/RAM with the key as input.
     *
     * @param id  The bike id.
     */
    public void removeBike(int id){
        try{
            cache.remove(id);
            cache.remove(id,"bikeCache");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * This method edits a bike in the memory
     * @param id The bike id
     * @param price The new price for the bike.
     * @param dateD The new produced date of the bike.
     * @param total_reparations The new total reparations
     * @param total_trips The new total trips
     * @param total_km      the total number of km the bike has traveled
     * @param type_id The new type id for the bike.
     * @param status        the status of the bike, represented in an int
     * @see main.objects.Bike
     * */
    public void editBike(int id, int price, java.sql.Date dateD, int total_reparations, int total_trips,int total_km,int type_id, int status){
        try{
            Bike bike = new Bike(id,dateD,price,type_id,returnBike(id).getProd_date());
            int charge = returnBike(id).getCharge();
            removeBike(id);
            addBikes(bike);
            returnBike(id).setTot_km(total_km);
            returnBike(id).setTot_trips(total_trips);
            returnBike(id).setAnt_rep(total_reparations);
            returnBike(id).setCharge(charge);
            returnBike(id).setStatus(status);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}