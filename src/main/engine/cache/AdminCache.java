package main.engine.cache;

import org.apache.jcs.JCS;
import org.apache.jcs.access.exception.CacheException;
/**
 * This creates an region in the memory that is specified for a specific user.
 * */
public class AdminCache {
    private JCS cache;
    /**
     *The constructor initializes the region in the memory.
     */
    public AdminCache(){
        try {
            cache = JCS.getInstance("adminCache");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * @return JCS cache object.
     */
    public JCS getCache(){
        return cache;
    }

    /**
     * this adds the user email that logs inn.
     * @param email     A string containing the email adress of the mail ti put in the cache
     */
    public void addEmail(String email){
        try {
            cache.put(1,email);
            cache.putInGroup(1, "adminCache", email);
        } catch (CacheException e) {
            e.printStackTrace();
        }
    }
    /**
     * This method returns a specific object in the cache.
     * @param id A key for the memory on where the admin is located.
     * @return an admin object.
     * **/
    public String returnAdmin(int id){
        return (String)cache.get(id);
    }
}

