package main.engine.cache;
/**
* this class takes in a region in the memory to store information about all the bike objects.
* that is so the application is faster.
*@see main.objects.Type
* */
import main.database.Executor;
import main.objects.Type;
import org.apache.jcs.JCS;
import org.apache.jcs.access.exception.CacheException;


public class TypeCache {
        private JCS cache;


        public TypeCache() {
            try {
                cache = JCS.getInstance("typeCache");
                addAllTypes();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public JCS getCache(){
            return cache;
        }
    /**The method here is used to add all the types in  the cache/RAM.
     *
     */
        private void addAllTypes(){
            if(cache.getGroupKeys("typeCache").isEmpty()){
                for (int i = 0; i < Executor.getAllTypes().size(); i++) {
                    try {
                        int typeID = Executor.getAllTypes().get(i).getType_id();
                        Type type = Executor.getAllTypes().get(i);
                        cache.put(typeID, type);
                        cache.putInGroup(typeID,"typeCache", type);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                System.out.println("[Types loaded to RAM]");
            }
        }
    /**
     * the method here is used to add a type in the cache/RAM.
     * @param type A Type object.
     */
        public void addType(Type type){
            try {
                cache.put(type.getType_id(), type);
                cache.putInGroup(type.getType_id(), "typeCache", type);
            } catch (CacheException e) {
                e.printStackTrace();
            }
        }

    /** this gets the type from cache with the spesific key! in this case its the ID.
     * @param id    the type id.
     * @return  A Type object.
     */
        public Type returnType(int id){
            return (Type)cache.get(id);
        }

        /**
         * This removes the type from the cache.
         * @param id Type id
         * */
        public void removetype(int id){
            try{
                cache.remove(id);
                cache.remove(id,"typeCache");
            }catch (Exception e){
                e.printStackTrace();
            }
        }

    /**
     *This edits the type in the cache.
     * @param id the type id that is going to be changed.
     * @param name  new name for the type.
     */
    public void editType(int id, String name){
            Type editedType = new Type(id,name);
            removetype(id);
            addType(editedType);
        }

}

