package main.engine.cache;

/**
* this class takes in a region in the memory to store information about all the bike objects.
* that is so the application is faster.
 * @see main.objects.Docking
* */

import main.database.Executor;
import main.objects.Docking;
import org.apache.jcs.JCS;
import main.objects.Bike;
import org.apache.jcs.access.exception.CacheException;

import java.util.ArrayList;
import java.util.List;

public class DockCache {
    private JCS cache;

    private BikeCache bc = new BikeCache();

    public DockCache() {
        try {
            cache = JCS.getInstance("dockCache");
            addAllDocks();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public JCS getCache(){
        return cache;
    }
    /**
     * The method here is used to add all the docks to the cache/RAM.
     */
    private void addAllDocks(){
        ArrayList<Docking> docks = new ArrayList<>(Executor.getAllDockings());
        ArrayList<Bike> bikes = new ArrayList<>();
        List<Integer> bufferID = new ArrayList<>(bc.getCache().getGroupKeys("bikeCache"));
        for(int i: bufferID){
            bikes.add(bc.returnBike(i));
        }

        if(cache.getGroupKeys("dockCache").isEmpty()){
            for(Docking dock: docks) {
                try {
                    int dockID = dock.getDock_id();
                    for(Bike bike: bikes){
                        if(bike.getStatus() != 0){
                            if(bike.getDocked() == dockID){
                                dock.addBike(bike);
                            }
                        }
                    }
                    cache.put(dockID,dock);
                    cache.putInGroup(dockID,"dockCache", dock);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            System.out.println("[Docks loaded to RAM]");
        }
    }
    /**the method here is used to add a docking in the cache/RAM.
     * @param dock A Docking object.
     */
    public void addDock(Docking dock){
        try {
            cache.put(dock.getDock_id(), dock);
            cache.putInGroup(dock.getDock_id(), "dockCache", dock);
        } catch (CacheException e) {
            e.printStackTrace();
        }
    }
    /** this gets the dock from cache with the spesific key! in this case its the ID.
     * @param id    the dock id.
     * @return  A Docking object.
     */
    public Docking returnDock(int id){
        return (Docking)cache.get(id);
    }
    /***
     * This methods remove the dock from the memory
     * @param id The dock id
     */

    public void removeDock(int id){
        try{
            cache.remove(id);
            cache.remove(id,"dockCache");
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    /**
     * This method edits a bike in the memory. The latitude and longitude is based on Geographic coordinate system.
     * @param id The dock id
     * @param name This is the new name of the dock.
     * @param lat This is the new latitude of the dock.
     * @param lon This is the new longitude of the dock.
     * */
    public void editDock(int id, String name, double lat, double lon){
        try{
            removeDock(id);
            Docking dock = new Docking(id,name,lat,lon);
            addDock(dock);
            returnDock(id).setName(name);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
