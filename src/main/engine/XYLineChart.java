package main.engine;

import java.io.*;

import main.database.Executor;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.ChartUtilities;

//Class for creating a chart for viewing revenue

/**
 * Class for creating a chart for viewing revenue
 */
public class XYLineChart {


    final DefaultCategoryDataset dataset = new DefaultCategoryDataset();

    DefaultCategoryDataset d1 = new DefaultCategoryDataset();
    DefaultCategoryDataset d2 = new DefaultCategoryDataset();

    /**
     * Method for making sure the generated chart is deleted, so the program always works with updated version
     */
    public void delete() {
        File fil = new File("XYLineChart.jpeg");
        if(fil.delete())
        {
            System.out.println("File deleted successfully");
        }
        else
        {
            System.out.println("Failed to delete the file");
        }
    }

    /**
     * Method for creating chart based on data from database, and saves it as a .jpeg for the GUI to use
     * @param year  int representing the year to view
     */
    public void makeChart(int year) {
        for (int i = 0; i < 12; i++) {
            String mnd = "";
            if (i == 0) { mnd = "January"; }
            if (i == 1) { mnd = "February"; }
            if (i == 2) { mnd = "March"; }
            if (i == 3) { mnd = "April"; }
            if (i == 4) { mnd = "May"; }
            if (i == 5) { mnd = "June"; }
            if (i == 6) { mnd = "July"; }
            if (i == 7) { mnd = "August"; }
            if (i == 8) { mnd = "September"; }
            if (i == 9) { mnd = "October"; }
            if (i == 10) { mnd = "November"; }
            if (i == 11) { mnd = "Desember";}
            dataset.addValue(0, "", "");
            dataset.addValue(Executor.getInn(year, i+1), "Income", mnd);
            dataset.addValue(Executor.getUt(year, i+1), "Expenses", mnd);
            dataset.addValue(0, "", "");
        }

        JFreeChart xylineChart = ChartFactory.createBarChart(
                "Revenue for " + year,
                "Month",
                "Kr,-",
                dataset,
                PlotOrientation.VERTICAL,
                true, true, false);

        int width = 1000;   /* Width of the image */
        int height = 480;  /* Height of the image */
        File XYChart = new File( "XYLineChart.jpeg" );
        try {
            ChartUtilities.saveChartAsJPEG( XYChart, xylineChart, width, height);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
