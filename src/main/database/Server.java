package main.database;

import main.objects.Bike;
import main.objects.Docking;

import java.util.ArrayList;

/**
 * class for creating a server to continuously update data in database
 */
public class Server {
    public static void main(String[] args) throws InterruptedException {
        try {
            while (true) {
                System.out.println("sending information to Database... ");
                Executor.powerUsageDock();
                Executor.bike_logg();
                System.out.println("sending complete!");
                Thread.sleep(60000);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
