
package main.database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * The Databasepool is a multiple connection class that is usually used when multiple threads are in use.
 * */

public class DatabasePool {
    private static ArrayBlockingQueue<Connection> pool = new ArrayBlockingQueue(9);
    private static final String dbDriver = "com.mysql.jdbc.Driver";
    private static String dbName = "jdbc:mysql://mysql.stud.iie.ntnu.no:3306/mathibra?autoReconnect=true&useSSL=false";
    private static String username = "mathibra";
    private static String password = "xIZWhTBQ";

    private static final String test_username = "williacj";
    private static final String test_password = "guxmQ4a0";
    private static final String test_dbName = "jdbc:mysql://mysql.stud.iie.ntnu.no:3306/williacj?autoReconnect=true&useSSL=false";
    public static boolean istest = false;

    /***
     * The consturctor creates 8 connections when its called. It is also used in the testing so it has a parameter that says if its a
     * testing databasepool or not
     * @see Executor
     */

    public DatabasePool() {
        System.out.println("Creating dbp");
        try{
            Class.forName(dbDriver);
            if (!istest) {
                for(int i = 0; i<8; i++) {
                    pool.add(DriverManager.getConnection(dbName, username, password));
                }
            } else {
                dbName = test_dbName;
                username = test_username;
                password = test_password;
                for(int i = 0; i<5; i++) {
                    pool.add(DriverManager.getConnection(dbName, username, password));
                }
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }

    }
    /**
     * This method takes a connection from the pool. It waits if it has to.
     * @return      the connection taken
     */
    public Connection take(){
        try{
            return pool.take();
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
    /**
     * This returns the connection to the pool and always needs to be used after a connection is taken ot it will be terminated.
     * @param con Connection from the take() method
     * */

    public void give(Connection con){
        pool.add(con);
    }
    /**
     * The method clears the whole pool.
     * */

    public void clearAll(){
        try{
            pool.clear();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
