package main.database;
import java.sql.Statement;
import main.engine.Email;
import main.engine.HashPasswords;
import main.objects.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Class for handling all connections with the database pool and database
 */

public class Executor {
    private static DatabasePool dbp = new DatabasePool();
    private static Statement stm;
    private static ResultSet res;
    private static HashPasswords hashPasswords = new HashPasswords();

    public static DatabasePool getDbp() {
        return dbp;
    }

    /**
     *@param name   a string with the name of the dock
     *@param con    Connection  a connection to the Database pool
     *@return       false or true based on if the object exists or not
     * Checks if dock exists.
     */
    private static boolean checkerDocking(String name, Connection con){
        try{
            stm = con.createStatement();
            String selectSentence ="SELECT * FROM Docking_station";
            res = stm.executeQuery(selectSentence);

            while(res.next()){
                if(res.getString("name").equalsIgnoreCase(name)){
                    return true;
                }
            }
            res.close();
            return false;

        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    /**
     *@param email  a string with the email of the admin
     *@param con    Connection  a connection to the Database pool
     *@return       false or true based on if the object exists or not
     * Checks if admin exists.
     */
    private static boolean checkerAdmin(String email, Connection con){//77
        boolean exist = false;
        try{
            stm = con.createStatement();
            String selectSentence ="SELECT * From Admin";
            res = stm.executeQuery(selectSentence);
            while(res.next()){
                if(res.getString("email").equals(email)){
                    exist = true;
                }
            }
            res.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return exist;
    }
    /**
     *@param name  a string with the email of the admin
     *@param con    Connection  a connection to the Database pool
     *@return       false or true based on if the object exists or not
     * Checks if type exists.
     */
    private static boolean checkerType(String name, Connection con){
        boolean exist = false;
        try{
            stm = con.createStatement();
            String selectSentence ="SELECT * From Type";
            res = stm.executeQuery(selectSentence);
            while(res.next()){
                if(res.getString("name").equals(name)){
                    exist = true;
                }
            }
            res.close();
            return exist;
        }catch (Exception e){
            e.printStackTrace();
        }
        return exist;
    }


    /**
     *@param name   a string with the name of a bike type
     *@return       the type id of the specified name
     * Converts a type name, and return the type id for that name
     */
    public static int nametoID(String name){
        Connection con = dbp.take();
        try{
            stm = con.createStatement();
            con.setAutoCommit(false);
            String selectSentence ="SELECT * From Type";
            res = stm.executeQuery(selectSentence);
            while(res.next()){
                if(res.getString("name").equalsIgnoreCase(name)){
                    return res.getInt("type_id");
                }
            }
            con.commit();
            res.close();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            dbp.give(con);
        }
        return -1;
    }

    /**
     *@param id  a number which represents a type id
     *@return       the name for the corresponding type id
     * Converts a type id, and returns the name of that type id
     */
    public static String idtoName(int id){
        Connection con = dbp.take();
        try{
            stm = con.createStatement();
            con.setAutoCommit(false);
            String selectSentence ="SELECT * From Type";
            res = stm.executeQuery(selectSentence);
            while(res.next()){
                if(res.getInt("type_id") == id){
                    return res.getString("name");
                }
            }
            res.close();
            con.commit();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            dbp.give(con);
        }
        return "";
    }

    /**
     * Method to make a new admin user. It checks if input contains an @ and if the email already have been used. If everything seems right it will send a password to the email entered.
     * @param email a String contaoning the email of the new user
     * @return      true or false based on if the creating went well or not
     */
    public static boolean newAdmin(String email){
        Connection con = dbp.take();

        if(!email.contains("@") || !email.contains(".")){
            System.out.println("please write a valid e-mail");
            return false;
        }
        if(checkerAdmin(email,con)){
            System.out.println("User already exists!");
            return false;
        }
        System.out.println(email);
        try{
            System.out.println("AutoCommit: " + con.getAutoCommit());
            con.setAutoCommit(false);
            Admin a1 = new Admin(email);
            Email one = new Email();
            one.sendEmail(a1);
            PreparedStatement stm = con.prepareStatement("INSERT INTO Admin(email, password) VALUES(?,?)");
            stm.setString(1,email);
            stm.setString(2,a1.getPassword());
            stm.execute();
            con.commit();
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
        finally {
            dbp.give(con);
        }
    }

    /**
     * Send new Password to specified user. It checks if the email that has been entered contains an @. It also checks that the admin with that email exists, and if so it sends a new password to that email.
     * @param email String containing the email of the user
     * @return      true or false based on if the request for a new password went well or not
     */
    public static   boolean newPassword(String email){
        Connection con = dbp.take();
        if(!checkerAdmin(email,con)){
            System.out.println("User Does'nt exist!");
            return false;
        }
        Email one = new Email();
        try{
            con.setAutoCommit(false);
            if(!email.contains("@") || !checkerAdmin(email,con) || email.equalsIgnoreCase(null)){
                System.out.println("Not a vaild e-mail input or the user does'nt exist");
                return false;
            }
            Admin a1 = new Admin(email);
            PreparedStatement stm = con.prepareStatement("UPDATE Admin SET password=? WHERE email=?");
            stm.setString(1,a1.getPassword());
            stm.setString(2,email);
            stm.execute();
            one.sendEmail(a1);
            con.commit();
            return true;
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            dbp.give(con);
        }
        return false;
    }

    /**
     * Method to change password. It checks if the email that has been entered contains an @.
     * It also checks that it exists an admin with that email, and if it does, it changes the password for that user to the password that is passed as the password parameter.
     * @param email     String containing the email of the user
     * @param password  String containing the new password for the specified user
     * @return          true or false based on if the password change went by smoothly or not
     */
    public static boolean changePassword(String email, String password){
        Connection con = dbp.take();
        try{
            con.setAutoCommit(false);
            if(!email.contains("@") || email.equalsIgnoreCase(null)){
                System.out.println("Not a vaild e-mail input");
                return false;
            }
            if(!checkerAdmin(email,con)){
                System.out.println("User does not exist!");
                return false;
            }
            String newPassword = hashPasswords.login(password);
            PreparedStatement stm = con.prepareStatement("UPDATE Admin SET password = ? WHERE email=?");
            stm.setString(1,newPassword);
            stm.setString(2,email);
            con.commit();
            stm.execute();
            return true;
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            dbp.give(con);
        }
        return false;
    }

    /**
     * Login for admin. Checks that the email exists, and that the password belongs to the email.
     * @param email     String containing the email of the user
     * @param password  String containing the new password for the specified user
     * @return          true or false based on if the login went well or not
     */
    public static   boolean loginA(String email, String password){
        Connection con = dbp.take();
        String hspws = hashPasswords.login(password);
        try{
            con.setAutoCommit(false);
            PreparedStatement stm = con.prepareStatement("SELECT * from Admin WHERE password =? && email=?");
            stm.setString(1,hspws);
            stm.setString(2,email);
            ResultSet rs = stm.executeQuery();
            while(rs.next()){
                System.out.println("login Done!");
                con.commit();
                return true;
            }
            System.out.println("login failed!");
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            dbp.give(con);
        }
        return false;
    }

    /**
     * Method for creating a new customer in the database. Customer is given a customer id, to make it easier to keep control on the customers instead of using the card number.
     * @param firstname     String containing the first name of the customer
     * @param surname       String containing the surname of the customer
     * @param cardNr        long containing the cardNr of the customer
     * @return              true or false based on if the creation of a new customer went by smoothly or not
     */
    public static   boolean newCustomer(String firstname, String surname,long cardNr ){
        Connection con = dbp.take();
        try{
            stm = con.createStatement();
            con.setAutoCommit(false);
            PreparedStatement stm = con.prepareStatement("INSERT INTO Customer(firstname, surname, card_number) VALUES(?,?,?)");
            stm.setString(1,firstname);
            stm.setString(2,surname);
            stm.setLong(3,cardNr);
            stm.execute();
            con.commit();
            return true;
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            dbp.give(con);
        }
        return false;
    }

    /**
     * Method for getting all customers from the database
     * @return  an arraylist of customers
     */
    public static   ArrayList<Customer> getallCustomers() {
        ArrayList<Customer> customers = new ArrayList<Customer>();
        Connection con = dbp.take();
        try {
            stm = con.createStatement();
            con.setAutoCommit(false);
            String statement1 = "SELECT * FROM Customer";
            stm.executeQuery(statement1);
            ResultSet rs = stm.executeQuery(statement1);
            while (rs.next()) {
                Customer customer = new Customer(rs.getString("firstname"), rs.getString("surname"),
                        rs.getLong("card_number"));
                customer.setCustomer_id(rs.getInt("customer_id"));
                customers.add(customer);
            }
            con.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dbp.give(con);
        }
        return customers;
    }

    /**
     * Method for making a new bike type.
     * @param name  String containing the name of the new bike type
     * @return      true if the creation went well, false if it failed
     */
    public static boolean newType(String name){
        Connection con = dbp.take();
        if(checkerType(name,con)){
            System.out.println("type already exist!");
            return false;
        }
        try{
            stm = con.createStatement();
            con.setAutoCommit(false);
            PreparedStatement stm = con.prepareStatement("INSERT INTO Type(name,status) VALUES(?,?)");
            stm.setString(1,name);
            stm.setInt(2,0);
            stm.execute();
            con.commit();
            return true;
        }catch (Exception e){
            e.printStackTrace();
        }
        finally {
            dbp.give(con);
        }
        return false;
    }

    /**
     * Method for editing a bike type
     * @param name  String containing the name of the type to edit
     * @param name1 String containing the name the name you want to change to
     * @return      true if change went well, false if it didn't
     */
    public static boolean editType(String name, String name1){
        Connection con = dbp.take();
        if(!checkerType(name,con)){
            System.out.println("type doesnt exist!");
            return false;
        }
        int id = nametoID(name);
        try{
            //con.setAutoCommit(false);
            stm = con.createStatement();
            PreparedStatement stm = con.prepareStatement("UPDATE Type SET name= ? WHERE type_id= ?");
            stm.setString(1,name1);
            stm.setInt(2,id);
            con.commit();
            stm.execute();
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
        finally {
            dbp.give(con);
        }
    }

    /**
     * Method for getting a type object from the database
     * @param name  String containing the name of the specified type
     * @return      the type object requested
     */
    public static   Type getSpesificType(String name) {
        return getAllTypes().stream().filter(type -> type.getName().equals(name)).findFirst().orElse(null);
    }

    /**
     * Method for deleting a type from the database
     * @param name  String containing the name of the type to delete
     * @return      true if deletion was a success, false if it failed
     */
    public static   boolean deleteType(String name) {
        Connection con = dbp.take();
        if(!checkerType(name,con)){
            System.out.println("The type doesnt exist");
            return false;
        }
        try {
            con.setAutoCommit(false);
            stm = con.createStatement();
            PreparedStatement stm = con.prepareStatement("UPDATE Type SET status = 1 where name =?");
            stm.setString(1,name);
            stm.execute();
            con.commit();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        finally {
            dbp.give(con);
        }
    }

    /**
     * Method for getting all bikes that need repair
     * @return  an arraylist of bike objects that need repair
     */
    public static   ArrayList<Bike> getAllRepairs() {
        ArrayList<Bike> bikes = new ArrayList<Bike>();
        Connection con = dbp.take();
        try {
            con.setAutoCommit(false);
            stm = con.createStatement();
            String statement1 = "SELECT * FROM Bike WHERE status = 4";
            stm.executeQuery(statement1);
            ResultSet rs = stm.executeQuery(statement1);
            while (rs.next()) {
                Bike newbike = new Bike(rs.getInt("bike_id"), rs.getDate("date_registered"), rs.getInt("price"),
                        rs.getInt("type_id"), rs.getDate("produced"));
                newbike.setPrice(rs.getInt("price"));
                newbike.setCharge(rs.getInt("charge"));
                newbike.setAnt_rep(rs.getInt("all_repairs"));
                newbike.setTot_trips(rs.getInt("total_trips"));
                newbike.setTot_km(rs.getInt("total_km"));
                newbike.setStatus(rs.getInt("status"));
                bikes.add(newbike);
            }
            con.commit();
            return bikes;
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            dbp.give(con);
        }
        return null;
    }

    /**
     * Method for updating current position and charge for a specified bike
     * @param id        int representing the bike id
     * @param lati      double representing latitude for the bike
     * @param longi     double representing longitude for the bike
     * @param charge    int representing charge percentage of the bike
     */
    public static void updateBike(int id, double lati, double longi, int charge) {
        Connection con = dbp.take();
        try{
            con.setAutoCommit(false);
            stm = con.createStatement();
            PreparedStatement stm = con.prepareStatement("INSERT INTO Bike_status VALUES (?,?,?,?,?)");
            stm.setInt(1, id);
            stm.setDouble(2, lati);
            stm.setDouble(3, longi);
            stm.setInt(4, charge);
            stm.setDate(5, dateNow());
            stm.execute();
            con.commit();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            dbp.give(con);
        }
    }

    /**
     * Method for creating a new docking
     * @param name  String containing name for the new dock
     * @param lat   double representing the latitude of the new dock
     * @param lon   double representing longitude of the new dock
     * @return      true if creation was successful, false if it wasn't
     */
    public static boolean newDocking(String name, double lat, double lon){
        Connection con = dbp.take();
        if(checkerDocking(name,con)){
            System.out.println("Docking already exist!");
            return false;
        }
        try {
            con.setAutoCommit(false);
            stm = con.createStatement();
            PreparedStatement stm = con.prepareStatement("INSERT INTO Docking_station(name,latitude, longitude,status) VALUES (?,?,?,?)");
            stm.setString(1,name);
            stm.setDouble(2,lat);
            stm.setDouble(3,lon);
            stm.setInt(4,0);
            stm.execute();
            con.commit();
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }finally {
            dbp.give(con);
        }
    }

    /**
     * Method for retrieving all current docking stations from the database
     * @return  an arraylist of docking station objects
     */
    public static   ArrayList<Docking> getAllDockings() {
        ArrayList<Docking> docks = new ArrayList<Docking>();
        Connection con = dbp.take();
        try {
            con.setAutoCommit(false);
            stm = con.createStatement();
            PreparedStatement stm = con.prepareStatement("SELECT * FROM Docking_station");
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Docking newDock = new Docking(rs.getString("name"), rs.getDouble("latitude"),
                        rs.getDouble("longitude"));
                newDock.setDock_id(rs.getInt("dock_id"));
                docks.add(newDock);
            }
            con.commit();
            return docks;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        finally {
            dbp.give(con);
        }
    }

    /**
     * Method for getting all bikes from a specific dock
     * @param dock_id   int representing the dock id to retrieve from
     * @return          an arraylist of bike objects from the desired docking station
     */
    public static ArrayList<Bike> getBikesFromDock(int dock_id) {
        ArrayList<Bike> bikes = new ArrayList<Bike>();
        Connection con = dbp.take();
        try {
            con.setAutoCommit(false);
            stm = con.createStatement();
            String statement1 = "SELECT * FROM Bike JOIN Bike_docked ON Bike.bike_id = Bike_docked.bike_id WHERE dock_id =" + dock_id;
            stm.executeQuery(statement1);
            ResultSet rs = stm.executeQuery(statement1);
            while (rs.next()){
                Bike newbike = new Bike(rs.getInt("bike_id"), rs.getDate("date_registered"), rs.getInt("price"),
                        rs.getInt("type_id"), rs.getDate("produced"));
                newbike.setPrice(rs.getInt("price"));
                newbike.setCharge(rs.getInt("charge"));
                newbike.setAnt_rep(rs.getInt("all_repairs"));
                newbike.setTot_trips(rs.getInt("total_trips"));
                newbike.setTot_km(rs.getInt("total_km"));
                newbike.setStatus(rs.getInt("status"));
                bikes.add(newbike);
            }
            con.commit();
            rs.close();
            return bikes;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        finally {
            dbp.give(con);
        }

    }

    /**
     * Method for getting the dock that the specified bike is docked in
     * @param bike_id   int representing the id of the bike
     * @return          an int representing the dock id that the bike is currently at
     */
    public static   int retriveDock(int bike_id){
        Connection con = dbp.take();
        int dock_id =0;
        try {
            con.setAutoCommit(false);
            stm = con.createStatement();
            String statement1 = "SELECT * FROM Bike_docked where bike_id =" + bike_id ;
            stm.executeQuery(statement1);
            ResultSet rs = stm.executeQuery(statement1);
            while (rs.next()) {
                return dock_id = rs.getInt("dock_id");
            }
            rs.close();
            con.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            dbp.give(con);
            return dock_id;
        }
    }

    /**
     * Method for retrieving all current bikes from the database
     * @return  an arraylist of bike objects
     */
    public static ArrayList<Bike> getAllBikes() {
        ArrayList<Bike> bikes = new ArrayList<Bike>();
        Connection con = dbp.take();
        try {
            stm = con.createStatement();
            con.setAutoCommit(false);
            String statement1 = "SELECT * FROM Bike";
            stm.executeQuery(statement1);
            ResultSet rs = stm.executeQuery(statement1);
            while (rs.next()) {
                Bike newbike = new Bike(rs.getInt("bike_id"), rs.getDate("date_registered"), rs.getInt("price"),
                        rs.getInt("type_id"), rs.getDate("produced"));
                newbike.setPrice(rs.getInt("price"));
                newbike.setCharge(rs.getInt("charge"));
                newbike.setAnt_rep(rs.getInt("all_repairs"));
                newbike.setTot_trips(rs.getInt("total_trips"));
                newbike.setTot_km(rs.getInt("total_km"));
                newbike.setStatus(rs.getInt("status"));
                bikes.add(newbike);
            }
            con.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            dbp.give(con);

        }
        return bikes;
    }

    /**
     * Method for getting number of repairs on bikes of a certain type
     * @param id    int representing the type id to check number of repairs for
     * @return      and int representing the number of repairs on the specified bike type
     */
    public static int getAntRepairs(int id) {
        Connection con = dbp.take();
        int count = 0;
        try {
            stm = con.createStatement();
            String statement1 = "SELECT * FROM Bike WHERE type_id = " + id;
            stm.executeQuery(statement1);
            ResultSet rs = stm.executeQuery(statement1);
            while (rs.next()) {
                count += rs.getInt("all_repairs");
            }
            return count;
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            dbp.give(con);
        }
        return -1;
    }

    /**
     * Get number of bikes rented per year, split by months
     * @param year      int representing the year to view
     * @param month     int for helping the graph maker separate the months
     * @return          int representing the number of rentals for a certain year and month
     * @see main.engine.LineChart
     */
    public static   int getAntUtleid(int year, int month) {
        Connection con = dbp.take();
        int count = 0;
        try {
            con.setAutoCommit(false);
            stm = con.createStatement();
            String statement1 = "SELECT * FROM  User_has_bike WHERE MONTH(date_rented) = " + month + " AND YEAR(date_rented) = " + year;
            stm.executeQuery(statement1);
            ResultSet rs = stm.executeQuery(statement1);
            rs.last();
            count = rs.getRow();
            rs.beforeFirst();
            con.commit();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            dbp.give(con);
            return count;
        }
    }

    /**
     * Method for getting income for a specified year and month
     * @param year      int representing year to view
     * @param month     int for helping graph maker separate the months
     * @return          int representing the income for specified year and month
     * @see main.engine.XYLineChart
     */
    public static   int getInn(int year, int month) {
        Connection con = dbp.take();
        int count = 0;
        try {
            con.setAutoCommit(false);
            stm = con.createStatement();
            String statement1 = "SELECT * FROM  User_has_bike WHERE MONTH(date_rented) = " + month + " AND YEAR(date_rented) = " + year;
            stm.executeQuery(statement1);
            ResultSet rs = stm.executeQuery(statement1);
            rs.last();
            count = rs.getRow();
            rs.beforeFirst();
            con.commit();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            dbp.give(con);
            return count*100;
        }
    }

    /**
     * Method for getting outcome for a specified year and month
     * @param year      int representing year to view
     * @param month     int for helping graph maker separate the months
     * @return          int representing the outcome for specified year and month
     * @see main.engine.XYLineChart
     */
    public static   int getUt(int year, int month) {
        Connection con = dbp.take();
        int sum = 0;
        try {
            con.setAutoCommit(false);
            stm = con.createStatement();
            String statement1 = "SELECT SUM(price) AS sum FROM Repairs WHERE MONTH(date_received) = " + month + " AND YEAR(date_received) = " + year;
            stm.executeQuery(statement1);
            ResultSet rs = stm.executeQuery(statement1);
            rs.last();
            sum = rs.getInt("sum");
            rs.beforeFirst();
            con.commit();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            dbp.give(con);
            return sum;
        }
    }

    /**
     * Methods for getting all known bike types from the database
     * @return  an arraylist of bike type objects
     */
    public static   ArrayList<Type> getAllTypes() {
        ArrayList<Type> types = new ArrayList<>();
        Connection con = dbp.take();
        try {
            con.setAutoCommit(false);
            stm = con.createStatement();
            String statement1 = "SELECT * FROM Type";
            stm.executeQuery(statement1);
            ResultSet rs = stm.executeQuery(statement1);
            while (rs.next()) {
                Type type = new Type(rs.getInt("type_id"), rs.getString("name"));
                types.add(type);
            }
            con.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            dbp.give(con);
            return types;
        }
    }

    /**
     * Method for getting all bikes currently rented
     * @return  an arraylist of bike objects that are currently being rented
     */
    public static   ArrayList<Bike> getRented() {
        ArrayList<Bike> rentedBikes = new ArrayList<Bike>();
        Connection con = dbp.take();
        try {
            con.setAutoCommit(false);
            stm = con.createStatement();
            String statement1 = "SELECT * FROM Bike JOIN User_has_bike ON Bike.bike_id = User_has_bike.bike_id";
            stm.executeQuery(statement1);
            ResultSet rs = stm.executeQuery(statement1);
            while (rs.next()) {
                Bike newbike = new Bike(rs.getDate("date_registered"), rs.getInt("price"),
                        rs.getInt("type_id"), rs.getDate("produced"));
                newbike.setPrice(rs.getInt("price"));
                newbike.setCharge(rs.getInt("charge"));
                newbike.setAnt_rep(rs.getInt("all_repairs"));
                newbike.setTot_trips(rs.getInt("total_trips"));
                newbike.setTot_km(rs.getInt("total_km"));
                rentedBikes.add(newbike);
            }
            con.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            dbp.give(con);
            return rentedBikes;
        }
    }

    /**
     * Method for editing a docking station
     * @param id        int representing id for the dock to be edited
     * @param name      String containing new name for the dock
     * @param lat       double representing new latitude for the dock
     * @param lon       double representing new longitude for the dock
     * @return          true if the edit went well, false if it did not
     */
    public static   boolean editDocking(int id, String name, double lat, double lon){
        Connection con = dbp.take();
        try{
            con.setAutoCommit(false);
            stm = con.createStatement();
            PreparedStatement stm = con.prepareStatement("UPDATE Docking_station SET name = ? , latitude = ? , longitude =  ?  WHERE dock_id =?");
            stm.setString(1,name);
            stm.setDouble(2,lat);
            stm.setDouble(3,lon);
            stm.setInt(4,id);
            stm.execute();
            con.commit();
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
        finally {
            dbp.give(con);
            return true;
        }
    }

    /**
     * Method for deleting a docking station
     * @param dock_id    int representing the id of the dock to be deleted
     * @return      true if deletion went well, false if it didn't
     */
    public static   boolean deleteDocking(int dock_id) {
        Connection con = dbp.take();
        try {
            con.setAutoCommit(false);
            stm = con.createStatement();
            PreparedStatement stm = con.prepareStatement("UPDATE Docking_station SET status = 1 where dock_id = ?");
            stm.setInt(1,dock_id);
            stm.execute();
            con.commit();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        finally {
            dbp.give(con);
        }
    }

    /**
     * Method for retrieving the dock where a bike was rented from when it started a trip
     * @param bike_id   int representing the id of the bike
     * @return          an int representing the id of the dock where the bike started
     */
    public static   int startDock(int bike_id){
        Connection con = dbp.take();
        try{
            con.setAutoCommit(false);
            stm = con.createStatement();
            PreparedStatement stm = con.prepareStatement("SELECT * FROM User_has_bike WHERE bike_id=?");
            stm.setInt(1,bike_id);
            stm.execute();
            ResultSet res = stm.executeQuery();
            while(res.next()){
                int startDock = res.getInt("start_dock");
                return startDock;
            }
            con.commit();
        }catch (Exception e){
            e.printStackTrace();
        }
        return -1;
    }


    /**
     * Method for getting a specified dock object from the database
     * @param name  String containing the name of the dock
     * @return      a dock object with the specified name
     */
    public static   Docking returnDockObj(String name){
        Connection con = dbp.take();

        try{
            con.setAutoCommit(false);
            stm = con.createStatement();
            PreparedStatement stm = con.prepareStatement("SELECT * FROM Docking_station WHERE name =?");
            stm.setString(1,name);
            stm.execute();
            ResultSet res = stm.executeQuery();
            while(res.next()){
                Docking newDock = new Docking(res.getString("name"), res.getDouble("latitude"),
                        res.getDouble("longitude"));
                newDock.setDock_id(res.getInt("dock_id"));
                dbp.give(con); // endret her
                return newDock;
            }
            con.commit();
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Method for creating a new bike and out it into the database
     * @param reg_date      a date representing when the bike was registered
     * @param price         int representing the price for the bike
     * @param type_id       int representing the id for the type of the new bike
     * @param prod_date     a date representing when the bike was produced
     * @return              true if the creation went well, false if not
     */
    public static   boolean newBike(Date reg_date, int price, int type_id, Date prod_date){
        Connection con = dbp.take();

        try{
            con.setAutoCommit(false);
            stm = con.createStatement();
            PreparedStatement stm = con.prepareStatement("INSERT INTO Bike(date_registered, price, charge, produced, " +
                    "all_repairs, total_trips, total_km, type_id, status) VALUES (?,?,?,?,?,?,?,?,?)");
            stm.setDate(1,reg_date);
            stm.setInt(2,price);
            stm.setInt(3,100);
            stm.setDate(4,prod_date);
            stm.setInt(5,0);
            stm.setInt(6,0);
            stm.setInt(7,0);
            stm.setInt(8,type_id);
            stm.setInt(9,5); // puts the bike in storage
            con.commit();
            stm.execute();
            dbp.give(con);
            return true;
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Method for editing a bike
     * @param id                int representing id for the bike to edit
     * @param price             int representing the new price for the bike
     * @param produced          date representing the updated production date for the bike
     * @param all_repairs       int representing the amount of times the bike has been repaired
     * @param total_trips       int representing the total number of trips the bike has been on
     * @param total_km          int representing the total amount of km traveled by the bike
     * @param type_id           int representing the id of the bike's type
     * @param status            int for representing the status of the bike. 0=deleted/inactive, 1=docked, 2=in repair, 3=being rented, 4=in need of repair and 5=in storage
     * @return                  true if changes were successfully made, false if not
     */
    public static   boolean editBike(int id , int price, Date produced, int all_repairs, int total_trips, int total_km, int type_id,int status){
        Connection con = dbp.take();
        try{
            con.setAutoCommit(false);
            stm = con.createStatement();
            PreparedStatement stm = con.prepareStatement("UPDATE Bike SET price = ?,produced=?, all_repairs= ?," +
                    " total_trips =?, total_km = ?, type_id =?, status =? WHERE Bike_id=?" );
            stm.setInt(1,price);
            stm.setDate(2,produced);
            stm.setInt(3,all_repairs);
            stm.setInt(4,total_trips);
            stm.setInt(5,total_km);
            stm.setInt(6,type_id);
            stm.setInt(7,status);
            stm.setInt(8,id);
            con.commit();
            stm.execute();
            dbp.give(con);
            return true;
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Method for deleting a bike from the database
     * @param id    int representing the id of the bike to be deleted
     * @return      true if deletion went well, false if not
     */
    public static   boolean deletebike(int id) {
        Connection con = dbp.take();
        try {
            con.setAutoCommit(false);
            PreparedStatement stm = con.prepareStatement("UPDATE Bike SET status = 0 WHERE bike_id =?");
            stm.setInt(1,id);
            con.commit();
            stm.execute();
            dbp.give(con);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Method to send a bike to repair. Changes status from need repair to repairing.
     * @param bike_id       int representing the id of the bike to be sent to repair
     * @param from_date     date representing the date from which the repair will start
     * @param reason        String containing the description of the repair
     * @return              true if the repair request was successful, false if not
     */
    public static   boolean addRepairBike(int bike_id, Date from_date, String reason){
        Connection con = dbp.take();
        try{
            con.setAutoCommit(false);
            stm = con.createStatement();
            PreparedStatement stm = con.prepareStatement("INSERT INTO Repairs (bike_id, date_sendt, repair_request) VALUES(?,?,?)");
            stm.setString(3,reason);
            stm.setDate(2,from_date);
            stm.setInt(1,bike_id);
            stm.execute();
            PreparedStatement stm1 = con.prepareStatement("UPDATE Bike set all_repairs = all_repairs +1 WHERE bike_id=?");
            stm1.setInt(1,bike_id);
            stm1.execute();
            PreparedStatement stm2 = con.prepareStatement("DELETE FROM Bike_docked WHERE bike_id= ?");
            stm2.setInt(1,bike_id);
            stm2.execute();
            PreparedStatement stm3 = con.prepareStatement("UPDATE Bike SET status =? where bike_id=?");
            stm3.setInt(1,2);
            stm3.setInt(2,bike_id);
            con.commit();
            stm3.execute();
            dbp.give(con);
            return true;
        }catch(Exception e){
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Method for finishing a bike which is on repair
     * @param id                        int representing the id of the repair
     * @param done                      date representing when the repair was done
     * @param repair_description        String containing the description of how the repair went
     * @param bike_id                   id representing the id of the bike finished repairing
     * @return                          true if the operation was successful, false if not
     */
    public static boolean doneRepair(int id, Date done, String repair_description, int bike_id){
        Connection con = dbp.take();
        try{
            con.setAutoCommit(false);
            PreparedStatement stm = con.prepareStatement("UPDATE Repairs SET date_received = ?, repair_description = ? WHERE repair_id =?");
            stm.setDate(1,done);
            stm.setString(2,repair_description);
            stm.setInt(3,id);
            stm.execute();
            PreparedStatement stm2 = con.prepareStatement("UPDATE Bike SET status =? WHERE bike_id=?");
            stm2.setInt(1,5);
            stm2.setInt(2,bike_id);
            con.commit();
            stm2.execute();
            dbp.give(con);
            return true;
        }catch(Exception e){
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Method for putting a bike in a dock
     * @param bike_id       int representing id of the bike to dock
     * @param dock_id       int representing the id of the dock to put the bike in
     * @return              true if the docking of the bike went well, false if not
     */
    public static boolean dockingBike(int bike_id, int dock_id){
        Connection con = dbp.take();

        try{
            con.setAutoCommit(false);
            PreparedStatement stm = con.prepareStatement("INSERT INTO Bike_docked (dock_id, bike_id, date_delivered) VALUES (?,?,?);");
            stm.setInt(1,dock_id);
            stm.setInt(2,bike_id);
            stm.setDate(3,dateNow());
            stm.executeUpdate();
            PreparedStatement stm2 = con.prepareStatement("DELETE FROM User_has_bike WHERE bike_id =?");
            stm2.setInt(1,bike_id);
            stm2.execute();
            PreparedStatement stm3 = con.prepareStatement("UPDATE Bike SET status = 1 AND total_trips = total_trips + 1 where bike_id=?");
            stm3.setInt(1,bike_id);
            con.commit();
            stm3.execute();
            dbp.give(con);
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Method for undocking a bike, and connecting it to a customer until the rental is done
     * @param bike_id       int representing the id for the bike to undock
     * @param cos_id        int representing the id for the customer that rented the bike
     * @param dock_id       int representing id of the dock to undock from
     * @param date          date representing when the bike was undocked
     * @return              true if the undocking was successful, false if not
     */
    public static   boolean unDockBike(int bike_id, int cos_id, int dock_id, Date date){
        Connection con = dbp.take();
        try{
            con.setAutoCommit(false);
            PreparedStatement stm = con.prepareStatement("DELETE FROM Bike_docked WHERE bike_id =?");
            stm.setInt(1,bike_id);
            stm.execute();
            PreparedStatement stm2 = con.prepareStatement("INSERT INTO User_has_bike(customer_id, bike_id, date_rented, start_dock) VALUES (?,?,?,?)");
            stm2.setInt(1,cos_id);
            stm2.setInt(2,bike_id);
            stm2.setDate(3,date);
            stm2.setInt(4,dock_id);
            stm2.execute();
            PreparedStatement stm3 = con.prepareStatement("UPDATE Bike SET status = 3 WHERE bike_id= ?");
            stm3.setInt(1,bike_id);
            con.commit();
            stm3.execute();
            dbp.give(con);
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Method for getting the bike with the highest charge. Useful for when deciding which bikes a customer can rent
     * @param dock_id       int representing the dock to get a bike from
     * @return              a bike objects with the highest charge percentage
     */
    public static  Bike bikeWithHighestCharge(int dock_id){
        Connection con = dbp.take();
        try{
            con.setAutoCommit(false);
            stm = con.createStatement();
            PreparedStatement stm = con.prepareStatement("SELECT max(charge),bike_id FROM Bike NATURAL JOIN Bike_docked WHERE dock_id =? GROUP BY bike_id");
            stm.setInt(1,dock_id);
            stm.execute();
            ResultSet res = stm.executeQuery();
            while(res.next()){
                for(Bike bike: getAllBikes()){
                    if(bike.getBike_id() == res.getInt("bike_id")){
                        return bike;
                    }
                }
            }
            con.commit();
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }


    /**
     * Method for updating and used when continuously changing the charging level of bikes
     * @return      true if the operation was executed successfully, false if not
     */
    public static boolean bike_logg(){
        Connection con = dbp.take();
        try{
            ArrayList<Bike> allBikes = getAllBikes();
            for(Bike bike: allBikes) {
                stm = con.createStatement();
                PreparedStatement stm = con.prepareStatement("INSERT INTO Bike_History(latitude, longitude, charging_level, date, bike_id) VALUES (?,?,?,?,?)");
                stm.setDouble(1, bike.getLatitude());
                stm.setDouble(2, bike.getLongitude());
                stm.setInt(3, bike.getCharge());
                stm.setTimestamp(4, timedatenow());
                stm.setInt(5, bike.getBike_id());
                stm.execute();
            }
            return true;
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            dbp.give(con);
        }
        return false;
    }

    /**
     * Method used when continuously update the power usage of a docking station, when charging bikes it has
     * @return      true if the operation was executed successfully, false if not
     */
    public static boolean powerUsageDock(){
        Connection con = dbp.take();
        try {
            ArrayList<Docking> allDocks = getAllDockings();
            for(Docking dock: allDocks) {
                double powerPerBike = 1.8;
                double dockPowerUsage = 0;
                int bikesCharging = 0;
                for(int i = 0; i < dock.getBikes().size(); i++){
                    if(dock.getBikes().get(i).getCharge() < 100){
                        bikesCharging++;
                    }
                }
                dockPowerUsage = powerPerBike * bikesCharging;
                stm = con.createStatement();
                PreparedStatement stm = con.prepareStatement("INSERT INTO Docking_station_history(date, name, power_usage, dock_id) VALUES (?,?,?,?)");
                stm.setTimestamp(1, timedatenow());
                stm.setString(2, dock.getName());
                stm.setDouble(3, dockPowerUsage);
                stm.setInt(4, dock.getDock_id());
                stm.execute();
            }
            return true;
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            dbp.give(con);
        }
        return false;
    }


    //Gets the status of the bike.
    /**
     * Method for setting the current status of a bike. Used for updating the position on the map
     * @param id                int representing the id of the bike tp be updated
     * @param lat               double representing the current latitude of the bike
     * @param lon               double representing the current longitude of the bike
     * @param charging_lvl      int representing the current charge percentage of the bike
     * @return                  true if the bike was successfully updated, false if not
     */
    public static   boolean bikeStatus(int id, double lat, double lon, int charging_lvl){
        Connection con = dbp.take();
        try{
            stm = con.createStatement();
            PreparedStatement stm = con.prepareStatement("INSERT INTO Bike_History(latitude, longitude, charging_level, date, bike_id) VALUES (?,?,?,?,?)" );
            stm.setDouble(1,lat);
            stm.setDouble(2,lon);
            stm.setInt(3,charging_lvl);
            stm.setTimestamp(4,timedatenow());
            stm.setInt(5,id);
            stm.execute();
            dbp.give(con);
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
        return false;
    }

    /**
     * Method for setting a bike's status to "need repair"
     * @param bike_id   int representing id of the bike to edit status of
     * @return          true if the status change of the bike went well, false if not
     */
    public static   boolean brokenBike(int bike_id){
        Connection con = dbp.take();
        try{
            con.setAutoCommit(false);
            PreparedStatement stm3 = con.prepareStatement("UPDATE Bike SET status = 4 WHERE bike_id = ?");
            stm3.setInt(1,bike_id);
            con.commit();
            stm3.execute();
            dbp.give(con);
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Method for getting the customer id for a customer object
     * @param customer  a customer object
     * @return          a number representing the id of the specified customer object
     */
    public static   int getCustomerID(Customer customer) {
        Connection con = dbp.take();
        try {
            con.setAutoCommit(false);
            PreparedStatement stm = con.prepareStatement("SELECT * FROM Customer WHERE card_number=? AND firstname =? AND surname=?");
            stm.setLong(1,customer.getCardNr());
            stm.setString(2,customer.getFirstname());
            stm.setString(3,customer.getSurname());
            ResultSet res = stm.executeQuery();
            while(res.next()) {
                int costumerID = res.getInt("customer_id");
                con.commit();
                dbp.give(con);
                return costumerID;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return -1;
    }

    /**
     * Method for getting the customer object for the specified customer id
     * @param customer_id       int representing the id for the customer
     * @return                  a customer object with id specified
     */
    public static Customer getCustomerObj(int customer_id){
        Connection con = dbp.take();
        try {
            con.setAutoCommit(false);
            PreparedStatement stm = con.prepareStatement("SELECT * FROM Customer WHERE customer_id=?");
            stm.setInt(1,customer_id);
            ResultSet res = stm.executeQuery();
            while(res.next()) {
                Customer cus = new Customer(res.getString("firstname"), res.getString("surname"),res.getLong("card_number"));
                con.commit();
                return cus;
            }
            return null;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Method for checking if a bike and a customer has a connection to each other already
     * @param Customer_id   int representing the id of the customer
     * @param Bike_id       int representing the id of the bike
     * @return              true if they are connected somehow, false if not
     */
    public static   boolean relationbetweencustandBike(int Customer_id, int Bike_id){
        Connection con = dbp.take();
        boolean exist = false;
        try {
            con.setAutoCommit(false);
            PreparedStatement stm = con.prepareStatement("SELECT * FROM User_has_bike WHERE customer_id =? and bike_id =?");
            stm.setInt(1,Customer_id);
            stm.setInt(2,Bike_id);
            ResultSet res = stm.executeQuery();
            con.commit();
            while(res.next()){
                exist = true;
            }
            return exist;
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            dbp.give(con);
        }
        return false;
    }

    /**
     * Method for getting the current time
     * @return  current date in java.sql.Date format
     */
    public static   java.sql.Date dateNow(){
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        return date;
    }

    private static Timestamp timedatenow(){
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        return timestamp;
    }


    /**
     * Method for removing all data from the database, use with caution
     * @return      true if the clear went well, false if it didn't
     */
    public static   boolean clearAll(){
        try{
            dbp.clearAll();
            return true;
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }
}