/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@Authors: Annebelle Almås, Mathias Braathen, William Chakroun Jacobsen & Terje Haugum
@Date submitted: 23.04.2018
@Summary: TakeN is where the bike nr pops up when the customer has payed for the bike. This is in norwegian.


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////**/
package main.ui.guiUser;
import main.ui.guiUser.DockOrTake.DockOrTake_Norwegian;

import javax.swing.*;
import java.awt.*;


import java.awt.Graphics;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TakeN extends JFrame {
    private JPanel contentPane;
    private static Font plainFont = new Font("Avenir", Font.PLAIN, 30);
    private static Font plainFontSmall = new Font("Avenir", Font.PLAIN, 20);
    private static Font boldFonts = new Font("Avenir", Font.PLAIN, 40);
    private static Font boldFont = new Font("Avenir", Font.BOLD, 50);
    private static Font bigFont = new Font("Avenir", Font.PLAIN, 200);
    private JPanel mal;
    private JPanel mal2;
    private JPanel mal3;
    int bike;
    Graphics g;


    public TakeN(int bike) {
        this.bike = bike;
        setTitle("Trondheim City Bike");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        contentPane = new JPanel();
        contentPane.setLayout(null);

        //Heading---------------------------------------------------------------------
        JLabel headding = new JLabel("Trondheim Citybikes");
        headding.setBounds(40, 40, 600, 60);
        headding.setFont(boldFont);
        //headding.setForeground(new Color(255,127,80));
        contentPane.add(headding);

        mal2 = new JPanel();
        mal2.setLayout(null);
        mal2.setBackground(new Color(255, 249, 244));
        mal2.setBounds(0, 0, 1500, 800);
        mal2.setVisible(true);

        mal = new JPanel();
        mal.setLayout(null);
        mal.setBackground(new Color(255, 127, 80));
        mal.setBounds(0, 0, 1500, 10);
        mal.setVisible(true);

        //Subheading---------------------------------------------------------------------
        JLabel sub = new JLabel("Ta sykkel nr:");
        sub.setBounds(500,100,400,100);
        sub.setFont(boldFonts);
        contentPane.add(sub);



        //Back ---------------------------------------------------------------------
        JButton backButton = new JButton("Tilbake");
        backButton.setFont(plainFontSmall);
        backButton.setBounds(40,100,100,40);

        backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new DockOrTake_Norwegian();
                setVisible(false);
                dispose();
            }
        });
        contentPane.add(backButton);
        //circle---------------------------------------------------------------------

        String biketext = Integer.toString(bike);


        Draw circle = new Draw(new Color(255, 127, 80), bigFont);
        circle.setText(biketext);
        circle.setBounds(425,200,400,400);
        contentPane.add(circle);



        //number---------------------------------------------------------------------
        JLabel nr = new JLabel(/*Her skal nommeret skrives inn*/);
        //nr.setBounds();
        nr.setFont(boldFonts);
        contentPane.add(nr);

        //Slutt-------------------------------------------------------------------------------------------------
        setContentPane(contentPane);
        setSize(1500,800);
        contentPane.add(mal);
        contentPane.add(mal2);
        setVisible(true);




    }

    public void draw(Graphics g){
        g.drawOval(500,150,100,100);
        g.setColor(new Color(255, 127, 80));
    }





    public static void main(String[]args){/*new TakeN();*/}
}


