/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@Authors: Annebelle Almås, Mathias Braathen, William Chakroun Jacobsen & Terje Haugum
@Date submitted: 23.04.2018
@Summary: In StartE the user can see where they are, switch to norwegian,
look at the terms for renting a bike, see how the machine works and rent a bike. This is in english.

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////**/
package main.ui.guiUser;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import main.engine.CustomerRents;
import main.ui.guiUser.DockOrTake.DockOrTake_English;
import main.engine.Maps;
import main.objects.Customer;
import main.objects.Docking;
import main.simulator.RandomGen;
import main.engine.cache.DockCache;

public class StartE extends JFrame{
    private JPanel contentPane;
    private static Font plainFont = new Font("Avenir", Font.PLAIN, 15);
    private static Font plainFontSmall = new Font("Avenir", Font.PLAIN, 20);
    private static Font boldFontBig = new Font("Avenir", Font.BOLD, 90);
    private static Font boldFont = new Font("Avenir", Font.BOLD, 50);
    private JPanel mal2;
    private JPanel mal;
    CustomerRents rent = new CustomerRents();
    RandomGen random = new RandomGen();
    DockCache dc = new DockCache();
    List<Integer> bufferID = new ArrayList<>(dc.getCache().getGroupKeys("dockCache"));
    Docking dock = dc.returnDock(bufferID.get(0));

    public StartE(){
        setTitle("Trondheim City Bike");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        contentPane = new JPanel();
        contentPane.setLayout(null);

        mal2 = new JPanel();
        mal2.setLayout(null);
        mal2.setBackground(new Color(255, 249, 244));
        mal2.setBounds(0,0,1500,800);
        mal2.setVisible(true);

        mal = new JPanel();
        mal.setLayout(null);
        mal.setBackground(new Color(255,127,80));
        mal.setBounds(0,350,1500,100);
        mal.setVisible(true);


        //Heading---------------------------------------------------------------------
        JLabel headding = new JLabel("Trondheim Citybikes");
        headding.setBounds(250, 200, 1000, 80);
        headding.setFont(boldFontBig);
        headding.setForeground(Color.BLACK);
        contentPane.add(headding);

        //Docking---------------------------------------------------------------------
        JLabel station = new JLabel(dock.getName());
        station.setBounds(40, 40, 800, 60);
        station.setFont(boldFont);
        contentPane.add(station);

        //Terms of service---------------------------------------------------------------------
        JButton terms = new JButton("Terms of service");
        terms.setBounds(500, 300, 275, 25);
        terms.setForeground(Color.BLACK);
        terms.setFont(plainFontSmall);

        //Back to Main page---------------------------------------------------------------------
        JButton backButton = new JButton("Back");
        backButton.setFont(plainFontSmall);
        backButton.setBounds(40,100,100,40);

        backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new DockOrTake_English();
                setVisible(false);
                dispose();
            }
        });
        contentPane.add(backButton);

        //ActionListener
        terms.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                new TermsE();
                setVisible(false);
                dispose();
            }
        });

        contentPane.add(terms);

        JCheckBox check = new JCheckBox("Accept terms");
        check.setBounds(800,300,350,25);
        check.setFont(plainFontSmall);
        contentPane.add(check);



        JLabel swipe = new JLabel("MAKE PAYMENT ON THE RIGHT");
        swipe.setBounds(275,350,800,100);
        swipe.setFont(boldFont);
        swipe.setForeground(Color.BLACK);
        contentPane.add(swipe);


        //swich to norwegian
        JButton nor = new JButton ("Norsk");
        nor.setBounds(975,50,120,50);
        nor.setFont(plainFontSmall);

        nor.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new StartN();
                setVisible(false);
                dispose();
            }
        });

        contentPane.add(nor);


        //How I work
        JButton work = new JButton("How I work");
        work.setBounds(1100,50,170,50);
        work.setFont(plainFontSmall);

        work.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new WorkE();
                setVisible(false);
                dispose();
            }
        });
        contentPane.add(work);

        //MAP and extend map Button--------------------------------------------------------------------------------------------
        JPanel mapPane = new JPanel( new BorderLayout());
        mapPane.setBounds(700, 200, 400, 50);
        contentPane.add(mapPane);
        Maps m = new Maps(mapPane,40,460,280,280,dock);

        List<Integer> bufferID = new ArrayList<>(dc.getCache().getGroupKeys("dockCache"));
        Docking[] docksCache = new Docking[bufferID.size()];

        for(int z = 0; z<bufferID.size(); z++){
            docksCache[z] = dc.returnDock(bufferID.get(z));
        }

        m.addDocks(docksCache);
        JButton map_Button = new JButton("Bigger map");
        map_Button.setFont(plainFontSmall);
        map_Button.setBounds(330,680,200,50);

        map_Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new MapE(dock);
                setVisible(false);
                dispose();

            }
        });
        contentPane.add(map_Button);


        //Red marker---------------------------------------------------------------------------------------
        JLabel red = new JLabel("You are at the red dot!");
        red.setBounds(320, 460, 400, 50);
        red.setFont(plainFontSmall);
        contentPane.add(red);

        //----------------------------------------------------------------------------------------------------------
        JButton paymentButton = new JButton("Pay");
        paymentButton.setFont(plainFontSmall);
        paymentButton.setBounds(1150,365,75,75);

        paymentButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(check.isSelected()) {
                    Customer cust = random.customerGen();
                    int bikeid = rent.rent(dock, cust);
                    if (bikeid > 0) {
                        new TakeE(bikeid);
                        setVisible(false);
                        dispose();
                    }
                }else if(!check.isSelected()){
                    JFrame frame = new JFrame();
                    setLayout(new BorderLayout());
                    JPanel panel = new JPanel();
                    panel.setLayout(null);


                    JLabel text = new JLabel("You have to accept the terms befor you can rent a bike");
                    text.setFont(plainFontSmall);
                    text.setBounds(20,20,800,50);
                    panel.add(text);

                    JButton ok = new JButton("Ok");
                    ok.setFont(plainFont);
                    ok.setBounds(460,75,75,25);
                    panel.add(ok);

                    ok.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            frame.dispose();
                        }
                    });

                    frame.add(panel);
                    frame.setBounds(350,300,540,125);
                    frame.setVisible(true);

                }else{
                    JFrame frame = new JFrame();
                    setLayout(new BorderLayout());
                    JPanel panel = new JPanel();
                    panel.setLayout(null);


                    JLabel text = new JLabel("It is not posible to rent a bike");
                    text.setFont(plainFontSmall);
                    text.setBounds(20,20,800,50);
                    panel.add(text);

                    JButton ok = new JButton("Ok");
                    ok.setFont(plainFont);
                    ok.setBounds(460,75,75,25);
                    panel.add(ok);

                    ok.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            frame.dispose();
                        }
                    });

                    frame.add(panel);
                    frame.setBounds(350,300,540,125);
                    frame.setVisible(true);
                }
            }
        });
        contentPane.add(paymentButton);


        //Slutt-------------------------------------------------------------------------------------------------
        setContentPane(contentPane);
        setSize(1500,800);
        contentPane.add(mal);
        contentPane.add(mal2);

        setVisible(true);

    }

    public static void main (String []args){new StartE();}
}
