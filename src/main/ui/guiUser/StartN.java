/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@Authors: Annebelle Almås, Mathias Braathen, William Chakroun Jacobsen & Terje Haugum
@Date submitted: 23.04.2018
@Summary: In StartN the user can see where they are, switch to english,
look at the terms for renting a bike , see how the machine works and rent a bike. This is in norwegian.

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////**/
package main.ui.guiUser;
import main.engine.cache.DockCache;
import main.engine.CustomerRents;
import main.ui.guiUser.DockOrTake.DockOrTake_Norwegian;
import main.engine.Maps;
import main.objects.Customer;
import main.objects.Docking;
import main.simulator.RandomGen;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class StartN extends JFrame{
    private JPanel contentPane;
    private static Font plainFont = new Font("Avenir", Font.PLAIN, 15);
    private static Font plainFontSmall = new Font("Avenir", Font.PLAIN, 20);
    private static Font boldFontBig = new Font("Avenir", Font.BOLD, 90);
    private static Font boldFont = new Font("Avenir", Font.BOLD, 50);
    private JPanel mal2;
    private JPanel mal;
    private JFrame frame;
    private JPanel panel;
    CustomerRents rent = new CustomerRents();
    RandomGen random = new RandomGen();
    DockCache dc = new DockCache();
    List<Integer> bufferID = new ArrayList<>(dc.getCache().getGroupKeys("dockCache"));
    Docking dock = dc.returnDock(bufferID.get(0));


   public StartN() {
       frame = new JFrame();
       setTitle("Trondheim City Bike");
       setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       setLayout(new BorderLayout());
       contentPane = new JPanel();
       contentPane.setLayout(null);

       mal2 = new JPanel();
       mal2.setLayout(null);
       mal2.setBackground(new Color(255, 249, 244));
       mal2.setBounds(0,0,1500,800);
       mal2.setVisible(true);

       mal = new JPanel();
       mal.setLayout(null);
       mal.setBackground(new Color(255,127,80));
       mal.setBounds(0,350,1500,100);
       mal.setVisible(true);

       //Heading---------------------------------------------------------------------
       JLabel headding = new JLabel("Trondheim Citybikes");
       headding.setBounds(250, 200, 1000, 80);
       headding.setFont(boldFontBig);
       headding.setForeground(Color.BLACK);
       contentPane.add(headding);


       //Docking---------------------------------------------------------------------
       JLabel station = new JLabel(dock.getName().toUpperCase());
       station.setBounds(40, 40, 800, 60);
       station.setFont(boldFont);
       contentPane.add(station);

       //Terms of service---------------------------------------------------------------------
       JButton terms = new JButton("Vilkår for bruker");
       terms.setBounds(500, 300, 275, 25);
       terms.setForeground(Color.BLACK);
       terms.setFont(plainFontSmall);

       //Back to take------------------------------------------------------------------------
       //Back to Main page---------------------------------------------------------------------
       JButton backButton = new JButton("Tilbake");
       backButton.setFont(plainFontSmall);
       backButton.setBounds(40,100,100,40);

       backButton.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent e) {
               new DockOrTake_Norwegian();
               setVisible(false);
               dispose();
           }
       });
       contentPane.add(backButton);

       //ActionListener
       terms.addActionListener(new ActionListener(){
           public void actionPerformed(ActionEvent e) {
               new TermsN();
               setVisible(false);
               dispose();
           }
       });

       contentPane.add(terms);

       JCheckBox check = new JCheckBox("Godta vilkår");
       check.setBounds(800,300,350,25);
       check.setFont(plainFontSmall);
       contentPane.add(check);


       JLabel swipe = new JLabel("BETAL TIL HØYRE");
       swipe.setBounds(450,350,700,100);
       swipe.setFont(boldFont);
       swipe.setForeground(Color.BLACK);
       contentPane.add(swipe);

       //swich to english
       JButton nor = new JButton ("English");
       nor.setBounds(875,50,120,50);
       nor.setFont(plainFontSmall);

       nor.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent e) {
               new StartE();
               setVisible(false);
               dispose();
           }
       });
       contentPane.add(nor);

       //How I work
       JButton work = new JButton("Hvordan jeg fungerer");
       work.setBounds(1000,50,250,50);
       work.setFont(plainFontSmall);

       work.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent e) {
               new WorkN();
               setVisible(false);
               dispose();
           }
       });

       contentPane.add(work);


       JLabel red = new JLabel("Du er ved den røde prikken!");
       red.setBounds(320, 460, 400, 50);
       red.setFont(plainFontSmall);
       contentPane.add(red);


       //Paybutton so we kan simulate that a person pay because we dont hav det pay system the payment wil always go throgh
       JButton paymentButton = new JButton("Betal");
       paymentButton.setFont(plainFontSmall);
       paymentButton.setBounds(1150,365,75,75);

       paymentButton.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent e) {
               if(check.isSelected()) {
                   Customer cust = random.customerGen();
                   int bikeid = rent.rent(dock,cust);
                   if(bikeid > 0) {
                       new TakeN(bikeid);
                       setVisible(false);
                       dispose();
                   }else{
                       panel = new JPanel();
                       panel.setLayout(null);
                       setLayout(new BorderLayout());
                       panel.setVisible(true);



                       JLabel text = new JLabel("Det er ikke mulig å leie en sykkel");
                       text.setFont(plainFontSmall);
                       text.setBounds(20,20,800,50);
                       panel.add(text);

                       JButton ok = new JButton("Ok");
                       ok.setFont(plainFont);
                       ok.setBounds(480,75,50,25);
                       panel.add(ok);

                       JPanel color = new JPanel();
                       color.setLayout(null);
                       color.setBackground(new Color(255, 249, 244));
                       color.setBounds(350,300,540,125);
                       color.setVisible(true);



                       frame.setContentPane(panel);
                       panel.add(color);
                       frame.setBounds(350,300,540,125);
                       frame.setVisible(true);
                   }

               }else if(!check.isSelected()) {
                   panel = new JPanel();
                   panel.setLayout(null);
                   setLayout(new BorderLayout());
                   panel.setVisible(true);


                   JLabel text = new JLabel("Du må akseptere vilkårene før du kan leie en sykkel");
                   text.setFont(plainFontSmall);
                   text.setBounds(20, 20, 800, 50);
                   panel.add(text);

                   JButton ok = new JButton("Ok");
                   ok.setFont(plainFont);
                   ok.setBounds(480, 75, 50, 25);
                   panel.add(ok);

                   JPanel color = new JPanel();
                   color.setLayout(null);
                   color.setBackground(new Color(255, 249, 244));
                   color.setBounds(350, 300, 540, 125);
                   color.setVisible(true);

                   frame.setContentPane(panel);
                   panel.add(color);
                   frame.setBounds(350, 300, 540, 125);
                   frame.setVisible(true);
               }

           }
       });
       contentPane.add(paymentButton);


       //MAP and extend map Button--------------------------------------------------------------------------------------------
       JPanel mapPane = new JPanel( new BorderLayout());
       mapPane.setBounds(700, 200, 400, 50);
       contentPane.add(mapPane);
       Maps m = new Maps(mapPane,40,460,280,280, dock);

       List<Integer> bufferID = new ArrayList<>(dc.getCache().getGroupKeys("dockCache"));
       Docking[] docksCache = new Docking[bufferID.size()];

       for(int z = 0; z<bufferID.size(); z++){
           docksCache[z] = dc.returnDock(bufferID.get(z));
       }

       m.addDocks(docksCache);

       JButton map_Button = new JButton("større kart ");
       map_Button.setFont(plainFontSmall);
       map_Button.setBounds(330,680,200,50);

       map_Button.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent e) {
               new MapN(dock);
               setVisible(false);
               dispose();

           }
       });
       contentPane.add(map_Button);


       //Slutt-------------------------------------------------------------------------------------------------
       this.setContentPane(contentPane);
       this.setSize(1500,800);
       contentPane.add(mal);
       contentPane.add(mal2);
       this.setVisible(true);
   }

    public  static void main(String[]args){new StartN();}
}
