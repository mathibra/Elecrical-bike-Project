/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@Authors: Annebelle Almås, Mathias Braathen, William Chakroun Jacobsen & Terje Haugum
@Date submitted: 23.04.2018
@Summary: Her the user can get a big picture of the city with docking stations. This is in english.

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////**/
package main.ui.guiUser;
import main.engine.cache.BikeCache;
import main.engine.cache.DockCache;
import main.engine.Maps;
import main.objects.Docking;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class MapE extends JFrame{
    private JPanel contentPane;
    private static Font plainFont = new Font("Avenir", Font.PLAIN, 30);
    private static Font plainFontSmall = new Font("Avenir", Font.PLAIN, 20);
    private static Font boldFontSmall = new Font("Avenir", Font.PLAIN, 40);
    private static Font boldFont = new Font("Avenir", Font.BOLD, 50);
    private JPanel mal;
    private JPanel mal2;
    private DockCache dc = new DockCache();
    private BikeCache bc = new BikeCache();
    private static Docking dock;



    public MapE(Docking dock){
        this.dock = dock;
        setTitle("Trondheim City Bike");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        contentPane = new JPanel();
        contentPane.setLayout(null);

        //Heading---------------------------------------------------------------------
        JLabel headding = new JLabel("Trondheim Citybikes");
        headding.setBounds(40, 40, 600, 60);
        headding.setFont(boldFont);
        contentPane.add(headding);

        mal2 = new JPanel();
        mal2.setLayout(null);
        mal2.setBackground(new Color(255, 249, 244));
        mal2.setBounds(0,0,1500,800);
        mal2.setVisible(true);

        mal = new JPanel();
        mal.setLayout(null);
        mal.setBackground(new Color(255,127,80));
        mal.setBounds(0,0,1500,10);
        mal.setVisible(true);




        //Map
        JPanel mapPane = new JPanel( new BorderLayout());
        Maps m = new Maps(mapPane,0,125,1500,675, this.dock);

        java.util.List<Integer> bufferID = new ArrayList<>(dc.getCache().getGroupKeys("dockCache"));
        Docking[] docksCache = new Docking[bufferID.size()];

        for(int z = 0; z<bufferID.size(); z++){
            docksCache[z] = dc.returnDock(bufferID.get(z));
        }
        m.addDocks(docksCache);


        //switch to norwegian
        JButton nor = new JButton ("Norsk");
        nor.setBounds(1000,50,120,50);
        nor.setFont(plainFontSmall);

        nor.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new MapN(null);
                setVisible(false);
                dispose();
            }
        });
        contentPane.add(nor);

        //main.ui.gui_admin.Main
        JButton main = new JButton("Back");
        main.setBounds(1125,50,120,50);
        main.setFont(plainFontSmall);

        main.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new StartE();
                setVisible(false);
                dispose();
            }
        });
        contentPane.add(main);

        //Slutt-------------------------------------------------------------------------------------------------
        setContentPane(contentPane);
        setSize(1500,800);
        contentPane.add(mal);
        contentPane.add(mal2);
        contentPane.add(mapPane);
        setVisible(true);

    }
    public static void main(String[]args){new MapE(dock);}
}
