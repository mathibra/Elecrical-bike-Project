/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@Authors: Annebelle Almås, Mathias Braathen, William Chakroun Jacobsen & Terje Haugum
@Date submitted: 23.04.2018
@Summary: Text is where the text of terms and how the machine works is placed.

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////**/
package main.ui.guiUser;
public class Text {



    //English terms-------------------------------------------------------------------------------------------------------------------------------------
    public String termsE(){
        String termE = "DummyText\n" +
                "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.\n" +
                "Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\n" +
                "Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.\nNulla consequat massa quis enim. Donec pede justo, \n" +
                "fringilla vel, aliquet nec, vulputate eget, arcu. \n" +
                "In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.\nNullam dictum felis eu pede mollis pretium.\n" +
                "Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.\n\n" +

                "Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.\n" +
                "Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet.\n" +
                "Quisque rutrum.Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. \n" +
                "Maecenas tempus,tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc,\n" +
                "blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut \n" +
                "libero venenatis faucibus.Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh.\n" +
                "Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,\n\n"+

                "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.\n" +
                "Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\n" +
                "Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.\nNulla consequat massa quis enim. \n" +
                "Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. \n" +
                "In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.\nNullam dictum felis eu pede mollis pretium.\n" +
                "Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.\n\n" +

                "Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.\n" +
                "Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet.\n" +
                "Quisque rutrum.Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. \n" +
                "Maecenas tempus,tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc,\n" +
                "blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut \n" +
                "libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh.\n" +
                "Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,\n\n\n\n";;

        return  termE;
    }





    //Norwegian terms-------------------------------------------------------------------------------------------------------------------------------------
    public String termsN(){
        String termN = "DummyText\n" +
                "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.\n" +
                "Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\n" +
                "Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.\nNulla consequat massa quis enim. Donec pede justo, \n" +
                "fringilla vel, aliquet nec, vulputate eget, arcu. \n" +
                "In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.\nNullam dictum felis eu pede mollis pretium.\n" +
                "Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.\n\n" +

                "Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.\n" +
                "Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet.\n" +
                "Quisque rutrum.Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. \n" +
                "Maecenas tempus,tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc,\n" +
                "blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut \n" +
                "libero venenatis faucibus.Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh.\n" +
                "Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,\n\n"+

                "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.\n" +
                "Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\n" +
                "Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.\nNulla consequat massa quis enim. \n" +
                "Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. \n" +
                "In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.\nNullam dictum felis eu pede mollis pretium.\n" +
                "Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.\n\n" +

                "Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.\n" +
                "Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet.\n" +
                "Quisque rutrum.Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. \n" +
                "Maecenas tempus,tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc,\n" +
                "blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut \n" +
                "libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh.\n" +
                "Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,\n\n\n\n";
        return termN;
    }




    //English how if works-------------------------------------------------------------------------------------------------------------------------------------
    public String worksE(){
        String workE = "DummyText\n" +
                "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.\n" +
                "Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\n" +
                "Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.\nNulla consequat massa quis enim. Donec pede justo, \n" +
                "fringilla vel, aliquet nec, vulputate eget, arcu. \n" +
                "In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.\nNullam dictum felis eu pede mollis pretium.\n" +
                "Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.\n\n" +

                "Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.\n" +
                "Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet.\n" +
                "Quisque rutrum.Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. \n" +
                "Maecenas tempus,tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc,\n" +
                "blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut \n" +
                "libero venenatis faucibus.Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh.\n" +
                "Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,\n\n"+

                "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.\n" +
                "Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\n" +
                "Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.\nNulla consequat massa quis enim. \n" +
                "Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. \n" +
                "In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.\nNullam dictum felis eu pede mollis pretium.\n" +
                "Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.\n\n" +

                "Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.\n" +
                "Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet.\n" +
                "Quisque rutrum.Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. \n" +
                "Maecenas tempus,tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc,\n" +
                "blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut \n" +
                "libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh.\n" +
                "Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,\n\n\n\n";;
        return workE;
    }




    //Norwegian how it works-------------------------------------------------------------------------------------------------------------------------------------
    public String worksN(){
        String workN = "DummyText\n" +
                "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.\n" +
                "Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\n" +
                "Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.\nNulla consequat massa quis enim. Donec pede justo, \n" +
                "fringilla vel, aliquet nec, vulputate eget, arcu. \n" +
                "In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.\nNullam dictum felis eu pede mollis pretium.\n" +
                "Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.\n\n" +

                "Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.\n" +
                "Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet.\n" +
                "Quisque rutrum.Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. \n" +
                "Maecenas tempus,tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc,\n" +
                "blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut \n" +
                "libero venenatis faucibus.Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh.\n" +
                "Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,\n\n"+

                "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.\n" +
                "Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\n" +
                "Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.\nNulla consequat massa quis enim. \n" +
                "Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. \n" +
                "In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.\nNullam dictum felis eu pede mollis pretium.\n" +
                "Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.\n\n" +

                "Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.\n" +
                "Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet.\n" +
                "Quisque rutrum.Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. \n" +
                "Maecenas tempus,tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc,\n" +
                "blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut \n" +
                "libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh.\n" +
                "Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,\n\n\n\n";;

        return workN;
    }


    public String termsOfUseE() {
        String termsOfService = "When renting a bike you accept to pay a fee on 5000 kr if the bike is not returned.\n\n\n"  +
                "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.\n" +
                "Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\n" +
                "Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.\nNulla consequat massa quis enim. Donec pede justo, \n" +
                "fringilla vel, aliquet nec, vulputate eget, arcu. \n" +
                "In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.\nNullam dictum felis eu pede mollis pretium.\n" +
                "Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.\n\n" +

                "Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.\n" +
                "Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet.\n" +
                "Quisque rutrum.Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. \n" +
                "Maecenas tempus,tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc,\n" +
                "blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut \n" +
                "libero venenatis faucibus.Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh.\n" +
                "Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,\n\n"+

                "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.\n" +
                "Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\n" +
                "Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.\nNulla consequat massa quis enim. \n" +
                "Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. \n" +
                "In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.\nNullam dictum felis eu pede mollis pretium.\n" +
                "Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.\n\n" +

                "Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.\n" +
                "Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet.\n" +
                "Quisque rutrum.Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. \n" +
                "Maecenas tempus,tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc,\n" +
                "blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut \n" +
                "libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh.\n" +
                "Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,\n\n\n\n";;;
        return termsOfService;
    }


    public String termsOfUseN() {
        String termsOfService = "Ved leie av sykkel aksepterer du å betale 5000 kr hvis sykkelen ikke blir returnert.\n\n\n"  +
                "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.\n" +
                "Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\n" +
                "Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.\nNulla consequat massa quis enim. Donec pede justo, \n" +
                "fringilla vel, aliquet nec, vulputate eget, arcu. \n" +
                "In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.\nNullam dictum felis eu pede mollis pretium.\n" +
                "Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.\n\n" +

                "Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.\n" +
                "Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet.\n" +
                "Quisque rutrum.Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. \n" +
                "Maecenas tempus,tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc,\n" +
                "blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut \n" +
                "libero venenatis faucibus.Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh.\n" +
                "Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,\n\n"+

                "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.\n" +
                "Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\n" +
                "Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.\nNulla consequat massa quis enim. \n" +
                "Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. \n" +
                "In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.\nNullam dictum felis eu pede mollis pretium.\n" +
                "Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.\n\n" +

                "Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.\n" +
                "Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet.\n" +
                "Quisque rutrum.Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. \n" +
                "Maecenas tempus,tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc,\n" +
                "blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut \n" +
                "libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh.\n" +
                "Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,\n\n\n\n";;;
        return termsOfService;
    }


}
