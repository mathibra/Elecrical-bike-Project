/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@Authors: Annebelle Almås, Mathias Braathen, William Chakroun Jacobsen & Terje Haugum
@Date submitted: 23.04.2018
@Summary: Draws the circle/rectangle and puts det bike nr in the circle. This is the bike the customer shall take.

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////**/
package main.ui.guiUser;
import javax.swing.*;
import java.awt.*;

    public class Draw extends JPanel{

        private String text = "";
        private Color borderColor;
        private Font textFont;
        private static int BORDER_THICKNESS = 15;

        public Draw(Color col, Font f) {

            borderColor = col;
            textFont = f;
            setLayout(null);

        }

        public void setText(String text) {
            this.text = text;
        }

        public void paint(Graphics g) {

            g.setColor(borderColor);


            // Variant 4
            Graphics2D g2 = (Graphics2D)g;
            g2.setStroke(new BasicStroke(BORDER_THICKNESS));
            //g2.drawOval(BORDER_THICKNESS, BORDER_THICKNESS, getWidth()-(2*BORDER_THICKNESS+1), getHeight()-(2*BORDER_THICKNESS+1));
            g.drawRoundRect(0, 0, getWidth()-1, getHeight()-1, 25, 25);
            //g.fillOval(0, 0, getWidth()-1, getHeight()-1);
            drawCenteredText(g2, getWidth()/2, getHeight()/2);

        }

        private void drawCenteredText(Graphics g, int x, int y) {

            g.setFont(textFont);
            g.setColor(Color.BLACK);

            // Find the size of string s in font f in the current Graphics context g.
            FontMetrics fm = g.getFontMetrics();
            java.awt.geom.Rectangle2D rect = fm.getStringBounds(text, g);

            int textHeight = (int) (rect.getHeight());
            int textWidth = (int) (rect.getWidth());

            // Find the top left and right corner
            int cornerX = x - (textWidth / 2);
            int cornerY = y - (textHeight / 2) + fm.getAscent();

            g.drawString(text, cornerX, cornerY);  // Draw the string.
        }


    }


