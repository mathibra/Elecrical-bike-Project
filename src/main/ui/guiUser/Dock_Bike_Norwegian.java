/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@Authors: Annebelle Almås, Mathias Braathen, William Chakroun Jacobsen & Terje Haugum
@Date submitted: 23.04.2018
@Summary: In DockOrTake_Norwegian the user can choose to rent a bike or park a bike. This is in norwegian.

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////**/
package main.ui.guiUser;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import main.database.Executor;
import main.engine.cache.BikeCache;
import main.engine.CustomerRents;
import main.ui.guiUser.DockOrTake.DockOrTake_Norwegian;
import main.objects.Bike;
import main.objects.Customer;
import main.objects.Docking;
import main.simulator.RandomGen;
import main.engine.cache.DockCache;

public class Dock_Bike_Norwegian extends JFrame{
    private JPanel contentPane;
    private static Font plainFont = new Font("Avenir", Font.PLAIN, 30);
    private static Font plainFontSmall = new Font("Avenir", Font.PLAIN, 20);
    private static Font boldFontBig = new Font("Avenir", Font.BOLD, 90);
    private static Font boldFont = new Font("Avenir", Font.BOLD, 50);
    private JPanel mal2;
    private JPanel mal;
    CustomerRents rent = new CustomerRents();
    RandomGen random = new RandomGen();

    DockCache dc = new DockCache();
    BikeCache bc = new BikeCache();
    List<Integer> bufferID = new ArrayList<>(dc.getCache().getGroupKeys("dockCache"));
    Docking dock = dc.returnDock(bufferID.get(0));

    public Dock_Bike_Norwegian(){
        this.dock = dock;
        setTitle("Trondheim City Bike");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        contentPane = new JPanel();
        contentPane.setLayout(null);

        mal2 = new JPanel();
        mal2.setLayout(null);
        mal2.setBackground(new Color(255, 249, 244));
        mal2.setBounds(0,0,1500,800);
        mal2.setVisible(true);

        mal = new JPanel();
        mal.setLayout(null);
        mal.setBackground(new Color(255,127,80));
        mal.setBounds(0,350,1500,100);
        mal.setVisible(true);


        //Heading---------------------------------------------------------------------
        JLabel headding = new JLabel("Trondheim Citybikes");
        headding.setBounds(250, 200, 1000, 80);
        headding.setFont(boldFontBig);
        headding.setForeground(Color.BLACK);
        contentPane.add(headding);

        //Docking---------------------------------------------------------------------
        JLabel station = new JLabel(dock.getName());
        station.setBounds(40, 40, 800, 60);
        station.setFont(boldFont);
        contentPane.add(station);

        //Heading---------------------------------------------------------------------
        JLabel info = new JLabel("Skriv inn din ID og sykkelen din sin ID");
        info.setBounds(450, 355, 1000, 80);
        info.setFont(plainFont);
        info.setForeground(Color.BLACK);
        contentPane.add(info);
        //Back to Main page---------------------------------------------------------------------
        JButton backButton = new JButton("Tilbake");
        backButton.setFont(plainFontSmall);
        backButton.setBounds(40,100,100,40);

        backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new DockOrTake_Norwegian();
                setVisible(false);
                dispose();
            }
        });
        contentPane.add(backButton);
        //Customer--------------------------------------------------------------------
        JLabel nameText = new JLabel("Din ID:");
        nameText.setBounds(300,470,200,40);
        nameText.setFont(plainFontSmall);
        contentPane.add(nameText);

        JTextField nameWrite = new JTextField();
        nameWrite.setBounds(450,470,150,40);
        nameWrite.setFont(plainFontSmall);
        contentPane.add(nameWrite);
        //Bike--------------------------------------------------------------------
        JLabel bikeText = new JLabel("Sykkel ID:");
        bikeText.setBounds(700,470,200,40);
        bikeText.setFont(plainFontSmall);
        contentPane.add(bikeText);

        JTextField bikeWrite = new JTextField();
        bikeWrite.setBounds(800,470,150,40);
        bikeWrite.setFont(plainFontSmall);
        contentPane.add(bikeWrite);

        //Dock--------------------------------------------------------------------------
        JButton dock_your_bike = new JButton("Parker uten kvittering");
        dock_your_bike.setFont(plainFontSmall);
        dock_your_bike.setBounds(700,530,300,75);

        dock_your_bike.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String customer_id = nameText.getText();
                String bike = bikeText.getText();

                int cust_id = Integer.parseInt(customer_id);
                int bike_id = Integer.parseInt(bike);

                if(bike.equalsIgnoreCase("") || customer_id.equalsIgnoreCase("")){
                    System.out.print("Please have the correct inputs");
                    new DockOrTake_Norwegian();
                }

                if(Executor.relationbetweencustandBike(cust_id,bike_id) && dock.getAntDocked() <= 50){
                    int startDock_id = Executor.startDock(bike_id);
                    Docking startDock = dc.returnDock(startDock_id);
                    Executor.dockingBike(bike_id,dock.getDock_id());
                    dc.returnDock(dock.getDock_id()).addBike(bc.returnBike(bike_id));
                    Customer c1 = Executor.getCustomerObj(cust_id);
                    Bike b1 = bc.returnBike(bike_id);
                    rent.receipt(startDock,dock,c1,b1);
                    new DockOrTake_Norwegian();
                    setVisible(false);
                    dispose();
                }else if(Executor.relationbetweencustandBike(cust_id,bike_id) && dock.getAntDocked() > 50){
                    System.out.println("Det er ikke mer plass i parkeringen: " + dock.getName());
                } else{
                    System.out.println("det er ingen sammenheng " + customer_id + " og " + bike_id);
                }
                new DockOrTake_Norwegian();
                setVisible(false);
                dispose();
            }
        });
        contentPane.add(dock_your_bike);

        //take--------------------------------------------------------------------------
        JButton rent_a_bike = new JButton("parker uten kvittering");
        rent_a_bike.setFont(plainFontSmall);
        rent_a_bike.setBounds(380,530,300,75);

        rent_a_bike.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String customer_id = nameWrite.getText();
                String bike = bikeWrite.getText();
                int cust_id = Integer.parseInt(customer_id);
                int bike_id = Integer.parseInt(bike);

                if(Executor.relationbetweencustandBike(cust_id,bike_id) && dock.getAntDocked() <= 50){
                    Executor.dockingBike(bike_id,dock.getDock_id());
                    dc.returnDock(dock.getDock_id()).addBike(bc.returnBike(bike_id));

                }else if(Executor.relationbetweencustandBike(cust_id,bike_id) && dock.getAntDocked() > 50){
                    System.out.println("Det er ikke mer plass i parkeringen: " + dock.getName());
                } else{
                    System.out.println("det er ingen sammenheng " + customer_id + " og " + bike_id);
                }
                new DockOrTake_Norwegian();
                setVisible(false);
                dispose();
            }
        });
        contentPane.add(rent_a_bike);



        //Slutt-------------------------------------------------------------------------------------------------
        setContentPane(contentPane);
        setSize(1500,800);
        contentPane.add(mal);
        contentPane.add(mal2);

        setVisible(true);

    }

    public static void main (String []args){new Dock_Bike_Norwegian();}
}