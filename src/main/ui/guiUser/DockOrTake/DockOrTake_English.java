/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@Authors: Annebelle Almås, Mathias Braathen, William Chakroun Jacobsen & Terje Haugum
@Date submitted: 23.04.2018
@Summary:

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////**/
package main.ui.guiUser.DockOrTake;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import main.engine.CustomerRents;
import main.ui.guiUser.Dock_bike_English;
import main.ui.guiUser.StartE;
import main.objects.Docking;
import main.simulator.RandomGen;
import main.engine.cache.DockCache;

public class DockOrTake_English extends JFrame{
    private JPanel contentPane;
    private static Font plainFont = new Font("Avenir", Font.PLAIN, 30);
    private static Font plainFontSmall = new Font("Avenir", Font.PLAIN, 20);
    private static Font boldFontBig = new Font("Avenir", Font.BOLD, 90);
    private static Font boldFont = new Font("Avenir", Font.BOLD, 50);
    private JPanel mal2;
    private JPanel mal;
    CustomerRents rent = new CustomerRents();
    RandomGen random = new RandomGen();
    DockCache dc = new DockCache();
    List<Integer> bufferID = new ArrayList<>(dc.getCache().getGroupKeys("dockCache"));
    Docking dock = dc.returnDock(bufferID.get(0));

    public DockOrTake_English(){
        setTitle("Trondheim City Bike");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        contentPane = new JPanel();
        contentPane.setLayout(null);

        mal2 = new JPanel();
        mal2.setLayout(null);
        mal2.setBackground(new Color(255, 249, 244));
        mal2.setBounds(0,0,1500,800);
        mal2.setVisible(true);

        mal = new JPanel();
        mal.setLayout(null);
        mal.setBackground(new Color(255,127,80));
        mal.setBounds(0,350,1500,100);
        mal.setVisible(true);


        //Heading---------------------------------------------------------------------
        JLabel headding = new JLabel("Trondheim Citybikes");
        headding.setBounds(250, 200, 1000, 80);
        headding.setFont(boldFontBig);
        headding.setForeground(Color.BLACK);
        contentPane.add(headding);

        //Docking---------------------------------------------------------------------
        JLabel station = new JLabel(dock.getName());
        station.setBounds(40, 40, 800, 60);
        station.setFont(boldFont);
        contentPane.add(station);


        //Dock--------------------------------------------------------------------------
        JButton dock_your_bike = new JButton("Dock your bike");
        dock_your_bike.setFont(plainFontSmall);
        dock_your_bike.setBounds(700,365,300,75);

        dock_your_bike.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                        new Dock_bike_English();
                        setVisible(false);
                        dispose();

            }
        });
        contentPane.add(dock_your_bike);

        //take--------------------------------------------------------------------------
        JButton rent_a_bike = new JButton("Rent a bike");
        rent_a_bike.setFont(plainFontSmall);
        rent_a_bike.setBounds(380,365,300,75);

        rent_a_bike.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new StartE();
                setVisible(false);
                dispose();

            }
        });
        contentPane.add(rent_a_bike);

        //swich to norwegian
        JButton nor = new JButton ("Norsk");
        nor.setBounds(1000,50,120,50);
        nor.setFont(plainFontSmall);

        nor.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new DockOrTake_Norwegian();
                setVisible(false);
                dispose();
            }
        });

        contentPane.add(nor);



        //Slutt-------------------------------------------------------------------------------------------------
        setContentPane(contentPane);
        setSize(1500,800);
        contentPane.add(mal);
        contentPane.add(mal2);

        setVisible(true);

    }

    public static void main (String []args){new DockOrTake_English();}
}
