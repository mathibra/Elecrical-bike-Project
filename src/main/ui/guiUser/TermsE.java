/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@Authors: Annebelle Almås, Mathias Braathen, William Chakroun Jacobsen & Terje Haugum
@Date submitted: 23.04.2018
@Summary: Her the terms of service is. If a customer wants to rent a bike they have to accept terms of service.
It will tell the customer about the responsibility they have and the consequences by not following the terms.
This is in english.

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////**/
package main.ui.guiUser;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TermsE extends JFrame{
    private JPanel contentPane;
    private static Font plainFont = new Font("Avenir", Font.PLAIN, 30);
    private static Font plainFontSmall = new Font("Avenir", Font.PLAIN, 20);
    private static Font boldFontSmall = new Font("Avenir", Font.BOLD, 30);
    private static Font boldFont = new Font("Avenir", Font.BOLD, 50);
    private JPanel mal;
    private JPanel mal2;
    Text t = new Text();

    public TermsE(){
        setTitle("Trondheim City Bike");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        contentPane = new JPanel();
        contentPane.setLayout(null);

        //Heading---------------------------------------------------------------------
        JLabel headding = new JLabel("Trondheim Citybikes");
        headding.setBounds(40, 40, 600, 60);
        headding.setFont(boldFont);
        //headding.setForeground(new Color(255,127,80));
        contentPane.add(headding);

        mal2 = new JPanel();
        mal2.setLayout(null);
        mal2.setBackground(new Color(255, 249, 244));
        mal2.setBounds(0,0,1500,800);
        mal2.setVisible(true);

        mal = new JPanel();
        mal.setLayout(null);
        mal.setBackground(new Color(255,127,80));
        mal.setBounds(0,0,1500,10);
        mal.setVisible(true);

        //Subheadding
        JLabel work = new JLabel("TERMS OF SERVICE:");
        work.setBounds(50,225,300,40);
        work.setFont(plainFont);
        contentPane.add(work);


        //swich to norwegian
        JButton nor = new JButton ("Norsk");
        nor.setBounds(1000,50,120,50);
        nor.setFont(plainFontSmall);

        nor.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new TermsN();
                setVisible(false);
                dispose();
            }
        });

        contentPane.add(nor);


        //main.ui.gui_admin.Main
        JButton main = new JButton("Back");
        main.setBounds(1125,50,120,50);
        main.setFont(plainFontSmall);

        main.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new StartE();
                setVisible(false);
                dispose();
            }
        });
        contentPane.add(main);


        // Text field
        JTextArea text = new JTextArea();
        text.setText(t.termsOfUseE());
        text.setEditable(false);
        JScrollPane scroll = new JScrollPane(text);
        scroll.setBounds(50,275,900,400);
        contentPane.add(scroll);

        //Slutt-------------------------------------------------------------------------------------------------
        setContentPane(contentPane);
        setSize(1500,800);
        contentPane.add(mal);
        contentPane.add(mal2);
        setVisible(true);



    }

    public static void main(String[]args){new TermsE();}
}
