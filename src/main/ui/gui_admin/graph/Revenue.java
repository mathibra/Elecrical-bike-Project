package main.ui.gui_admin.graph;
import main.engine.XYLineChart;

import java.awt.*;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.swing.JFrame;

//Generates a chart based on the year chosen in the "RevenueValg" menu, and views it
public class Revenue extends JFrame{
    private static Font plainFont = new Font("Avenir", Font.PLAIN, 30);
    private static Font boldFont = new Font("Avenir", Font.BOLD, 50);
    private JPanel mal;
    private JPanel mal2;
    static int year;
    BufferedImage img;

    public Revenue(int year) throws IOException {

        JPanel contentPane;
        XYLineChart lc = new XYLineChart();
        lc.delete();
        this.repaint();
        revalidate();
        repaint();
        this.year = year;
        lc.makeChart(year);
        setTitle("Trondheim city graph");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLayout(new BorderLayout());
        contentPane = new JPanel();
        contentPane.setLayout(null);


        mal2 = new JPanel();
        mal2.setLayout(null);
        mal2.setBackground(new Color(255,250,250));
        mal2.setBounds(0,0,1500,800);
        mal2.setVisible(true);

        mal = new JPanel();
        mal.setLayout(null);
        mal.setBackground(new Color(255,127,80));
        mal.setBounds(0,120,1500,5);
        mal.setVisible(true);



        //Heading---------------------------------------------------------------------
        JLabel headding = new JLabel("Trondheim Citybikes");
        headding.setBounds(40, 40, 600, 60);
        headding.setFont(boldFont);
        contentPane.add(headding);

        //Subheading---------------------------------------------------------------------
        JLabel subHeadding = new JLabel("GRAPH");
        subHeadding.setBounds(900, 40, 600, 60);
        subHeadding.setFont(boldFont);
        contentPane.add(subHeadding);

//        JLabel del = new JLabel("COST OF BIKES:");
//        del.setBounds(50,225,350,40);
//        del.setFont(plainFont);
//        contentPane.add(del);




        //Back to main page---------------------------------------------------------------------
        JButton back = new JButton("Back to Graph");
        back.setBounds(50, 150, 250, 50);
        back.setFont(plainFont);

        back.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                new Graph();
                setVisible(false);
                dispose();
            }
        });

        contentPane.add(back);


        JPanel panel = new JPanel();
        panel.setBounds(250, 225, 1000, 480);
        img = ImageIO.read(new File("XYLineChart.jpeg"));
        ImageIcon icon = new ImageIcon(img);
        JLabel label = new JLabel();
        label.setIcon(icon);

        panel.add(label);
        contentPane.add(panel);


        setContentPane(contentPane);
        setSize(1500,800);
        setVisible(true);
        contentPane.add(mal);
        contentPane.add(mal2);
    }

    public void flush() {
        img.flush();
    }

    public static void main(String[] args) {
        try {
            new Revenue(year);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}