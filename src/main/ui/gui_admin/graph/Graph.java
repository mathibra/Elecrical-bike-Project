package main.ui.gui_admin.graph;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.JLabel;

//main.ui.gui_admin.Main graph menu, for accessing the other graphs
public class Graph extends JFrame{
    private JPanel contentPane;
    private static Font plainFont = new Font("Avenir", Font.PLAIN, 30);
    private static Font boldFont = new Font("Avenir", Font.BOLD, 50);
    private JPanel mal;
    private JPanel mal2;

    public Graph(){
        setTitle("Trondheim city graph");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        contentPane = new JPanel();
        contentPane.setLayout(null);


        mal2 = new JPanel();
        mal2.setLayout(null);
        mal2.setBackground(new Color(255,250,250));
        mal2.setBounds(0,0,1500,800);
        mal2.setVisible(true);

        mal = new JPanel();
        mal.setLayout(null);
        mal.setBackground(new Color(255,127,80));
        mal.setBounds(0,120,1500,5);
        mal.setVisible(true);


        //Heading---------------------------------------------------------------------
        JLabel headding = new JLabel("Trondheim Citybikes");
        headding.setBounds(40, 40, 600, 60);
        headding.setFont(boldFont);
        contentPane.add(headding);

        //Subheading---------------------------------------------------------------------
        JLabel subHeadding = new JLabel("GRAPH");
        subHeadding.setBounds(900, 40, 600, 60);
        subHeadding.setFont(boldFont);
        contentPane.add(subHeadding);

        //Back to main page---------------------------------------------------------------------
        JButton back = new JButton("Main");
        back.setBounds(50, 150, 200, 50);
        back.setFont(plainFont);

        back.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                new main.ui.gui_admin.Main();
                setVisible(false);
                dispose();
            }
        });

        contentPane.add(back);

        //Cost of bikes---------------------------------------------------------------------
        JButton cost = new JButton("Revenue");
        cost.setBounds(450, 250, 350, 50);
        cost.setFont(plainFont);

        String[] name = new String[10];
        for(int i = 0; i < 10; i++){
            int hjelp = 2009+i;
            name[i] = String.valueOf(hjelp);
        }

        JComboBox box = new JComboBox(name);
        box.setFont(plainFont);
        box.setBounds(30,300,150,70);

        contentPane.add(box);

        JLabel del = new JLabel("Please specify year:");
        del.setBounds(30,225,350,40);
        del.setFont(plainFont);
        contentPane.add(del);

        //Actionlistener
        cost.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                String typ = (String)box.getSelectedItem();
                int ar = Integer.parseInt(typ);
                try {
                    Revenue ut = new Revenue(ar);
                    ut.flush();
                } catch (Exception er) {
                    er.printStackTrace();
                }
                typ = null;
                setVisible(false);
                dispose();
            }
        });

        contentPane.add(cost);



        //Powerusage of a bike---------------------------------------------------------------------
        JButton powerPerBike = new JButton("Repairs per type");
        powerPerBike.setBounds(450, 350, 350, 50);
        powerPerBike.setFont(plainFont);

        powerPerBike.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                try {
                    new RepairGraph();
                } catch (Exception er) {
                    er.printStackTrace();
                }

                setVisible(false);
                dispose();
            }
        });

        contentPane.add(powerPerBike);

        //total KM vs. days"---------------------------------------------------------------------
        JButton km = new JButton("Number of rentals");
        km.setBounds(450, 450, 350, 50);
        km.setFont(plainFont);

        km.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                String typ = (String)box.getSelectedItem();
                int ar = Integer.parseInt(typ);
                try {
                    UtleieGraph ut = new UtleieGraph(ar);
                    ut.flush();
                } catch (Exception er) {
                    er.printStackTrace();
                }
                setVisible(false);
                dispose();
            }
        });
        contentPane.add(km);



        //Slutt-------------------------------------------------------------------------------------------------
        setContentPane(contentPane);
        setSize(1500,800);
        setVisible(true);
        contentPane.add(mal);
        contentPane.add(mal2);

    }


    //main.ui.gui_admin.Main---------------------------------------------------------------------
    public static void main(String[]args){ new Graph(); }
}