package main.ui.gui_admin;/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@Authors: Annebelle Almås, Mathias Braathen, William Chakroun Jacobsen & Terje Haugum
@Date submitted: 23.04.2018
@Summary: In main.ui.gui_admin.Main admin can se map over docks and bikes. There are buttons to go to Bike, Graph, docking station,
change password and refresh button for map.

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////**/
//package main.ui.gui_admin;

import main.database.Executor;
import main.engine.Maps;
import main.engine.cache.AdminCache;
import main.engine.cache.BikeCache;
import main.engine.cache.DockCache;
import main.objects.Bike;
import main.objects.Docking;
import main.ui.gui_admin.admin_package.ChangePassword;
import main.ui.gui_admin.admin_package.NewUser;
import main.ui.gui_admin.docking_package.DockStation;
import main.ui.gui_admin.graph.Graph;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JFrame;


public class Main extends JFrame{
    private JPanel contentPane;
    private static Font plainFont = new Font("Avenir", Font.PLAIN, 30);
    private static Font plainFontSmall = new Font("Avenir", Font.PLAIN, 20);
    private static Font boldFontSmall = new Font("Avenir", Font.BOLD, 30);
    private static Font boldFont = new Font("Avenir", Font.BOLD, 50);
    private JPanel mal;
    private JPanel mal2;
    private JPanel maps;

    private DockCache dc = new DockCache();
    private BikeCache bc = new BikeCache();
    private AdminCache ac = new AdminCache();

    public Main(){
        setTitle("Trondheim City Bike");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        contentPane = new JPanel();
        contentPane.setLayout(null);


        mal2 = new JPanel();
        mal2.setLayout(null);
        mal2.setBackground(new Color(255,250,250));
        mal2.setBounds(0,0,1500,800);
        mal2.setVisible(true);

        mal = new JPanel();
        mal.setLayout(null);
        mal.setBackground(new Color(255,127,80));
        mal.setBounds(0,120,1500,5);
        mal.setVisible(true);


        //Heading---------------------------------------------------------------------
        JLabel headding = new JLabel("Trondheim Citybikes");
        headding.setBounds(40, 40, 600, 60);
        headding.setFont(boldFont);
        contentPane.add(headding);

        //Bikes---------------------------------------------------------------------
        JButton bikes = new JButton("Bikes");
        bikes.setBounds(200, 250, 300, 50);
        bikes.setFont(plainFont);

        //ActionListener(Hva som skjer når du trykker på knappen)
        bikes.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                new main.ui.gui_admin.bike_package.Bike();
                setVisible(false);
                dispose();
            }
        });

        contentPane.add(bikes);



        //Docking station---------------------------------------------------------------------
        JButton docingStation = new JButton("Docking station");
        docingStation.setBounds(200, 325, 300, 50);
        docingStation.setFont(plainFont);

        docingStation.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                new DockStation();
                setVisible(false);
                dispose();
            }
        });

        contentPane.add(docingStation);


        //Graphs---------------------------------------------------------------------
        JButton graphs = new JButton("Graphs");
        graphs.setBounds(200, 400, 300, 50);
        graphs.setFont(plainFont);

        graphs.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                new Graph();
                setVisible(false);
                dispose();
            }
        });
        contentPane.add(graphs);

        //Register new user---------------------------------------------------------------------
        JButton user =  new JButton("New user");
        user.setBounds(200,475,300,50);
        user.setFont(plainFont);

        user.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                new NewUser();
                setVisible(false);
                dispose();
            }
        });
        contentPane.add(user);


        //change the password
        JButton changePassword = new JButton("Change password");
        changePassword.setBounds(200,550,300,50);
        changePassword.setFont(plainFont);

        changePassword.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                new ChangePassword();
                setVisible(false);
                dispose();
            }
        });
        contentPane.add(changePassword);


        //Map---------------------------------------------------------------------

        JLabel mapLabel = new JLabel("Position to bikes and docking stations");
        mapLabel.setBounds(700, 200, 400, 50);
        mapLabel.setFont(plainFontSmall);
        contentPane.add(mapLabel);

        JPanel mapPane = new JPanel( new BorderLayout());
        mapPane.setBounds(700, 200, 400, 50);
        contentPane.add(mapPane);
        Maps m = new Maps(mapPane,700,250,400,400);


        List<Integer> bufferID = new ArrayList<>(bc.getCache().getGroupKeys("bikeCache"));
        Bike[] bikes_array = new Bike[bufferID.size()];

        for(int i = 0; i < bufferID.size(); i++){
            bikes_array[i] = bc.returnBike(bufferID.get(i));
        }

        List<Integer> bufferID_D = new ArrayList<>(dc.getCache().getGroupKeys("dockCache"));
        Docking[] dock_array = new Docking[bufferID_D.size()];

        for(int i = 0; i < bufferID_D.size(); i++){
            dock_array[i] = dc.returnDock(bufferID_D.get(i));
        }

        m.addBikes(bikes_array);
        m.addDocks(dock_array);

        //Refresh map---------------------------------------------------------------------
        JButton refresh =  new JButton("Refresh map");
        refresh.setBounds(1100,600,150,50);
        refresh.setFont(plainFontSmall);

        refresh.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){

                ArrayList<Bike> bikes = new ArrayList<>(Executor.getAllBikes());
                ArrayList<Docking> docks = new ArrayList<>(Executor.getAllDockings());
                Docking[] dockings = new Docking[docks.size()];
                Bike[] bike = new Bike[bikes.size()];

                bikes.toArray(bike);
                docks.toArray(dockings);

                m.updateBikes(bike);
                refresh.setVisible(true);

            }
        });
        contentPane.add(refresh);


        ImageIcon i = new ImageIcon("BikeIcon.png");
        JLabel l = new JLabel(i);
        l.setVisible(true);
        l.setBounds(800,50,50,50);
        contentPane.add(l);

        //Logout-------------------------------------------------------------------------------------------------
        JButton logout =  new JButton("Logout");
        logout.setBounds(25,700,100,40);
        logout.setFont(plainFontSmall);

        logout.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                new Login();
                setVisible(false);
                dispose();
            }
        });
        contentPane.add(logout);


        //Slutt-------------------------------------------------------------------------------------------------
        setContentPane(contentPane);
        setSize(1500,800);
        setVisible(true);
        contentPane.add(mal);
        contentPane.add(mal2);
    }


    //main.ui.gui_admin.Main---------------------------------------------------------------------
    public static void main(String[]args){
        try
        {
            Main main = new Main();
        } catch (NullPointerException ne) {
            ne.printStackTrace();
        }

    }
}