/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@Authors: Annebelle Almås, Mathias Braathen, William Chakroun Jacobsen & Terje Haugum
@Date submitted: 23.04.2018
@Summary: In Terms admin can read the terms they have to accept to make a new admin user.

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////**/
package main.ui.gui_admin;
import main.ui.guiUser.Text;
import main.ui.gui_admin.admin_package.NewUser;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.JLabel;



public class Terms extends JFrame {
    private JPanel contentPane;
    private static Font plainFont = new Font("Avenir", Font.PLAIN, 30);
    private static Font plainFontSmall = new Font("Avenir", Font.PLAIN, 20);
    private static Font boldFont = new Font("Avenir", Font.BOLD, 50);
    private static Font plainFontSmall10 = new Font("Helvetica", Font.PLAIN, 15);
    private JPanel mal;
    private JPanel mal2;
    Text t = new Text();

    public Terms() {
        setTitle("Trondheim city bike");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        contentPane = new JPanel();
        contentPane.setLayout(null);


        mal2 = new JPanel();
        mal2.setLayout(null);
        mal2.setBackground(new Color(255,250,250));
        mal2.setBounds(0,0,1500,800);
        mal2.setVisible(true);

        mal = new JPanel();
        mal.setLayout(null);
        mal.setBackground(new Color(255,127,80));
        mal.setBounds(0,120,1500,5);
        mal.setVisible(true);



        //Heading---------------------------------------------------------------------
        JLabel headding = new JLabel("Trondheim Citybikes");
        headding.setBounds(40, 40, 800, 60);
        headding.setFont(boldFont);
        contentPane.add(headding);

        //Subheading---------------------------------------------------------------------
        JLabel subHeadding = new JLabel("NEW USER");
        subHeadding.setBounds(900, 40, 600, 60);
        subHeadding.setFont(boldFont);
        contentPane.add(subHeadding);

        JLabel del = new JLabel("Terms of service:");
        del.setBounds(50,225,350,40);
        del.setFont(plainFont);
        contentPane.add(del);


        //Back to main page---------------------------------------------------------------------
        JButton back = new JButton("Back to new user");
        back.setBounds(50, 150, 250, 50);
        back.setFont(plainFont);

        back.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                new NewUser();
                setVisible(false);
                dispose();
            }
        });
        contentPane.add(back);

        //Text---------------------------------------------------------------------
        // Text field
        JTextArea text = new JTextArea();
        text.setText(t.termsE());
        text.setEditable(false);
        JScrollPane scroll = new JScrollPane(text);
        scroll.setBounds(300,250,900,450);
        contentPane.add(scroll);


        //Slutt-------------------------------------------------------------------------------------------------
        setContentPane(contentPane);
        setSize(1500,800);
        setVisible(true);
        contentPane.add(mal);
        contentPane.add(mal2);
    }

    public static void main(String[]args){new Terms();}
}