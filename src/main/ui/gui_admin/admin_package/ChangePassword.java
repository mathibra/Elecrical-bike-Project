/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@Authors: Annebelle Almås, Mathias Braathen, William Chakroun Jacobsen & Terje Haugum
@Date submitted: 23.04.2018
@Summary: The admin can change password her;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////**/
package main.ui.gui_admin.admin_package;


import main.database.Executor;
import main.engine.cache.AdminCache;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ChangePassword extends JFrame {
        private JPanel contentPane;
        private static Font plainFont = new Font("Avenir", Font.PLAIN, 30);
        private static Font plainFontSmall = new Font("Avenir", Font.PLAIN, 20);
        private static Font boldFontSmall = new Font("Avenir", Font.BOLD, 30);
        private static Font boldFont = new Font("Avenir", Font.BOLD, 50);
        private JPanel mal;
        private JPanel mal2;

        AdminCache ac = new AdminCache();

        public ChangePassword() {
            String userEmail = ac.returnAdmin(1);
            setTitle("Trondheim city bike login");
            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            setLayout(new BorderLayout());
            contentPane = new JPanel();
            contentPane.setLayout(null);


            mal2 = new JPanel();
            mal2.setLayout(null);
            mal2.setBackground(new Color(255, 250, 250));
            mal2.setBounds(0, 0, 1500, 800);
            mal2.setVisible(true);

            mal = new JPanel();
            mal.setLayout(null);
            mal.setBackground(new Color(255, 127, 80));
            mal.setBounds(0, 120, 1500, 5);
            mal.setVisible(true);


            //Heading---------------------------------------------------------------------
            JLabel headding = new JLabel("Trondheim Citybikes");
            headding.setBounds(40, 40, 700, 60);
            headding.setFont(boldFont);
            contentPane.add(headding);

            //Subheading---------------------------------------------------------------------
            JLabel subHeadding = new JLabel("CHANGE PASSWORD");
            subHeadding.setBounds(700, 40, 600, 60);
            subHeadding.setFont(boldFont);
            contentPane.add(subHeadding);


            //Back to main page---------------------------------------------------------------------
            JButton back = new JButton("Main");
            back.setBounds(50, 150, 200, 50);
            back.setFont(plainFont);

            back.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e) {
                    new main.ui.gui_admin.Main();
                    setVisible(false);
                    dispose();
                }
            });

            contentPane.add(back);

            //New password---------------------------------------------------------------------
            //setBounds(x-akse, y-akse, størrelse på tekstområdet, tykkelse på tekstområde)

            JLabel passwordT1 = new JLabel("New password: ");
            passwordT1.setBounds(350, 250, 275, 40);
            passwordT1.setFont(plainFontSmall);
            contentPane.add(passwordT1);

            JPasswordField passwordW1 = new JPasswordField();
            passwordW1.setBounds(525, 250, 275, 40);

            contentPane.add(passwordW1);

            //Password---------------------------------------------------------------------
            JLabel passwordT2 = new JLabel("Confirm password: ");
            passwordT2.setBounds(350, 300, 200, 40);
            passwordT2.setFont(plainFontSmall);
            contentPane.add(passwordT2);

            JPasswordField passwordW2 = new JPasswordField();
            passwordW2.setBounds(525, 300, 275, 40);


            contentPane.add(passwordW2);


            //Login Button---------------------------------------------------------------------
            JButton loginButton = new JButton("Change");
            loginButton.setBounds(525, 350, 100, 40);
            loginButton.setFont(plainFontSmall);

            loginButton.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e){
                    String charsPass = String.valueOf(passwordW1.getPassword());
                    String charsPass1 = String.valueOf(passwordW2.getPassword());

                    if(charsPass.equals(charsPass1)){
                        Executor.changePassword(userEmail,charsPass);
                        System.out.println("The passwords are equal and are changed!");
                    }else{
                        System.out.println("Im sorry, the passwords are not equal!");
                    }

                    new main.ui.gui_admin.Main();
                    setVisible(false);
                    dispose();
                }

            });

            contentPane.add(loginButton);


            //Slutt-------------------------------------------------------------------------------------------------
            setContentPane(contentPane);
            setSize(1500,800);
            setVisible(true);
            contentPane.add(mal);
            contentPane.add(mal2);
        }

        // Fikser hva som skjer når man trykker på knappen
        //private class Listener();


        //main.ui.gui_admin.Main---------------------------------------------------------------------
        public static void main(String[]args){new ChangePassword(); }
}
