/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@Authors: Annebelle Almås, Mathias Braathen, William Chakroun Jacobsen & Terje Haugum
@Date submitted: 23.04.2018
@Summary: If admin forgot the password, they enter their username, and a new password are sent to them on email.

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////**/
package main.ui.gui_admin.admin_package;
import main.database.Executor;
import main.ui.gui_admin.Login;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ForgotPassword extends JFrame{

    private JPanel contentPane;
    private static Font plainFont = new Font("Avenir", Font.PLAIN, 30);
    private static Font plainFontSmall = new Font("Avenir", Font.PLAIN, 20);
    private static Font boldFontSmall = new Font("Avenir", Font.BOLD, 30);
    private static Font boldFont = new Font("Avenir", Font.BOLD, 50);
    private JPanel mal;
    private JPanel mal2;


    public ForgotPassword() {
        setTitle("Trondheim city bike login");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        contentPane = new JPanel();
        contentPane.setLayout(null);


        mal2 = new JPanel();
        mal2.setLayout(null);
        mal2.setBackground(new Color(255, 250, 250));
        mal2.setBounds(0, 0, 1500, 800);
        mal2.setVisible(true);

        mal = new JPanel();
        mal.setLayout(null);
        mal.setBackground(new Color(255, 127, 80));
        mal.setBounds(0, 120, 1500, 5);
        mal.setVisible(true);


        //Heading---------------------------------------------------------------------
        JLabel headding = new JLabel("Trondheim Citybikes");
        headding.setBounds(40, 40, 700, 60);
        headding.setFont(boldFont);
        contentPane.add(headding);

        //Subheading---------------------------------------------------------------------
        JLabel subHeadding = new JLabel("FORGOT PASSWORD");
        subHeadding.setBounds(700, 40, 600, 60);
        subHeadding.setFont(boldFont);
        contentPane.add(subHeadding);


        //Back to main page---------------------------------------------------------------------
        JButton back = new JButton("Login page");
        back.setBounds(50, 150, 200, 50);
        back.setFont(plainFont);

        back.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                new Login();
                setVisible(false);
                dispose();
            }
        });

        contentPane.add(back);


        //Username---------------------------------------------------------------------
        //setBounds(x-akse, y-akse, størrelse på tekstområdet, tykkelse på tekstområde)

        JLabel usernameText = new JLabel("Username: ");
        usernameText.setBounds(350, 250, 275, 40);
        usernameText.setFont(plainFontSmall);
        contentPane.add(usernameText);

        JTextField usernameWrite = new JTextField();
        usernameWrite.setBounds(500, 250, 275, 40);
        usernameWrite.setFont(plainFontSmall);

        contentPane.add(usernameWrite);


        //Login Button---------------------------------------------------------------------
        JButton loginButton = new JButton("Get new password");
        loginButton.setBounds(500, 300, 200, 40);
        loginButton.setFont(plainFontSmall);

        loginButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){

                String username = usernameWrite.getText();
                if (username.equals("")) {
                    System.out.println("Please add input in the Textfield");
                    username = null;
                }

                Executor.newPassword(username);

                new Login();
                setVisible(false);
                dispose();
                }

        });

        contentPane.add(loginButton);


        //Slutt-------------------------------------------------------------------------------------------------
        setContentPane(contentPane);
        setSize(1500,800);
        setVisible(true);
        contentPane.add(mal);
        contentPane.add(mal2);
    }




    //main.ui.gui_admin.Main---------------------------------------------------------------------
    public static void main(String[]args){new ForgotPassword(); }
}

