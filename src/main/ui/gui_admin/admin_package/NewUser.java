/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@Authors: Annebelle Almås, Mathias Braathen, William Chakroun Jacobsen & Terje Haugum
@Date submitted: 23.04.2018
@Summary: The admin kan make a new admin her. They have to accept terms, and wiLl have responsibility for the new admin.

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////**/
package main.ui.gui_admin.admin_package;



import main.database.Executor;
import main.ui.gui_admin.Main;
import main.ui.gui_admin.Terms;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.JLabel;


public class NewUser extends JFrame {
    private JPanel contentPane;
    private static Font plainFont = new Font("Avenir", Font.PLAIN, 30);
    private static Font plainFontSmall = new Font("Avenir", Font.PLAIN, 20);
    private static Font boldFont = new Font("Avenir", Font.BOLD, 50);
    private static Font plainFontSmall10 = new Font("Avenir", Font.PLAIN, 15);
    private JPanel mal;
    private JPanel mal2;


    public NewUser() {
        setTitle("Trondheim city bike");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        contentPane = new JPanel();
        contentPane.setLayout(null);


        mal2 = new JPanel();
        mal2.setLayout(null);
        mal2.setBackground(new Color(255,250,250));
        mal2.setBounds(0,0,1500,800);
        mal2.setVisible(true);

        mal = new JPanel();
        mal.setLayout(null);
        mal.setBackground(new Color(255,127,80));
        mal.setBounds(0,120,1500,5);
        mal.setVisible(true);



        //Heading---------------------------------------------------------------------
        JLabel headding = new JLabel("Trondheim Citybikes");
        headding.setBounds(40, 40, 800, 60);
        headding.setFont(boldFont);
        contentPane.add(headding);

        //Subheading---------------------------------------------------------------------
        JLabel subHeadding = new JLabel("NEW USER");
        subHeadding.setBounds(900, 40, 600, 60);
        subHeadding.setFont(boldFont);
        contentPane.add(subHeadding);


        //Back to main page---------------------------------------------------------------------
        JButton back = new JButton("Main");
        back.setBounds(50, 150, 200, 50);
        back.setFont(plainFont);

        back.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                new Main();
                setVisible(false);
                dispose();
            }
        });
        contentPane.add(back);

        //Username---------------------------------------------------------------------
        JLabel usernameText = new JLabel("Username: ");
        usernameText.setBounds(350,250,275,40);
        usernameText.setFont(plainFontSmall);
        contentPane.add(usernameText);

        JTextField usernameWrite = new JTextField();
        usernameWrite.setBounds(500,250, 275, 40);
        usernameWrite.setText("");
        usernameWrite.setFont(plainFontSmall);
        contentPane.add(usernameWrite);

        //Terms of service---------------------------------------------------------------------
        JButton terms = new JButton("Terms of service");
        terms.setBounds(500,300,275,40);
        terms.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                new Terms();
                setVisible(false);
                dispose();
            }
        });
        contentPane.add(terms);


        //I have read and accepted the terms of servis---------------------------------------------------------------------
        JCheckBox accept = new JCheckBox("I have read and accept the terms of service");
        accept.setBounds(500,350,450,40);
        accept.setFont(plainFontSmall);
        contentPane.add(accept);



        //Submit Button---------------------------------------------------------------------
        JButton submitButton = new JButton("Submit");
        submitButton.setBounds(500, 400, 100, 40);
        submitButton.setFont(plainFontSmall);
        submitButton.addActionListener(new ActionListener(){
           public void actionPerformed(ActionEvent e){
               String theEmail = usernameWrite.getText();
               if(theEmail != null && accept.isSelected()){
                   Executor.newAdmin(theEmail);
                   new Main();
                   setVisible(false);
                   dispose();
               }

           }
        });

        //Må skjekke at ingen andre har den eposten/ om det er en gyldig epost(usikker på om vi skal ha med det)
        //Må skjekke at skjekkboksen er krysset av

        contentPane.add(submitButton);



        //Slutt-------------------------------------------------------------------------------------------------
        setContentPane(contentPane);
        setSize(1500,800);
        setVisible(true);
        contentPane.add(mal);
        contentPane.add(mal2);
    }

    public static void main(String[]args){new NewUser();}
}