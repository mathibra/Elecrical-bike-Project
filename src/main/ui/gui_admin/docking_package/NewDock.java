/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@Authors: Annebelle Almås, Mathias Braathen, William Chakroun Jacobsen & Terje Haugum
@Date submitted: 23.04.2018
@Summary: In NewDock admin enter a name and latitude and longitude.

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////**/
package main.ui.gui_admin.docking_package;

import main.database.Executor;
import main.engine.cache.DockCache;
import main.ui.gui_admin.admin_package.ForgotPassword;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.JLabel;

class NewDock extends JFrame {
    private JPanel contentPane;
    private static Font plainFont = new Font("Avenir", Font.PLAIN, 30);
    private static Font plainFontSmall = new Font("Avenir", Font.PLAIN, 20);
    private static Font boldFontSmall = new Font("Avenir", Font.BOLD, 30);
    private static Font boldFont = new Font("Avenir", Font.BOLD, 50);
    private JPanel mal;
    private JPanel mal2;
    private JFrame frame;

    DockCache dc = new DockCache();

    public NewDock() {
        frame = new JFrame();
        setTitle("Trondheim city bike");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        contentPane = new JPanel();
        contentPane.setLayout(null);


        mal2 = new JPanel();
        mal2.setLayout(null);
        mal2.setBackground(new Color(255,250,250));
        mal2.setBounds(0,0,1500,800);
        mal2.setVisible(true);

        mal = new JPanel();
        mal.setLayout(null);
        mal.setBackground(new Color(255,127,80));
        mal.setBounds(0,120,1500,5);
        mal.setVisible(true);


        //Heading---------------------------------------------------------------------
        JLabel headding = new JLabel("Trondheim Citybikes");
        headding.setBounds(40, 40, 800, 60);
        headding.setFont(boldFont);
        contentPane.add(headding);

        //Subheading---------------------------------------------------------------------
        JLabel subHeadding = new JLabel("DOCKING STATION");
        subHeadding.setBounds(800, 40, 600, 60);
        subHeadding.setFont(boldFont);
        contentPane.add(subHeadding);

        JLabel del = new JLabel("NEW DOCKING STATION:");
        del.setBounds(50,225,450,40);
        del.setFont(plainFont);
        contentPane.add(del);

        //Back to main page---------------------------------------------------------------------
        JButton back = new JButton("Back to Dock");
        back.setBounds(50, 150, 200, 50);
        back.setFont(plainFont);

        back.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                new DockStation();
                setVisible(false);
                dispose();
            }
        });

        contentPane.add(back);


        //Price------------------------------------------------------
        JLabel nameText = new JLabel("Name:");
        nameText.setBounds(300,275,200,40);
        nameText.setFont(plainFontSmall);
        contentPane.add(nameText);


        JTextField nameWrite = new JTextField();
        nameWrite.setBounds(400,275,150,40);
        nameWrite.setFont(plainFontSmall);
        contentPane.add(nameWrite);


        //positions coordinates------------------------------------------------------
        JLabel positionLatText = new JLabel("Latitude:");
        positionLatText.setBounds(300,325,200,40);
        positionLatText.setFont(plainFontSmall);
        contentPane.add(positionLatText);

        JTextField positionLatWrite = new JTextField();
        positionLatWrite.setBounds(400,325,150,40);
        positionLatWrite.setFont(plainFontSmall);

        positionLatWrite.addFocusListener(new FocusListener() {
            public void focusLost(FocusEvent e) {
                if (!positionLatWrite.getText().equalsIgnoreCase("")) {
                    try {
                        new Double(positionLatWrite.getText()).doubleValue();
                        positionLatWrite.setBackground(Color.WHITE);
                    } catch (Exception e2) {
                        positionLatWrite.setBackground(Color.RED);
                        positionLatWrite.requestFocus();
                    }
                }
            }
            public void focusGained(FocusEvent e) {
            }
        });
        contentPane.add(positionLatWrite);




        JLabel positionLonText = new JLabel("Longitude:");
        positionLonText.setBounds(575,325,200,40);
        positionLonText.setFont(plainFontSmall);
        contentPane.add(positionLonText);

        JTextField positionLonWrite = new JTextField();
        positionLonWrite.setBounds(675,325,150,40);
        positionLonWrite.setFont(plainFontSmall);

        positionLonWrite.addFocusListener(new FocusListener() {
            public void focusLost(FocusEvent e) {
                if (!positionLonWrite.getText().equalsIgnoreCase("")) {
                    try {
                        new Double(positionLonWrite.getText()).doubleValue();
                        //Integer.decode(positionLonWrite.getText()).intValue();
                        positionLonWrite.setBackground(Color.WHITE);
                    } catch (Exception e2) {
                        positionLonWrite.setBackground(Color.RED);
                        positionLonWrite.requestFocus();
                    }
                }
            }
            public void focusGained(FocusEvent e) {
            }
        });
        contentPane.add(positionLonWrite);



        // Commit_button---------------------------------------------------------------
        JButton commit = new JButton("Commit");
        commit.setBounds(400,450,150,40);
        commit.setFont(plainFontSmall);
        commit.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                double latDouble = -1;
                double lonDouble = -1;
                String name = nameWrite.getText();

                String lat = positionLatWrite.getText();
                try {
                    latDouble = Double.parseDouble(lat);
                }catch (NumberFormatException nfe){
                }

                String lon = positionLonWrite.getText();
                try {
                    lonDouble = Double.parseDouble(lon);
                }catch (NumberFormatException nfe){
                }

                if(!(nameWrite.getText().isEmpty()) && !(positionLatWrite.getText().isEmpty()) && !(positionLonWrite.getText().isEmpty()) && latDouble != -1 && lonDouble != -1)
                {
                    Executor.newDocking(name, latDouble, lonDouble);
                    dc.addDock(Executor.returnDockObj(name));


                        new DockStation();
                        setVisible(false);
                        dispose();
                }else{
                    JPanel panel = new JPanel();
                    panel.setLayout(null);
                    setLayout(new BorderLayout());
                    panel.setVisible(true);

                    JLabel text = new JLabel("You have to enter all the fields");
                    text.setFont(plainFontSmall);
                    text.setBounds(20,20,400,50);
                    panel.add(text);

                    frame.setContentPane(panel);
                    frame.setBounds(350,300,350,125);
                    frame.setVisible(true);
                }
            }
        });
        contentPane.add(commit);




        //Slutt-------------------------------------------------------------------------------------------------
        setContentPane(contentPane);
        setSize(1500,800);
        setVisible(true);
        contentPane.add(mal);
        contentPane.add(mal2);
    }

    public static void main(String[]args){ new NewDock(); }
}
