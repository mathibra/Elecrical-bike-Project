/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@Authors: Annebelle Almås, Mathias Braathen, William Chakroun Jacobsen & Terje Haugum
@Date submitted: 23.04.2018
@Summary: In DockingStation admin have overview of the docks in a table, and can edit docks,
delete docks and make new docks.

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////**/
package main.ui.gui_admin.docking_package;

import main.engine.Maps;
import main.engine.cache.BikeCache;
import main.engine.cache.DockCache;
import main.objects.Bike;
import main.objects.Docking;
import main.ui.gui_admin.Main;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;


public class DockStation extends JFrame{
    private JPanel contentPane;

    private static Font plainFont = new Font("Avenir", Font.PLAIN, 30);
    private static Font plainFontSmall = new Font("Avenir", Font.PLAIN, 20);
    private static Font boldFont = new Font("Avenir", Font.BOLD, 50);
    private static Font plainFontSmall10 = new Font("Avenir", Font.BOLD, 15);
    private JPanel mal;
    private JPanel mal2;
    BikeCache bc = new BikeCache();
    DockCache dc = new DockCache();



    public DockStation() {
        long start = System.currentTimeMillis();
        setTitle("Trondheim city bike");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        contentPane = new JPanel();
        contentPane.setLayout(null);

        mal2 = new JPanel();
        mal2.setLayout(null);
        mal2.setBackground(new Color(255,250,250));
        mal2.setBounds(0,0,1500,800);
        mal2.setVisible(true);

        mal = new JPanel();
        mal.setLayout(null);
        mal.setBackground(new Color(255,127,80));
        mal.setBounds(0,120,1500,5);
        mal.setVisible(true);



        //Heading---------------------------------------------------------------------
        JLabel headding = new JLabel("Trondheim Citybikes");
        headding.setBounds(40, 40, 800, 60);
        headding.setFont(boldFont);
        contentPane.add(headding);

        //Subheading---------------------------------------------------------------------
        JLabel subHeadding = new JLabel("DOCKING STATION");
        subHeadding.setBounds(800, 40, 600, 60);
        subHeadding.setFont(boldFont);
        contentPane.add(subHeadding);


        //Back to main page---------------------------------------------------------------------
        JButton back = new JButton("Main");
        back.setBounds(50, 150, 200, 50);
        back.setFont(plainFont);

        back.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                new Main();
                setVisible(false);
                dispose();
            }
        });

        contentPane.add(back);

        int bufferSize =dc.getCache().getGroupKeys("dockCache").size();
        List<Integer> bufferID_Docks = new ArrayList<>(dc.getCache().getGroupKeys("dockCache"));


        //Map---------------------------------------------------------------------
        JLabel mapText = new JLabel("Position of docking stations & bikes");
        mapText.setFont(plainFontSmall10);
        mapText.setBounds(900,150,250,40);
        contentPane.add(mapText);

        JPanel mapPane = new JPanel( new BorderLayout());
        contentPane.add(mapPane);
        Maps m = new Maps(mapPane,900,190,350,250);
        List<Integer> bufferID_Bike = new ArrayList<>(bc.getCache().getGroupKeys("bikeCache"));
        Bike[] bikes_array = new Bike[bufferID_Bike.size()];


        for(int i = 0; i < bufferID_Bike.size(); i++){
            bikes_array[i] = bc.returnBike(bufferID_Bike.get(i));
        }

        Docking[] dock_array = new Docking[bufferID_Docks.size()];

        for(int i = 0; i <bufferID_Docks.size(); i++){
            dock_array[i] = dc.returnDock(bufferID_Docks.get(i));
        }

        m.addBikes(bikes_array);
        m.addDocks(dock_array);
        //Delete docing station------------------------------------------------------------------------
        JButton deleteDock = new JButton("Delete docking station");
        deleteDock.setBounds(900, 450, 350, 50);
        deleteDock.setFont(plainFontSmall);

        deleteDock.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                new DeleteDock();
                setVisible(false);
                dispose();
            }
        });

        contentPane.add(deleteDock);

        //Edit docing station------------------------------------------------------------------------
        JButton editDock = new JButton("Edit docking station");
        editDock.setBounds(900, 525, 350, 50);
        editDock.setFont(plainFontSmall);

        editDock.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                new EditDock();
                setVisible(false);
                dispose();
            }
        });

        contentPane.add(editDock);

        //New docking station------------------------------------------------------------------------
        JButton newDock = new JButton("New docking station");
        newDock.setBounds(900, 600, 350, 50);
        newDock.setFont(plainFontSmall);

        newDock.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
               new NewDock();
                setVisible(false);
                dispose();
            }
        });

        contentPane.add(newDock);

        DockSortFilter test = new DockSortFilter();
        test.setBounds(50, 250, 775, 450);
        test.setFont(plainFontSmall);
        contentPane.add(test);


        //Slutt-------------------------------------------------------------------------------------------------
        setContentPane(contentPane);
        setSize(1500,800);
        setVisible(true);
        contentPane.add(mal);
        contentPane.add(mal2);


        long elapsedTIme = System.currentTimeMillis() - start;
        System.out.println("Time to load Docks: " + elapsedTIme);


    }
    public static void main(String[]args){
        DockStation dock = new DockStation();
    }
}

class DockSortFilter extends JPanel {

    private DockCache dc = new DockCache();

    private String[] statusType
            = {"Docking_id","Name","Powerusage for charging","Bikes in dock"};

    Object[][] data = {};
    private DefaultTableModel model = new DefaultTableModel(data, statusType) {
        public boolean isCellEditable(int row, int column) {
            return false;//This causes all cells to be not editable
        }

        @Override
        public Class getColumnClass(int column) {
            switch (column) {
                case 1:
                    return Integer.class;
                case 2:
                    return String.class;
                case 3:
                    return Double.class;
                case 4:
                    return Integer.class;
                default:
                    return Integer.class;
            }
        }
    };
    private JTable jTable = new JTable(model);

    private TableRowSorter<TableModel> rowSorter
            = new TableRowSorter<>(jTable.getModel());

    private JTextField jtfFilter = new JTextField();

    public DockSortFilter() {

        int bufferSize =dc.getCache().getGroupKeys("dockCache").size();
        List<Integer> bufferID_Docks = new ArrayList<>(dc.getCache().getGroupKeys("dockCache"));

        for(int i = 0; i< bufferSize; i++) {
            if (dc.returnDock(bufferID_Docks.get(i)).getStatus() == 0) {
                int id = dc.returnDock(bufferID_Docks.get(i)).getDock_id();
                String name = dc.returnDock(bufferID_Docks.get(i)).getName();
                double pwrUsg = dc.returnDock(bufferID_Docks.get(i)).getPower_usage();
                int numberOfBikes = dc.returnDock(bufferID_Docks.get(i)).numberOfBikes();
                Object[] data = {id, name,pwrUsg,numberOfBikes};
                model.addRow(data);
            }
        }
        jTable.setRowSorter(rowSorter);

        JPanel panel = new JPanel(new BorderLayout());
        panel.add(new JLabel("Specify search:"),
                BorderLayout.WEST);
        panel.add(jtfFilter, BorderLayout.CENTER);

        setLayout(new BorderLayout());
        add(panel, BorderLayout.SOUTH);
        add(new JScrollPane(jTable), BorderLayout.CENTER);

        //method for adding a search bar, that actively listens for input and updates table
        jtfFilter.getDocument().addDocumentListener(new DocumentListener() {

            @Override
            public void insertUpdate(DocumentEvent e) {
                String text = jtfFilter.getText();

                if (text.trim().length() == 0) {
                    rowSorter.setRowFilter(null);
                } else {
                    rowSorter.setRowFilter(RowFilter.regexFilter("(?i)" + text));
                }
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                String text = jtfFilter.getText();

                if (text.trim().length() == 0) {
                    rowSorter.setRowFilter(null);
                } else {
                    rowSorter.setRowFilter(RowFilter.regexFilter("(?i)" + text));
                }
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
    }
}