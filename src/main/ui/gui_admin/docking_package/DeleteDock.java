/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@Authors: Annebelle Almås, Mathias Braathen, William Chakroun Jacobsen & Terje Haugum
@Date submitted: 23.04.2018
@Summary: In DeleteDock admin can delete a dock, but only if there are no bikes on the docking station.

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////**/
package main.ui.gui_admin.docking_package;

import main.database.Executor;
import main.engine.cache.DockCache;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

class DeleteDock extends JFrame{
    private JPanel contentPane;
    private static Font plainFont = new Font("Avenir", Font.PLAIN, 30);
    private static Font plainFontSmall = new Font("Avenir", Font.PLAIN, 20);
    private static Font boldFont = new Font("Avenir", Font.BOLD, 50);
    private JPanel mal;
    private JPanel mal2;

    DockCache dc = new DockCache();

    public DeleteDock(){
        setTitle("Trondheim city bike");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        contentPane = new JPanel();
        contentPane.setLayout(null);

        mal2 = new JPanel();
        mal2.setLayout(null);
        mal2.setBackground(new Color(255,250,250));
        mal2.setBounds(0,0,1500,800);
        mal2.setVisible(true);

        mal = new JPanel();
        mal.setLayout(null);
        mal.setBackground(new Color(255,127,80));
        mal.setBounds(0,120,1500,5);
        mal.setVisible(true);

        //Table-----------------------------------------------------------------------
        int bufferSize =dc.getCache().getGroupKeys("dockCache").size();
        List<Integer> bufferID = new ArrayList<>(dc.getCache().getGroupKeys("dockCache"));


        Integer[] name = new Integer[bufferSize];
        for(int i = 0; i < bufferSize; i++){

            name[i] = bufferID.get(i);
        }

        //Heading---------------------------------------------------------------------
        JLabel headding = new JLabel("Trondheim Citybikes");
        headding.setBounds(40, 40, 800, 60);
        headding.setFont(boldFont);
        contentPane.add(headding);

        //Subheading---------------------------------------------------------------------
        JLabel subHeadding = new JLabel("DOCKING STATION");
        subHeadding.setBounds(800, 40, 600, 60);
        subHeadding.setFont(boldFont);
        contentPane.add(subHeadding);

        JLabel del = new JLabel("DELETE DOCKING STATION:");
        del.setBounds(50,225,450,40);
        del.setFont(plainFont);
        contentPane.add(del);

        //Back to main page---------------------------------------------------------------------
        JButton back = new JButton("Back to Dock");
        back.setBounds(50, 150, 200, 50);
        back.setFont(plainFont);

        back.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                new DockStation();
                setVisible(false);
                dispose();
            }
        });

        contentPane.add(back);

        // Check_button---------------------------------------------------------------
        JCheckBox sure = new JCheckBox("Are you sure?");
        sure.setBounds(475,300,200,40);
        sure.setBackground(new Color(255,250,250));
        sure.setFont(plainFontSmall);
        contentPane.add(sure);



        // Dock_id---------------------------------------------------------------
        JLabel idText = new JLabel("Docking_id:");
        idText.setBounds(200,300,200,40);
        idText.setFont(plainFontSmall);
        contentPane.add(idText);

        JComboBox<Integer> box = new JComboBox<>(name);
        box.setFont(plainFontSmall);
        box.setSize(100,500);
        box.setBounds(325,300,100,40);
        contentPane.add(box);


        // Commit_button---------------------------------------------------------------
        JButton commit = new JButton("Commit");
        commit.setBounds(675,300,100,40);
        commit.setFont(plainFontSmall);
        commit.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                if(sure.isSelected()) {
                    Integer typ = (Integer)box.getSelectedItem();
                    Executor.deleteDocking(typ);
                    dc.removeDock(typ);

                    new DockStation();
                    setVisible(false);
                    dispose();
                }
            }
        });
        contentPane.add(commit);

        DockSortFilter test = new DockSortFilter();
        test.setBounds(50,400,800,300);
        test.setFont(plainFontSmall);
        contentPane.add(test);


        //Slutt-------------------------------------------------------------------------------------------------
        setContentPane(contentPane);
        setSize(1500,800);
        setVisible(true);
        contentPane.add(mal);
        contentPane.add(mal2);
    }


    //Main---------------------------------------------------------------------
    public static void main(String[]args){ new DeleteDock(); }
}
