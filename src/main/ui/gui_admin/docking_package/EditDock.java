/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@Authors: Annebelle Almås, Mathias Braathen, William Chakroun Jacobsen & Terje Haugum
@Date submitted: 23.04.2018
@Summary: In EditDock admin have to take a dock_id and sett a new name, latitude and longitude.

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////**/
package main.ui.gui_admin.docking_package;


import main.database.Executor;
import main.engine.cache.DockCache;


import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.IllegalFormatException;
import java.util.List;
import javax.swing.JLabel;

class EditDock extends JFrame {
    private JPanel contentPane;
    private static Font plainFont = new Font("Avenir", Font.PLAIN, 30);
    private static Font plainFontSmall = new Font("Avenir", Font.PLAIN, 20);
    private static Font boldFontSmall = new Font("Avenir", Font.BOLD, 30);
    private static Font boldFont = new Font("Avenir", Font.BOLD, 50);
    private JPanel mal;
    private JPanel mal2;
    JFrame frame2;

    DockCache dc = new DockCache();

    public EditDock() {
        frame2 = new JFrame();
        setTitle("Trondheim city bike");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        contentPane = new JPanel();
        contentPane.setLayout(null);


        mal2 = new JPanel();
        mal2.setLayout(null);
        mal2.setBackground(new Color(255,250,250));
        mal2.setBounds(0,0,1500,800);
        mal2.setVisible(true);

        mal = new JPanel();
        mal.setLayout(null);
        mal.setBackground(new Color(255,127,80));
        mal.setBounds(0,120,1500,5);
        mal.setVisible(true);

        //Heading---------------------------------------------------------------------
        JLabel headding = new JLabel("Trondheim Citybikes");
        headding.setBounds(40, 40, 800, 60);
        headding.setFont(boldFont);
        contentPane.add(headding);

        //Subheading---------------------------------------------------------------------
        JLabel subHeadding = new JLabel("DOCKING STATION");
        subHeadding.setBounds(800, 40, 600, 60);
        subHeadding.setFont(boldFont);
        contentPane.add(subHeadding);

        JLabel del = new JLabel("EDIT DOCKING STATION:");
        del.setBounds(900,225,450,40);
        del.setFont(plainFont);
        contentPane.add(del);

        //Back to main page---------------------------------------------------------------------
        JButton back = new JButton("Back to Dock");
        back.setBounds(50, 150, 200, 50);
        back.setFont(plainFont);

        back.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                new DockStation();
                setVisible(false);
                dispose();
            }
        });

        contentPane.add(back);




        //Docking_id------------------------------------------------------
        JLabel idText = new JLabel("Docking_id:");
        idText.setBounds(900,275,200,40);
        idText.setFont(plainFontSmall);
        contentPane.add(idText);


        int bufferSize =dc.getCache().getGroupKeys("dockCache").size();
        List<Integer> bufferID = new ArrayList<>(dc.getCache().getGroupKeys("dockCache"));

        Integer[] name = new Integer[bufferSize];
        for(int i = 0; i < bufferSize; i++){

            name[i] = bufferID.get(i);
        }
        JComboBox<Integer> box = new JComboBox<>(name);
        box.setFont(plainFontSmall);
        box.setSize(100,500);
        box.setBounds(1100,275,150,40);
        contentPane.add(box);

        //new name------------------------------------------------------
        JLabel nameText = new JLabel("New name:");
        nameText.setBounds(900,325,200,40);
        nameText.setFont(plainFontSmall);
        contentPane.add(nameText);

        JTextField nameWrite = new JTextField();
        nameWrite.setBounds(1100,325,150,40);
        nameWrite.setFont(plainFontSmall);
        contentPane.add(nameWrite);


        //positions coordinates------------------------------------------------------

        JLabel positionLonText = new JLabel("New Longitude:");
        positionLonText.setBounds(900,375,200,40);
        positionLonText.setFont(plainFontSmall);
        contentPane.add(positionLonText);

        JTextField positionLanWrite = new JTextField();
        positionLanWrite.setBounds(1100,375,150,40);
        positionLanWrite.setFont(plainFontSmall);

        positionLanWrite.addFocusListener(new FocusListener() {
            public void focusLost(FocusEvent e) {
                if (!positionLanWrite.getText().equalsIgnoreCase("")) {
                    try {
                        new Double(positionLanWrite.getText()).doubleValue();
                        positionLanWrite.setBackground(Color.WHITE);
                    } catch (Exception e2) {
                        positionLanWrite.setBackground(Color.RED);
                        positionLanWrite.requestFocus();
                    }
                }
            }

            public void focusGained(FocusEvent e) {

            }
        });

        contentPane.add(positionLanWrite);




        JLabel positionLatText = new JLabel("New Latitude:");
        positionLatText.setBounds(900,425,200,40);
        positionLatText.setFont(plainFontSmall);
        contentPane.add(positionLatText);

        JTextField positionLatWrite = new JTextField();
        positionLatWrite.setBounds(1100,425,150,40);
        positionLatWrite.setFont(plainFontSmall);



        positionLatWrite.addFocusListener(new FocusListener() {
            public void focusLost(FocusEvent e) {
                if (!positionLatWrite.getText().equalsIgnoreCase("")) {
                    try {
                        new Double(positionLatWrite.getText()).doubleValue();
                        positionLatWrite.setBackground(Color.WHITE);
                    } catch (Exception e2) {
                        positionLatWrite.setBackground(Color.RED);
                        positionLatWrite.requestFocus();
                    }
                }
            }
            public void focusGained(FocusEvent e) {
            }
        });
        contentPane.add(positionLatWrite);

        // Commit_button---------------------------------------------------------------
        JButton commit = new JButton("Commit");
        commit.setBounds(1100,500,150,40);
        commit.setFont(plainFontSmall);
        commit.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                String newname;
                int id = (int)box.getSelectedItem();
                double latDouble = dc.returnDock(id).getLatitude();;
                double lonDouble = dc.returnDock(id).getLongitude();;
                double checkLong = -1;
                double checkLat = -1;



                String lat = positionLatWrite.getText();

                String lon =positionLanWrite.getText();

                newname  = nameWrite.getText();

                if(nameWrite.getText().isEmpty()) {

                    newname = dc.returnDock(id).getName();
                }

                if(!positionLatWrite.getText().isEmpty()){
                    try {
                        latDouble = Double.parseDouble(lat);
                        checkLat = 1;
                    }catch (NumberFormatException ife){
                    }
                }

                if(!positionLanWrite.getText().isEmpty()){
                    try {
                        lonDouble = Double.parseDouble(lon);
                        checkLong = 1;
                    }catch (NumberFormatException nfe){
                    }
                }

                if(box.getItemCount() != 0 && (!(nameWrite.getText().isEmpty()) || (!(positionLatWrite.getText().isEmpty()) && checkLat != -1) || (!(positionLanWrite.getText().isEmpty())) && checkLong != -1)) {
                    dc.editDock(id, newname, latDouble, lonDouble);
                    Executor.editDocking(id, newname, latDouble, lonDouble);

                    new DockStation();
                    setVisible(false);
                    dispose();

                }
            }
        });
        contentPane.add(commit);

        DockSortFilter test = new DockSortFilter();
        test.setBounds(50,250,800,400);
        test.setFont(plainFontSmall);
        contentPane.add(test);


        //Slutt-------------------------------------------------------------------------------------------------
        setContentPane(contentPane);
        setSize(1500,800);
        setVisible(true);
        contentPane.add(mal);
        contentPane.add(mal2);

    }

    public static void main(String[]args){ new EditDock(); }
}
