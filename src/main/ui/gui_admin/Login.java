/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@Authors: Annebelle Almås, Mathias Braathen, William Chakroun Jacobsen & Terje Haugum
@Date submitted: 23.04.2018
@Summary: In Loging admin can login or go to forgot password to get a new one.

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////**/
package main.ui.gui_admin;

import main.database.Executor;
import main.engine.cache.AdminCache;
import main.ui.gui_admin.admin_package.ForgotPassword;

import java.awt.*;
import javax.swing.*;
import javax.swing.JLabel;
import java.awt.event.*;



public class Login extends JFrame{
    private JPanel contentPane;
    private static Font plainFontSmall = new Font("Avenir", Font.PLAIN, 20);
    private static Font boldFont = new Font("Avenir", Font.BOLD, 50);
    private JPanel mal;
    private JPanel mal2;

    AdminCache adminCache = new AdminCache();

    public Login() {
        setTitle("Trondheim city bike login");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        contentPane = new JPanel();
        contentPane.setLayout(null);


        mal2 = new JPanel();
        mal2.setLayout(null);
        mal2.setBackground(new Color(255, 250, 250));
        mal2.setBounds(0, 0, 1500, 800);
        mal2.setVisible(true);

        mal = new JPanel();
        mal.setLayout(null);
        mal.setBackground(new Color(255, 127, 80));
        mal.setBounds(0, 120, 1500, 5);
        mal.setVisible(true);


        //Heading---------------------------------------------------------------------
        JLabel headding = new JLabel("Trondheim Citybikes");
        headding.setBounds(40, 40, 700, 60);
        headding.setFont(boldFont);
        contentPane.add(headding);


        //Username---------------------------------------------------------------------
        //setBounds(x-akse, y-akse, størrelse på tekstområdet, tykkelse på tekstområde)

        JLabel usernameText = new JLabel("Username: ");
        usernameText.setBounds(350, 250, 275, 40);
        usernameText.setFont(plainFontSmall);
        contentPane.add(usernameText);

        JTextField usernameWrite = new JTextField();
        usernameWrite.setBounds(500, 250, 275, 40);
        usernameWrite.setFont(plainFontSmall);

        contentPane.add(usernameWrite);

        //Password---------------------------------------------------------------------
        JLabel passwordText = new JLabel("Password: ");
        passwordText.setBounds(350, 300, 200, 40);
        passwordText.setFont(plainFontSmall);
        contentPane.add(passwordText);

        JPasswordField passwordWrite = new JPasswordField();
        passwordWrite.setBounds(500, 300, 275, 40);


        contentPane.add(passwordWrite);

        //Forgot password---------------------------------------------------------------------
        JButton forgot = new JButton("Forgot your password?");
        forgot.setBounds(500, 350, 275, 40);
        forgot.setFont(plainFontSmall);

        forgot.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                new ForgotPassword();
                setVisible(false);
                dispose();
            }

        });
        contentPane.add(forgot);



        //Login Button---------------------------------------------------------------------
        JButton loginButton = new JButton("Login");
        loginButton.setBounds(500, 400, 100, 40);
        loginButton.setFont(plainFontSmall);

        loginButton.addActionListener(new ActionListener(){
           public void actionPerformed(ActionEvent e){
               String username = usernameWrite.getText();
               String password = String.valueOf(passwordWrite.getPassword());
               if(Executor.loginA(username, password)){
                   adminCache.addEmail(username);
                   new main.ui.gui_admin.Main();
                   setVisible(false);
                   dispose();
               }

           }
        });

        contentPane.add(loginButton);




        //Slutt-------------------------------------------------------------------------------------------------
        setContentPane(contentPane);
        setSize(1500,800);
        setVisible(true);
        contentPane.add(mal);
        contentPane.add(mal2);
    }



    //main.ui.gui_admin.Main---------------------------------------------------------------------
    public static void main(String[]args){new Login(); }
}