/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@Authors: Annebelle Almås, Mathias Braathen, William Chakroun Jacobsen & Terje Haugum
@Date submitted: 23.04.2018
@Summary: In  EditBike admin can edit a bike. Admin can choose to change one thing or every ting on the bike.
It is possible to change the price, the date when it was produced, number of repairs, trips and km, change type or status.

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////**/
package main.ui.gui_admin.bike_package;

import com.toedter.calendar.JDateChooser;
import main.database.Executor;
import main.engine.cache.BikeCache;
import main.engine.cache.TypeCache;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

class EditBike extends JFrame {
    private JPanel contentPane;
    private static Font plainFont = new Font("Avenir", Font.PLAIN, 30);
    private static Font plainFontOK = new Font("Avenir", Font.PLAIN, 15);
    private static Font plainFontSmall = new Font("Avenir", Font.PLAIN, 20);
    private static Font boldFontSmall = new Font("Avenir", Font.BOLD, 30);
    private static Font boldFont = new Font("Avenir", Font.BOLD, 50);
    private JPanel mal;
    private JPanel mal2;
    private JTextField priceWrite;

    BikeCache bc = new BikeCache();
    TypeCache tc = new TypeCache();

    public EditBike() {
        setTitle("Trondheim city bike");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        contentPane = new JPanel();
        contentPane.setLayout(null);


        mal2 = new JPanel();
        mal2.setLayout(null);
        mal2.setBackground(new Color(255,250,250));
        mal2.setBounds(0,0,1500,800);
        mal2.setVisible(true);

        mal = new JPanel();
        mal.setLayout(null);
        mal.setBackground(new Color(255,127,80));
        mal.setBounds(0,120,1500,5);
        mal.setVisible(true);


        //Heading---------------------------------------------------------------------
        JLabel headding = new JLabel("Trondheim Citybikes");
        headding.setBounds(40, 40, 800, 60);
        headding.setFont(boldFont);
        contentPane.add(headding);

        //Subheading---------------------------------------------------------------------
        JLabel subHeadding = new JLabel("BIKES");
        subHeadding.setBounds(800, 50, 600, 60);
        subHeadding.setFont(boldFont);
        contentPane.add(subHeadding);


        JLabel del = new JLabel("EDIT BIKE:");
        del.setBounds(800,150,250,40);
        del.setFont(plainFont);
        contentPane.add(del);


        //Back to main page---------------------------------------------------------------------
        JButton back = new JButton("Back to Bike");
        back.setBounds(50, 150, 200, 50);
        back.setFont(plainFont);

        back.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                new Bike();
                setVisible(false);
                dispose();
            }
        });
        contentPane.add(back);





        //BIKE_ID------------------------------------------------------
        int bufferSize =bc.getCache().getGroupKeys("bikeCache").size();
        List<Integer> bufferID = new ArrayList<>(bc.getCache().getGroupKeys("bikeCache"));

        Integer[] nameid = new Integer[bufferSize];

        for(int i = 0; i < bufferID.size(); i++){
            if(bc.returnBike(bufferID.get(i)).getStatus() ==0){
                continue;
            }
            nameid[i] = bufferID.get(i);
        }

        JLabel bike_idText = new JLabel("Bike_id:");
        bike_idText.setBounds(800,250,150,40);
        bike_idText.setFont(plainFontSmall);
        contentPane.add(bike_idText);

        JComboBox<Integer> boxid = new JComboBox<>(nameid);
        boxid.setFont(plainFontSmall);
        boxid.setSize(100,500);
        boxid.setBounds(1000,250,150,40);
        contentPane.add(boxid);


       //Price------------------------------------------------------
        JLabel priceText = new JLabel("Edit price:");
        priceText.setBounds(800,300,150,40);
        priceText.setFont(plainFontSmall);
        contentPane.add(priceText);

        priceWrite = new JTextField();
        priceWrite.setBounds(1000,300,150,40);
        priceWrite.setFont(plainFontSmall);

        priceWrite.addFocusListener(new FocusListener() {
            public void focusLost(FocusEvent e) {
                if (!priceWrite.getText().equalsIgnoreCase("")) {
                    try {
                        Integer.decode(priceWrite.getText()).intValue();
                        priceWrite.setBackground(Color.WHITE);
                    } catch (Exception e2) {
                        priceWrite.setBackground(Color.RED);
                        priceWrite.requestFocus();
                    }
                }
            }

            public void focusGained(FocusEvent e) {

            }
        });


        contentPane.add(priceWrite);



        //Date produced------------------------------------------------------
        JLabel prodText = new JLabel("Edit date produced:");
        prodText.setBounds(800,350,200,40);
        prodText.setFont(plainFontSmall);
        contentPane.add(prodText);

        JDateChooser prodWrite = new JDateChooser();
        prodWrite.setBounds(1000,350,150,40);
        prodWrite.setFont(plainFontSmall);
        contentPane.add(prodWrite);

        //All repairs------------------------------------------------------
        JLabel repText = new JLabel("Edit all repairs:");
        repText.setBounds(800,400,175,40);
        repText.setFont(plainFontSmall);
        contentPane.add(repText);

        JTextField repWrite = new JTextField();
        repWrite.setBounds(1000,400,150,40);
        repWrite.setFont(plainFontSmall);

        repWrite.addFocusListener(new FocusListener() {
            public void focusLost(FocusEvent e) {
                if (!repWrite.getText().equalsIgnoreCase("")) {
                    try {
                        Integer.decode(repWrite.getText()).intValue();
                        repWrite.setBackground(Color.WHITE);
                    } catch (Exception e2) {
                        repWrite.setBackground(Color.RED);
                        repWrite.requestFocus();
                    }
                }
            }

            public void focusGained(FocusEvent e) {
            }
        });
        contentPane.add(repWrite);

        //All trips------------------------------------------------------
        JLabel tripText = new JLabel("Edit all trips:");
        tripText.setBounds(800,450,150,40);
        tripText.setFont(plainFontSmall);
        contentPane.add(tripText);

        JTextField tripWrite = new JTextField();
        tripWrite.setBounds(1000,450,150,40);
        tripWrite.setFont(plainFontSmall);

        tripWrite.addFocusListener(new FocusListener() {
            public void focusLost(FocusEvent e) {
                if (!tripWrite.getText().equalsIgnoreCase("")) {
                    try {
                        Integer.decode(tripWrite.getText()).intValue();
                        tripWrite.setBackground(Color.WHITE);
                    } catch (Exception e2) {
                        tripWrite.setBackground(Color.RED);
                        tripWrite.requestFocus();
                    }
                }
            }

            public void focusGained(FocusEvent e) {
            }
        });

        contentPane.add(tripWrite);


        //All kmTot------------------------------------------------------
        JLabel kmText = new JLabel("Edit km:");
        kmText.setBounds(800,500,150,40);
        kmText.setFont(plainFontSmall);
        contentPane.add(kmText);

        JTextField kmWrite = new JTextField();
        kmWrite.setBounds(1000,500,150,40);
        kmWrite.setFont(plainFontSmall);

        kmWrite.addFocusListener(new FocusListener() {
            public void focusLost(FocusEvent e) {
                if (!kmWrite.getText().equalsIgnoreCase("")) {
                    try {
                        Integer.decode(kmWrite.getText()).intValue();
                        kmWrite.setBackground(Color.WHITE);
                    } catch (Exception e2) {
                        kmWrite.setBackground(Color.RED);
                        kmWrite.requestFocus();
                    }
                }
            }

            public void focusGained(FocusEvent e) {
            }
        });

        contentPane.add(kmWrite);

        //Type------------------------------------------------------
        JLabel typeText = new JLabel("Type:");
        typeText.setBounds(800,550,150,40);
        typeText.setFont(plainFontSmall);
        contentPane.add(typeText);


        int bufferSizeType =tc.getCache().getGroupKeys("typeCache").size();
        List<Integer> bufferIDType = new ArrayList<>(tc.getCache().getGroupKeys("typeCache"));
        String[] nameTyp = new String[bufferSizeType];

        for(int i = 0; i < bufferIDType.size(); i++){
            nameTyp[i] = tc.returnType(bufferIDType.get(i)).getName();
        }

        JComboBox boxt = new JComboBox(nameTyp);
        boxt.setFont(plainFontSmall);
        boxt.setSize(100,500);
        boxt.setBounds(1000,550,150,40);
        String typ = (String)boxt.getSelectedItem();
        contentPane.add(boxt);

        JCheckBox checkType = new JCheckBox("Change type?");
        checkType.setBounds(1140,550,250,50);
        checkType.setFont(plainFontOK);
        checkType.setBackground(new Color(255,250,250));
        contentPane.add(checkType);


        //Creation of status------------------------------------------------------
        JLabel statusText = new JLabel("Status:");
        statusText.setBounds(800,600,150,40);
        statusText.setFont(plainFontSmall);
        contentPane.add(statusText);

        String statusType[] = {"In Repair", "Driving", "Need Repair", "In Storage"};

        JComboBox boxs = new JComboBox(statusType);
        boxs.setFont(plainFontSmall);
        boxs.setSize(100,500);
        boxs.setBounds(1000,600,150,40);
        contentPane.add(boxs);


        JCheckBox checkStatus = new JCheckBox("Change status?");
        checkStatus.setFont(plainFontOK);
        checkStatus.setBounds(1140,600,250,50);
        checkStatus.setBackground(new Color(255,250,250));
        contentPane.add(checkStatus);

        // Commit_button---------------------------------------------------------------
        JButton commit = new JButton("Commit");
        commit.setBounds(1000,650,150,40);
        commit.setFont(plainFontSmall);


        commit.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent p) {
                int id =(int)boxid.getSelectedItem();
                int priceInt;
                java.sql.Date dateD;
                int repInt;
                int tripInt;
                int kmInt;
                int typeInt;
                int statusInt = bc.returnBike(id).getStatus();


                String price = priceWrite.getText();
                if(price.equals("")){
                    priceInt = bc.returnBike(id).getPrice();
                }else{
                    priceInt = Integer.parseInt(price);
                }

                SimpleDateFormat dateFormatM = new SimpleDateFormat("yyyy-MM-dd");

                if(prodWrite.getDate() == null){
                    dateD = bc.returnBike(id).getProd_date();
                }else{
                    String date = dateFormatM.format(prodWrite.getDate());
                    dateD = java.sql.Date.valueOf(date);
                }

                String rep = repWrite.getText();
                if(rep.equalsIgnoreCase("")){
                    repInt = bc.returnBike(id).getAnt_rep();
                } else{
                    repInt = Integer.parseInt(rep);
                }

                String trip = tripWrite.getText();
                if(trip.equalsIgnoreCase("")){
                    tripInt = bc.returnBike(id).getTot_trips();
                } else {
                    tripInt = Integer.parseInt(trip);
                }

                String km = kmWrite.getText();
                if(km.equalsIgnoreCase("")){
                    kmInt = bc.returnBike(id).getTot_km();
                }else{
                    kmInt = Integer.parseInt(km);
                }

               String type =(String)boxt.getSelectedItem();

               if(!checkType.isSelected()){
                   typeInt = bc.returnBike(id).getType_id();
               }else{
                   typeInt = Executor.nametoID(type);
               }

               String status = (String)boxs.getSelectedItem();


                if(checkStatus.isSelected()){
                    switch (status) {
                        case "In Repair":
                            statusInt = 2;
                            break;
                        case "Driving":
                            statusInt = 3;
                            break;
                        case "Need Repair":
                            statusInt = 4;
                            break;
                        case "In Storage":
                            statusInt = 5;
                            break;
                    }
                }

                //If none of the fields are entered, a a box wil pop up and tell the user to enter one or more of the fields
                if(priceWrite.getText().isEmpty() && prodWrite.getDate() == null && repWrite.getText().isEmpty() && tripWrite.getText().isEmpty() && kmWrite.getText().isEmpty() && !checkType.isSelected() && !checkStatus.isSelected()){

                    //JOptionPane.showMessageDialog(null, "To commit, you have to enter one or more of the fields");

                    JFrame frame = new JFrame();
                    setLayout(new BorderLayout());
                    JPanel panel = new JPanel();
                    panel.setLayout(null);


                    JLabel text = new JLabel("To commit, you have to enter one or more of the fields");
                    text.setFont(plainFontSmall);
                    text.setBounds(20,20,700,50);
                    panel.add(text);

                    JButton ok = new JButton("Ok");
                    ok.setFont(plainFontOK);
                    ok.setBounds(460,75,75,25);
                    panel.add(ok);

                    ok.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            frame.dispose();
                        }
                    });

                    frame.add(panel);
                    frame.setBounds(350,300,540,125);
                    frame.setVisible(true);

                }else {
                    Executor.editBike(id, priceInt, dateD, repInt, tripInt, kmInt, typeInt, statusInt);
                    bc.editBike(id, priceInt, dateD, repInt, tripInt, kmInt, typeInt, statusInt);


                    new Bike();
                    setVisible(false);
                    dispose();
                }
            }
        });

        contentPane.add(commit);


        EditSortFilter test = new EditSortFilter();
        test.setBounds(50, 250, 725, 450);
        test.setFont(plainFontSmall);
        contentPane.add(test);


        //Slutt-------------------------------------------------------------------------------------------------
        setContentPane(contentPane);
        setSize(1500,800);
        setVisible(true);
        contentPane.add(mal);
        contentPane.add(mal2);

    }


    public static void main(String[]args){
        new EditBike();
    }

}

class EditSortFilter extends JPanel {

    private BikeCache bc = new BikeCache();


    private String[] statusType
            = {"Bike_id", "Price", "Date of purchase", "Charging level", "Antall repairs", "Total km", "Total trips", "Docked", "Made", "Type", "Status"};

    int bufferSize = bc.getCache().getGroupKeys("bikeCache").size();
    List<Integer> bufferID = new ArrayList<>(bc.getCache().getGroupKeys("bikeCache"));

    Object[][] data = {};
    private DefaultTableModel model = new DefaultTableModel(data, statusType) {
        public boolean isCellEditable(int row, int column) {
            return false;//This causes all cells to be not editable
        }

        @Override
        public Class getColumnClass(int column) {
            switch (column) {
                case 2:
                    return java.sql.Date.class;
                case 7:
                    return String.class;
                case 8:
                    return java.sql.Date.class;
                case 9:
                    return String.class;
                case 10:
                    return String.class;
                default:
                    return Integer.class;
            }
        }
    };
    private JTable jTable = new JTable(model);

    private TableRowSorter<TableModel> rowSorter
            = new TableRowSorter<>(jTable.getModel());

    private JTextField jtfFilter = new JTextField();
    private JButton jbtFilter = new JButton("Filter");

    private String converter(int status) {
        String str = "";
        switch (status) {
            case 1:
                str = "Docked";
                break;
            case 2:
                str = "In Repair";
                break;
            case 3:
                str = "Driving";
                break;
            case 4:
                str = "Need repair";
                break;
            case 5:
                str = "In Storage";
                break;
        }
        return str;
    }

    public EditSortFilter() {
        for (int i = 0; i < bufferSize; i++) {
            if (bc.returnBike(bufferID.get(i)).getStatus() == 0) {
                continue;
            }
            Object[] data = {new Integer(bc.returnBike(bufferID.get(i)).getBike_id()), new Integer(bc.returnBike(bufferID.get(i)).getPrice()),
                    bc.returnBike(bufferID.get(i)).getReg_date(), bc.returnBike(bufferID.get(i)).getCharge(),
                    bc.returnBike(bufferID.get(i)).getAnt_rep(), bc.returnBike(bufferID.get(i)).getTot_km(),
                    bc.returnBike(bufferID.get(i)).getTot_trips(), bc.returnBike(bufferID.get(i)).getDocked(),
                    bc.returnBike(bufferID.get(i)).getProd_date(), Executor.idtoName(bc.returnBike(bufferID.get(i)).getType_id()),
                    converter(bc.returnBike(bufferID.get(i)).getStatus())};
            model.addRow(data);
        }
        jTable.setRowSorter(rowSorter);

        JPanel panel = new JPanel(new BorderLayout());
        panel.add(new JLabel("Specify search:"),
                BorderLayout.WEST);
        panel.add(jtfFilter, BorderLayout.CENTER);

        setLayout(new BorderLayout());
        add(panel, BorderLayout.SOUTH);
        add(new JScrollPane(jTable), BorderLayout.CENTER);

        //method for adding a search bar, that actively listens for input and updates table
        jtfFilter.getDocument().addDocumentListener(new DocumentListener() {

            @Override
            public void insertUpdate(DocumentEvent e) {
                String text = jtfFilter.getText();

                if (text.trim().length() == 0) {
                    rowSorter.setRowFilter(null);
                } else {
                    rowSorter.setRowFilter(RowFilter.regexFilter("(?i)" + text));
                }
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                String text = jtfFilter.getText();

                if (text.trim().length() == 0) {
                    rowSorter.setRowFilter(null);
                } else {
                    rowSorter.setRowFilter(RowFilter.regexFilter("(?i)" + text));
                }
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
    }
}