/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@Authors: Annebelle Almås, Mathias Braathen, William Chakroun Jacobsen & Terje Haugum
@Date submitted: 23.04.2018
@Summary: In NewBike admin can make a new bike. Her admin have to enter the price, date purchased, date produced and type.

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////**/
package main.ui.gui_admin.bike_package;

import main.database.Executor;
import main.engine.cache.BikeCache;
import main.engine.cache.TypeCache;
import com.toedter.calendar.JDateChooser;


import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.text.SimpleDateFormat;
import javax.swing.JLabel;
import java.util.ArrayList;
import java.util.List;

class NewBike extends JFrame {
    private JPanel contentPane;
    private static Font plainFont = new Font("Avenir", Font.PLAIN, 30);
    private static Font plainFontSmall = new Font("Avenir", Font.PLAIN, 20);
    private static Font boldFontSmall = new Font("Avenir", Font.BOLD, 30);
    private static Font boldFont = new Font("Avenir", Font.BOLD, 50);
    private JPanel mal;
    private JPanel mal2;
    private JFrame frame;
    private JPanel panel;

    BikeCache bc = new BikeCache();
    TypeCache tc = new TypeCache();

    public NewBike() {
        frame = new JFrame();
        setTitle("Trondheim city bike");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        contentPane = new JPanel();
        contentPane.setLayout(null);


        mal2 = new JPanel();
        mal2.setLayout(null);
        mal2.setBackground(new Color(255,250,250));
        mal2.setBounds(0,0,1500,800);
        mal2.setVisible(true);

        mal = new JPanel();
        mal.setLayout(null);
        mal.setBackground(new Color(255,127,80));
        mal.setBounds(0,120,1500,5);
        mal.setVisible(true);



        //Heading---------------------------------------------------------------------
        JLabel headding = new JLabel("Trondheim Citybikes");
        headding.setBounds(40, 40, 800, 60);
        headding.setFont(boldFont);
        contentPane.add(headding);


        //Subheading---------------------------------------------------------------------
        JLabel subHeadding = new JLabel("BIKES");
        subHeadding.setBounds(900, 50, 600, 60);
        subHeadding.setFont(boldFont);
        contentPane.add(subHeadding);

        JLabel del = new JLabel("NEW BIKE:");
        del.setBounds(50,225,250,40);
        del.setFont(plainFont);
        contentPane.add(del);


        //Back to main page---------------------------------------------------------------------
        JButton back = new JButton("Back to bike");
        back.setBounds(50, 150, 200, 50);
        back.setFont(plainFont);

        back.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                new Bike();
                setVisible(false);
                dispose();
            }
        });

        contentPane.add(back);


        //Price------------------------------------------------------
        JLabel priceText = new JLabel("Price:");
        priceText.setBounds(300,300,150,40);
        priceText.setFont(plainFontSmall);
        contentPane.add(priceText);

        final JTextField priceWrite = new JTextField();
        priceWrite.setToolTipText("Input number");
        priceWrite.setBounds(500,300,150,40);
        priceWrite.setFont(plainFontSmall);
        priceWrite.addFocusListener(new FocusListener() {

            public void focusLost(FocusEvent e) {
            if (!priceWrite.getText().equalsIgnoreCase("")) {
                try {
                    Integer.decode(priceWrite.getText()).intValue();
                    priceWrite.setBackground(Color.WHITE);
                } catch (Exception e2) {
                    priceWrite.setBackground(Color.RED);
                    priceWrite.requestFocus();
                }
            }
        }
            public void focusGained(FocusEvent e) {

            }
        });
        contentPane.add(priceWrite);

        //Date of purchase------------------------------------------------------
        JLabel datePurchaseText = new JLabel("Date of purchase:");
        datePurchaseText.setBounds(300,350,200,40);
        datePurchaseText.setFont(plainFontSmall);
        contentPane.add(datePurchaseText);

        JDateChooser datePurchaseWrite = new JDateChooser();
        datePurchaseWrite.setBounds(500,350,150,40);
        datePurchaseWrite.setFont(plainFontSmall);
        contentPane.add(datePurchaseWrite);

        //Made------------------------------------------------------
        JLabel madeText = new JLabel("Date produced:");
        madeText.setBounds(300,400,150,40);
        madeText.setFont(plainFontSmall);
        contentPane.add(madeText);

        JDateChooser madeWrite = new JDateChooser();
        madeWrite.setBounds(500,400,150,40);
        madeWrite.setFont(plainFontSmall);
        contentPane.add(madeWrite);

        //Type------------------------------------------------------

        JLabel typeText = new JLabel("Type:");
        typeText.setBounds(300,450,150,40);
        typeText.setFont(plainFontSmall);
        contentPane.add(typeText);

        List<Integer> bufferIDType = new ArrayList<>(tc.getCache().getGroupKeys("typeCache"));

        String[] name = new String[bufferIDType.size()];
        for(int i = 0; i < bufferIDType.size(); i++){
            name[i] = Executor.idtoName(bufferIDType.get(i));
        }

        JComboBox box = new JComboBox(name);
        box.setFont(plainFontSmall);
        box.setSize(100,500);
        box.setBounds(500,450,150,50);
        String typ = (String)box.getSelectedItem();
        contentPane.add(box);

        // Commit_button---------------------------------------------------------------
        //Make a new bike-------------------------------------------------------------------------------------------------
        JButton commit = new JButton("Commit");
        commit.setBounds(500,600,150,40);
        commit.setFont(plainFontSmall);

        commit.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent p) {
                String price = priceWrite.getText();
                int priceInt1;

                try{
                    priceInt1 = Integer.parseInt(price);
                } catch (NumberFormatException n){
                    priceInt1 = -1;
                }


                //date purchase
                java.sql.Date theDateProduced = null;
                if (datePurchaseWrite.getDate() != null) {


                    SimpleDateFormat dateFormatP = new SimpleDateFormat("yyyy-MM-dd");
                    String datep;


                    datep = dateFormatP.format(datePurchaseWrite.getDate());
                    theDateProduced = java.sql.Date.valueOf(datep);

                   /* datep = "0000-00-00";
                    theDateProduced = java.sql.Date.valueOf(datep);*/
                }

                //date made
                java.sql.Date theDateMade1 =null;
                if(madeWrite.getDate() != null) {
                    SimpleDateFormat dateFormatM = new SimpleDateFormat("yyyy-MM-dd");
                    String datemade = dateFormatM.format(madeWrite.getDate());
                    theDateMade1 = java.sql.Date.valueOf(datemade);

                }

                String typ = (String)box.getSelectedItem();
                int typeInt1 = Executor.nametoID(typ);


                if(madeWrite.getDate() == null ||  datePurchaseWrite.getDate() == null ||priceInt1 == -1 || box.getItemCount() == 0){
                    JPanel panel = new JPanel();
                    panel.setLayout(null);
                    setLayout(new BorderLayout());
                    panel.setVisible(true);



                    JLabel text = new JLabel("You have not entered all fields, or type do not exist");
                    text.setFont(plainFontSmall);
                    text.setBounds(20,20,800,50);
                    panel.add(text);


                    frame.setContentPane(panel);
                    frame.setBounds(350,300,540,125);
                    frame.setVisible(true);
                }


                //Sjekker at alle parametere har fått input før det er lov å lage ny sykkel
                if(madeWrite.getDate() !=null && datePurchaseWrite.getDate() !=null  && priceInt1 != -1 && box.getItemCount() != 0 ){
                    if(theDateProduced != null && theDateMade1 != null && priceInt1 != -1) {
                        Executor.newBike(theDateProduced, priceInt1, typeInt1, theDateMade1);
                        main.objects.Bike bike = new main.objects.Bike(Executor.getAllBikes().get(Executor.getAllBikes().size() - 1).getBike_id(), theDateProduced, priceInt1, typeInt1, theDateMade1);
                        bike.setStatus(5);
                        bike.setCharge(100);
                        bc.addBikes(bike);
                        new Bike();
                        setVisible(false);
                        dispose();
                    }
                }
            }
        });
        contentPane.add(commit);

        //Slutt-------------------------------------------------------------------------------------------------
        setContentPane(contentPane);
        setSize(1500,800);
        setVisible(true);
        contentPane.add(mal);
        contentPane.add(mal2);
    }

    public static void main(String[]args){ new NewBike(); }
}
