/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@Authors: Annebelle Almås, Mathias Braathen, William Chakroun Jacobsen & Terje Haugum
@Date submitted: 23.04.2018
@Summary: In DeleteBike admin can delete a bike.

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////**/
package main.ui.gui_admin.bike_package;
import main.database.Executor;
import main.engine.cache.BikeCache;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

public class DeleteBike extends JFrame{
    private JPanel contentPane;
    private static Font plainFont = new Font("Avenir", Font.PLAIN, 30);
    private static Font plainFontSmall = new Font("Avenir", Font.PLAIN, 20);
    private static Font boldFont = new Font("Avenir", Font.BOLD, 50);
    private JPanel mal;
    private JPanel mal2;

    BikeCache bc = new BikeCache();

    public DeleteBike(){
        setTitle("Trondheim city delete bike");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        contentPane = new JPanel();
        contentPane.setLayout(null);


        mal2 = new JPanel();
        mal2.setLayout(null);
        mal2.setBackground(new Color(255,250,250));
        mal2.setBounds(0,0,1500,800);
        mal2.setVisible(true);

        mal = new JPanel();
        mal.setLayout(null);
        mal.setBackground(new Color(255,127,80));
        mal.setBounds(0,120,1500,5);
        mal.setVisible(true);
        //Table-----------------------------------------------------------------------

        int bufferSize =bc.getCache().getGroupKeys("bikeCache").size();
        List<Integer> bufferID = new ArrayList<>(bc.getCache().getGroupKeys("bikeCache"));

        Integer[] name = new Integer[bufferSize];
        for(int i = 0; i < bufferSize; i++){

            name[i] = bufferID.get(i);
        }

        //Heading---------------------------------------------------------------------
        JLabel headding = new JLabel("Trondheim Citybikes");
        headding.setBounds(40, 40, 800, 60);
        headding.setFont(boldFont);
        contentPane.add(headding);

        //Subheading---------------------------------------------------------------------
        JLabel subHeadding = new JLabel("BIKES");
        subHeadding.setBounds(900, 40, 600, 60);
        subHeadding.setFont(boldFont);
        contentPane.add(subHeadding);

        JLabel del = new JLabel("DELETE BIKE:");
        del.setBounds(900,250,250,40);
        del.setFont(plainFont);
        contentPane.add(del);

        //Back to main page---------------------------------------------------------------------
        JButton back = new JButton("Back to bike");
        back.setBounds(50, 150, 200, 50);
        back.setFont(plainFont);

        back.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                new Bike();
                setVisible(false);
                dispose();
            }
        });

        contentPane.add(back);

        // Check_button---------------------------------------------------------------
        JCheckBox sure = new JCheckBox("Are you sure?");
        sure.setBounds(900,350,200,40);
        sure.setBackground(new Color(255,250,250));
        sure.setFont(plainFontSmall);
        contentPane.add(sure);


        // Bike_id---------------------------------------------------------------
        JLabel idText = new JLabel("Bike_id:");
        idText.setBounds(900,300,200,40);
        idText.setFont(plainFontSmall);
        contentPane.add(idText);

        JComboBox<Integer> box = new JComboBox<>(name);
        box.setFont(plainFontSmall);
        box.setSize(100,500);
        box.setBounds(1000,300,100,40);
        contentPane.add(box);


        // Commit_button---------------------------------------------------------------
        JButton commit = new JButton("Commit");
        commit.setBounds(900,400,150,40);
        commit.setFont(plainFontSmall);
        commit.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                int id = (int)box.getSelectedItem();
                if(sure.isSelected()) {
                    Executor.deletebike(id);
                    bc.returnBike(id).setStatus(0);
                    new Bike();
                    setVisible(false);
                    dispose();
                }
            }
        });
        contentPane.add(commit);

        DeleteSortFilter test = new DeleteSortFilter();
        test.setBounds(50, 250, 775, 450);
        test.setFont(plainFontSmall);
        contentPane.add(test);


        //Slutt-------------------------------------------------------------------------------------------------
        setContentPane(contentPane);
        setSize(1500,800);
        setVisible(true);
        contentPane.add(mal);
        contentPane.add(mal2);
    }


    //main.ui.gui_admin.Main---------------------------------------------------------------------
    public static void main(String[]args){
        new DeleteBike();
    }
}
class DeleteSortFilter extends JPanel {

    private BikeCache bc = new BikeCache();


    private String[] statusType
            = {"Bike_id", "Price", "Date of purchase", "Charging level", "Antall repairs", "Total km", "Total trips", "Docked", "Made", "Type", "Status"};

    int bufferSize = bc.getCache().getGroupKeys("bikeCache").size();
    List<Integer> bufferID = new ArrayList<>(bc.getCache().getGroupKeys("bikeCache"));

    Object[][] data = {};
    private DefaultTableModel model = new DefaultTableModel(data, statusType) {
        public boolean isCellEditable(int row, int column) {
            return false;//This causes all cells to be not editable
        }

        @Override
        public Class getColumnClass(int column) {
            switch (column) {
                case 2:
                    return java.sql.Date.class;
                case 7:
                    return String.class;
                case 8:
                    return java.sql.Date.class;
                case 9:
                    return String.class;
                case 10:
                    return String.class;
                default:
                    return Integer.class;
            }
        }
    };
    private JTable jTable = new JTable(model);

    private TableRowSorter<TableModel> rowSorter
            = new TableRowSorter<>(jTable.getModel());

    private JTextField jtfFilter = new JTextField();

    private String converter(int status) {
        String str = "";
        switch (status) {
            case 1:
                str = "Docked";
                break;
            case 2:
                str = "In Repair";
                break;
            case 3:
                str = "Driving";
                break;
            case 4:
                str = "Need repair";
                break;
            case 5:
                str = "In Storage";
                break;
        }
        return str;
    }

    public DeleteSortFilter() {
        for (int i = 0; i < bufferSize; i++) {
            if (bc.returnBike(bufferID.get(i)).getStatus() == 0) {
                continue;
            }
            Object[] data = {new Integer(bc.returnBike(bufferID.get(i)).getBike_id()), new Integer(bc.returnBike(bufferID.get(i)).getPrice()),
                    bc.returnBike(bufferID.get(i)).getReg_date(), bc.returnBike(bufferID.get(i)).getCharge(),
                    bc.returnBike(bufferID.get(i)).getAnt_rep(), bc.returnBike(bufferID.get(i)).getTot_km(),
                    bc.returnBike(bufferID.get(i)).getTot_trips(), bc.returnBike(bufferID.get(i)).getDocked(),
                    bc.returnBike(bufferID.get(i)).getProd_date(), Executor.idtoName(bc.returnBike(bufferID.get(i)).getType_id()),
                    converter(bc.returnBike(bufferID.get(i)).getStatus())};
            model.addRow(data);
        }
        jTable.setRowSorter(rowSorter);

        JPanel panel = new JPanel(new BorderLayout());
        panel.add(new JLabel("Specify search:"),
                BorderLayout.WEST);
        panel.add(jtfFilter, BorderLayout.CENTER);

        setLayout(new BorderLayout());
        add(panel, BorderLayout.SOUTH);
        add(new JScrollPane(jTable), BorderLayout.CENTER);

        //method for adding a search bar, that actively listens for input and updates table
        jtfFilter.getDocument().addDocumentListener(new DocumentListener() {

            @Override
            public void insertUpdate(DocumentEvent e) {
                String text = jtfFilter.getText();

                if (text.trim().length() == 0) {
                    rowSorter.setRowFilter(null);
                } else {
                    rowSorter.setRowFilter(RowFilter.regexFilter("(?i)" + text));
                }
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                String text = jtfFilter.getText();

                if (text.trim().length() == 0) {
                    rowSorter.setRowFilter(null);
                } else {
                    rowSorter.setRowFilter(RowFilter.regexFilter("(?i)" + text));
                }
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
    }
}