package main.ui.gui_admin.bike_package;



import main.database.Executor;
import main.engine.cache.BikeCache;
import main.engine.cache.TypeCache;


import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

public class Type1 extends JFrame{

    private JPanel contentPane;
    private JPanel content;
    private JFrame frame;
    private static Font plainFont = new Font("Avenir", Font.PLAIN, 30);
    private static Font plainFontSmall = new Font("Avenir", Font.PLAIN, 20);
    private static Font plainFontSmallXL = new Font("Avenir", Font.PLAIN, 15);
    private static Font boldFont = new Font("Avenir", Font.BOLD, 50);
    private static Font boldFontSmall = new Font("Avenir", Font.BOLD, 20);
    private JPanel mal;
    private JFrame frame2;
    private JPanel mal2;
    private JPanel mal3;
    private JPanel mal4;


    TypeCache tc = new TypeCache();

    public Type1(){
        frame = new JFrame();
        frame2 = new JFrame();
        setTitle("Trondheim city delete bike");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        contentPane = new JPanel();
        contentPane.setLayout(null);

        mal2 = new JPanel();
        mal2.setLayout(null);
        mal2.setBackground(new Color(255,250,250));
        mal2.setBounds(0,0,1500,800);
        mal2.setVisible(true);

        mal = new JPanel();
        mal.setLayout(null);
        mal.setBackground(new Color(255,127,80));
        mal.setBounds(0,120,1500,5);
        mal.setVisible(true);

        //creation of Type table------------------------------------------------------

        List<Integer> bufferIDType = new ArrayList<>(tc.getCache().getGroupKeys("typeCache"));
        ArrayList<main.objects.Type> types = new ArrayList<>();

        String[] nameTyp = new String[bufferIDType.size()];

        for(int i = 0; i < bufferIDType.size(); i++){
            nameTyp[i] = tc.returnType(bufferIDType.get(i)).getName();
            types.add(tc.returnType(bufferIDType.get(i)));
        }

        //Heading---------------------------------------------------------------------
        JLabel headding = new JLabel("Trondheim Citybikes");
        headding.setBounds(40, 40, 800, 60);
        headding.setFont(boldFont);
        contentPane.add(headding);

        //Subheading---------------------------------------------------------------------
        JLabel subHeadding = new JLabel("BIKES");
        subHeadding.setBounds(900, 40, 600, 60);
        subHeadding.setFont(boldFont);
        contentPane.add(subHeadding);

        JLabel del = new JLabel("EDIT TYPE:");
        del.setBounds(50,225,250,40);
        del.setFont(plainFont);
        contentPane.add(del);

        //Back to main page---------------------------------------------------------------------
        JButton back = new JButton("Back to Bike");
        back.setBounds(50, 150, 200, 50);
        back.setFont(plainFont);

        back.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                new Bike();
                setVisible(false);
                dispose();
            }
        });

        contentPane.add(back);

        //New type---------------------------------------------------------------------
        JLabel newTypeText = new JLabel("New type:");
        newTypeText.setBounds(600,325,150,40);
        newTypeText.setFont(plainFontSmall);
        contentPane.add(newTypeText);

        JTextField newTypeWrite = new JTextField();
        newTypeWrite.setFont(plainFontSmall);
        contentPane.add(newTypeWrite);
        newTypeWrite.setBounds(750,325,150,40);



        //Delete type---------------------------------------------------------------------
        JLabel deleteTypeText = new JLabel("Delete type:");
        deleteTypeText.setBounds(600,375,150,40);
        deleteTypeText.setFont(plainFontSmall);
        contentPane.add(deleteTypeText);

        JComboBox box = new JComboBox(nameTyp);
        box.setFont(plainFontSmall);

        box.setSize(150,500);

        box.setBounds(750,375,150,50);
        String typ = (String)box.getSelectedItem();
        contentPane.add(box);



        //Edit type from---------------------------------------------------------------
        JLabel editTypeText = new JLabel("Edit type from:");
        editTypeText.setBounds(600,425,150,40);
        editTypeText.setFont(plainFontSmall);
        contentPane.add(editTypeText);

        JComboBox box2 = new JComboBox(nameTyp);
        box2.setFont(plainFontSmall);

        box2.setSize(150,500);


        box2.setBounds(750,425,150,50);
        String typ2 = (String)box2.getSelectedItem();
        contentPane.add(box2);



        //Edit type to----------------------------------------------------------------

        JLabel editTypetoText =  new JLabel("To:");
        editTypetoText.setBounds(925,425,150,40);
        editTypetoText.setFont(plainFontSmall);
        contentPane.add(editTypetoText);

        JTextField editTypetoWrite = new JTextField();
        editTypetoWrite.setBounds(975,425,150,40);

        editTypetoWrite.setFont(plainFontSmall);
        contentPane.add(editTypetoWrite);



        // Button--------------------------------------------------------
        JButton commit = new JButton("Commit");
        commit.setBounds(750,485,150,40);
        commit.setFont(plainFontSmall);

        commit.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                //JFrame f = new JFrame();
                //f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                //f.setLayout(new BorderLayout());
                content = new JPanel();
                content.setLayout(null);


                mal3 = new JPanel();
                mal3.setLayout(null);
                mal3.setBounds(400,200,400,300);
                mal3.setBackground(new Color(255,250,250));
                mal3.setVisible(true);
                content.add(mal3);

                mal4 = new JPanel();
                mal4.setLayout(null);
                mal4.setBounds(400,200,400,5);
                mal4.setBackground(new Color(255,127,80));
                mal4.setVisible(true);
                content.add(mal4);




                setTitle("Are you sure you want to make these changes?");
                setLayout(new BorderLayout());
                content = new JPanel();
                content.setLayout(null);

                JLabel title = new JLabel("Are you sure you want to:");
                title.setFont(boldFontSmall);
                title.setBounds(20,30,350,40);
                content.add(title);

                JLabel new1 = new JLabel("Create a new type:");
                new1.setFont(plainFontSmallXL);
                new1.setBounds(20,100,400,40);
                content.add(new1);

                JCheckBox boxnew = new JCheckBox();
                boxnew.setBounds(150,100,40,40);
                content.add(boxnew);


                JLabel delete = new JLabel("Delete a type:");
                delete.setFont(plainFontSmallXL);
                delete.setBounds(20,150,200,40);
                content.add(delete);

                JCheckBox boxdel = new JCheckBox();
                boxdel.setBounds(150,150,40,40);
                content.add(boxdel);

                JLabel edit = new JLabel("Edit a type:");
                edit.setFont(plainFontSmallXL);
                edit.setBounds(20,200,200,40);
                content.add(edit);


                JCheckBox boxedit = new JCheckBox();
                boxedit.setBounds(150,200,40,40);
                content.add(boxedit);


                JButton submit = new JButton("Submit");
                submit.setFont(plainFontSmallXL);
                submit.setBounds(225,210,70,20);
                content.add(submit);

                JButton cancel = new JButton("Cancel");
                cancel.setFont(plainFontSmallXL);
                cancel.setBounds(300,210,70,20);
                content.add(cancel);


                submit.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {

                        if(boxnew.isSelected() && !newTypeWrite.getText().isEmpty()) {
                            String newT = newTypeWrite.getText();
                            Executor.newType(newT);
                            tc.addType(Executor.getSpesificType(newT));
                        }


                        if(boxdel.isSelected() && box.getItemCount() != 0) {
                           String delete = (String)box.getSelectedItem();
                            tc.removetype(Executor.nametoID(delete));
                            Executor.deleteType(delete);
                        }
                        if(boxedit.isSelected() && !editTypetoWrite.getText().isEmpty() && box2.getItemCount() != 0){
                            String fromTyp = (String)box2.getSelectedItem();
                            String toTyp = editTypetoWrite.getText();
                            tc.editType(Executor.nametoID(fromTyp), toTyp);
                            Executor.editType(fromTyp, toTyp);
                        }

                        if((boxnew.isSelected() && !(newTypeWrite.getText().isEmpty())) || (boxdel.isSelected() && box.getItemCount() != 0) || (boxedit.isSelected() && !(editTypetoWrite.getText().isEmpty()))) {
                            new Bike();
                            setVisible(false);
                            frame.dispose();
                            dispose();
                        }else{
                            JPanel panel = new JPanel();
                            panel.setLayout(null);
                            setLayout(new BorderLayout());
                            panel.setVisible(true);

                            JLabel text = new JLabel("You have not entered the fields you wanted to change");
                            text.setFont(plainFontSmall);
                            text.setBounds(20,20,900,50);
                            panel.add(text);

                            frame2.setContentPane(panel);
                            frame2.setBounds(350,300,540,125);
                            frame2.setVisible(true);
                            frame.dispose();
                        }


                    }
                });


                //Slutt-------------------------------------------------------------------------------------------------
                frame.add(content);
                frame.setBounds(400,200,400,300);
                frame.setVisible(true);




                cancel.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        frame.setVisible(false);
                        frame.dispose();
                    }
                });

            }
        });
        contentPane.add(commit);


        TypeSortFilter test = new TypeSortFilter();
        test.setBounds(50,325,375,350);
        test.setFont(plainFontSmall);
        contentPane.add(test);


        //Slutt-------------------------------------------------------------------------------------------------
        setContentPane(contentPane);
        setSize(1500,800);
        setVisible(true);
        contentPane.add(mal);
        contentPane.add(mal2);
    }



    //Main---------------------------------------------------------------------
    public static void main(String[]args){ new Type1(); }
}
class TypeSortFilter extends JPanel {

    TypeCache tc = new TypeCache();

    private BikeCache bc = new BikeCache();

    private String[] statusType
            = {"Type_id","Type_name"};

    List<Integer> bufferIDType = new ArrayList<>(tc.getCache().getGroupKeys("typeCache"));
    ArrayList<main.objects.Type> types = new ArrayList<>();

    String[] nameTyp = new String[bufferIDType.size()];


    int bufferSize = bc.getCache().getGroupKeys("bikeCache").size();
    List<Integer> bufferID = new ArrayList<>(bc.getCache().getGroupKeys("bikeCache"));

    Object[][] data = {};
    private DefaultTableModel model = new DefaultTableModel(data, statusType) {
        public boolean isCellEditable(int row, int column) {
            return false;//This causes all cells to be not editable
        }
        @Override
        public Class getColumnClass(int column) {
            switch (column) {
                case 2:
                    return java.sql.Date.class;
                case 7:
                    return String.class;
                case 8:
                    return java.sql.Date.class;
                case 9:
                    return String.class;
                case 10:
                    return String.class;
                default:
                    return Integer.class;
            }
        }
    };
    private JTable jTable = new JTable(model);

    private TableRowSorter<TableModel> rowSorter
            = new TableRowSorter<>(jTable.getModel());

    private JTextField jtfFilter = new JTextField();

    public TypeSortFilter() {
        String headline[]={"Type_id","Type_name"};

        DefaultTableModel tableModel = new DefaultTableModel(headline, 0);
        JTable table = new JTable(tableModel);

        for(int i = 0; i < bufferIDType.size(); i++){
            nameTyp[i] = tc.returnType(bufferIDType.get(i)).getName();
            types.add(tc.returnType(bufferIDType.get(i)));
        }

        for(main.objects.Type type: types) {
            int id = type.getType_id();
            String nameOfType = type.getName();

            Object[] data = {id, nameOfType};
            model.addRow(data);
        }

        table.setPreferredScrollableViewportSize(new Dimension(80,200));
        table.setFillsViewportHeight(true);


        TableColumnModel columnModel = table.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(80);
        columnModel.getColumn(1).setPreferredWidth(100);


        JScrollPane scroll = new JScrollPane(table);

        scroll.setSize(20,20);
        scroll.setBounds(50,325,375,350);

        jTable.setRowSorter(rowSorter);

        JPanel panel = new JPanel(new BorderLayout());
        panel.add(new JLabel("Specify search:"),
                BorderLayout.WEST);
        panel.add(jtfFilter, BorderLayout.CENTER);

        setLayout(new BorderLayout());
        add(panel, BorderLayout.SOUTH);
        add(new JScrollPane(jTable), BorderLayout.CENTER);

        //method for adding a search bar, that actively listens for input and updates table
        jtfFilter.getDocument().addDocumentListener(new DocumentListener() {

            @Override
            public void insertUpdate(DocumentEvent e) {
                String text = jtfFilter.getText();

                if (text.trim().length() == 0) {
                    rowSorter.setRowFilter(null);
                } else {
                    rowSorter.setRowFilter(RowFilter.regexFilter("(?i)" + text));
                }
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                String text = jtfFilter.getText();

                if (text.trim().length() == 0) {
                    rowSorter.setRowFilter(null);
                } else {
                    rowSorter.setRowFilter(RowFilter.regexFilter("(?i)" + text));
                }
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
    }
}
