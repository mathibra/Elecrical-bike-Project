/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@Authors: Annebelle Almås, Mathias Braathen, William Chakroun Jacobsen & Terje Haugum
@Date submitted: 23.04.2018
@Summary: In Bike admin have overview of the bikes in a table, and can edit bikes, typs, make new bikes, delete bikes and
send the bikes to repair.

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////**/
package main.ui.gui_admin.bike_package;

import main.database.Executor;
import main.engine.cache.BikeCache;
import main.engine.cache.DockCache;
import main.engine.Maps;
import main.objects.Docking;


import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;
import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import javax.swing.table.TableColumnModel;

public class Bike extends JFrame {
    long start = System.currentTimeMillis();
    private JPanel contentPane = new JPanel(new BorderLayout());
    private static Font plainFont = new Font("Avenir", Font.PLAIN, 30);
    private static Font plainFontSmall = new Font("Avenir", Font.PLAIN, 20);
    private static Font boldFont = new Font("Avenir", Font.BOLD, 50);
    private static Font plainFontSmall10 = new Font("Avenir", Font.BOLD, 15);
    private JPanel mal;
    private JPanel mal2;

    private BikeCache bc = new BikeCache();
    private DockCache dc = new DockCache();
    String headline[] = {"Bike_id", "Price", "Date of purchase", "Charging level", "Antall repairs", "Total km", "Total trips", "Docked", "Made", "Type", "Status"};

    private DefaultTableModel tableModel = new DefaultTableModel(headline, 0);
    private JTable table = new JTable(tableModel);

    public Bike() {
        setTitle("Trondheim city bike");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        contentPane = new JPanel();
        contentPane.setLayout(null);

        mal2 = new JPanel();
        mal2.setLayout(null);
        mal2.setBackground(new Color(255, 250, 250));
        mal2.setBounds(0, 0, 1500, 800);
        mal2.setVisible(true);

        mal = new JPanel();
        mal.setLayout(null);
        mal.setBackground(new Color(255, 127, 80));
        mal.setBounds(0, 120, 1500, 5);
        mal.setVisible(true);


        //Heading---------------------------------------------------------------------
        JLabel headding = new JLabel("Trondheim Citybikes");
        headding.setBounds(40, 40, 800, 60);
        headding.setFont(boldFont);
        contentPane.add(headding);

        //Subheading---------------------------------------------------------------------
        JLabel subHeadding = new JLabel("BIKES");
        subHeadding.setBounds(875, 40, 600, 60);
        subHeadding.setFont(boldFont);
        contentPane.add(subHeadding);


        //Back to main page---------------------------------------------------------------------
        JButton back = new JButton("Main page");
        back.setBounds(50, 150, 200, 50);
        back.setFont(plainFont);

        back.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                new main.ui.gui_admin.Main();
                setVisible(false);
                dispose();
            }
        });

        contentPane.add(back);

        TableColumnModel columnModel = table.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(80);
        columnModel.getColumn(1).setPreferredWidth(100);
        columnModel.getColumn(2).setPreferredWidth(150);
        columnModel.getColumn(3).setPreferredWidth(100);
        columnModel.getColumn(4).setPreferredWidth(100);
        columnModel.getColumn(5).setPreferredWidth(100);
        columnModel.getColumn(6).setPreferredWidth(100);
        columnModel.getColumn(7).setPreferredWidth(100);
        columnModel.getColumn(8).setPreferredWidth(100);

        JScrollPane scroll = new JScrollPane(table);


        scroll.setSize(100, 100);
        scroll.setBounds(50, 400, 700, 350);

//        contentPane.add(scroll);

        //Map---------------------------------------------------------------------
        JLabel mapText = new JLabel("Position of docking stations & bikes");
        mapText.setFont(plainFontSmall10);
        mapText.setBounds(875,150,250,40);
        contentPane.add(mapText);

        JPanel mapPane = new JPanel( new BorderLayout());
        contentPane.add(mapPane);
        Maps m = new Maps(mapPane,875,190,300,250);

        int bufferSize = bc.getCache().getGroupKeys("bikeCache").size();
        List<Integer> bufferID = new ArrayList<>(bc.getCache().getGroupKeys("bikeCache"));

        main.objects.Bike[] bikes_array = new main.objects.Bike[bufferID.size()];

        for(int i = 0; i < bufferID.size(); i++){
            bikes_array[i] = bc.returnBike(bufferID.get(i));
        }

        List<Integer> bufferID_D = new ArrayList<>(dc.getCache().getGroupKeys("dockCache"));
        Docking[] dock_array = new Docking[bufferID_D.size()];

        for(int i = 0; i < bufferID_D.size(); i++){
            dock_array[i] = dc.returnDock(bufferID_D.get(i));
        }

        m.addBikes(bikes_array);
        m.addDocks(dock_array);

        //Refresh map---------------------------------------------------------------------
        JButton refresh =  new JButton("Refresh map");
        refresh.setBounds(1175,400,100,40);
        refresh.setFont(plainFontSmall10);

        refresh.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                ArrayList<main.objects.Bike> bikes = new ArrayList<>(Executor.getAllBikes());
                ArrayList<Docking> docks = new ArrayList<>(Executor.getAllDockings());
                Docking[] dockings = new Docking[docks.size()];
                main.objects.Bike[] bike = new main.objects.Bike[bikes.size()];
                bikes.toArray(bike);
                docks.toArray(dockings);
                m.updateBikes(bike);
                refresh.setVisible(true);
            }
        });
        contentPane.add(refresh);

        //Edit type------------------------------------------------------------------------
        JButton editType = new JButton("Edit Type");
        editType.setBounds(875, 450, 150, 60);
        editType.setFont(plainFontSmall);

        editType.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                new Type1();
                setVisible(false);
                dispose();
            }
        });

        contentPane.add(editType);
        //New bike------------------------------------------------------------------------
        JButton nBike = new JButton("New Bike");
        nBike.setBounds(1025, 450, 150, 60);
        nBike.setFont(plainFontSmall);

        nBike.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                new NewBike();
                setVisible(false);
                dispose();
            }
        });

        contentPane.add(nBike);
        //Repair bike------------------------------------------------------------------------
        JButton repBike = new JButton("Repair Bike");
        repBike.setBounds(1025, 525, 150, 60);
        repBike.setFont(plainFontSmall);

        repBike.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                new Repair();
                setVisible(false);
                dispose();
            }
        });

        contentPane.add(repBike);

        //Delete bike------------------------------------------------------------------------
        JButton delBike = new JButton("Delete Bike");
        delBike.setBounds(875, 525, 150, 60);
        delBike.setFont(plainFontSmall);

        delBike.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                new DeleteBike();
                setVisible(false);
                dispose();
            }
        });
        contentPane.add(delBike);

        //Edit bike------------------------------------------------------------------------
        JButton editBike = new JButton("Edit Bike");
        editBike.setBounds(875, 600, 150, 60);
        editBike.setFont(plainFontSmall);

        editBike.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                new EditBike();
                setVisible(false);
                dispose();
            }
        });

        contentPane.add(editBike);

        TableSortFilter test = new TableSortFilter();
        test.setBounds(50, 250, 775, 450);
        test.setFont(plainFontSmall);
        contentPane.add(test);


        //Slutt-----------------------------------------------------------------------------------------------
        setContentPane(contentPane);

        contentPane.setSize(1500, 800);
        contentPane.setVisible(true);
        contentPane.add(mal);
        contentPane.add(mal2);


        setSize(1500, 800);
        setVisible(true);
        long elapsedTIme = System.currentTimeMillis() - start;

        System.out.println("Time to load Bike-GUI: " + elapsedTIme);
    }

    //main.ui.gui_admin.Main---------------------------------------------------------------------
    public static void main(String[] args){
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Bike();
            }
        });
    }
}

//Class to add table for overview of bikes, and adding search filter for easy access
class TableSortFilter extends JPanel {

    private static BikeCache bc = new BikeCache();


    private String[] statusType
            = {"Bike_id", "Price", "Date of purchase", "Charging level", "Antall repairs", "Total km", "Total trips", "Docked", "Made", "Type", "Status"};

    int bufferSize = bc.getCache().getGroupKeys("bikeCache").size();
    List<Integer> bufferID = new ArrayList<>(bc.getCache().getGroupKeys("bikeCache"));

    Object[][] data = {};
    private DefaultTableModel model = new DefaultTableModel(data, statusType) {
        public boolean isCellEditable(int row, int column) {
            return false;//This causes all cells to be not editable
        }

        @Override
        public Class getColumnClass(int column) {
            switch (column) {
                case 2:
                    return java.sql.Date.class;
                case 7:
                    return String.class;
                case 8:
                    return java.sql.Date.class;
                case 9:
                    return String.class;
                case 10:
                    return String.class;
                default:
                    return Integer.class;
            }
        }
    };
    private JTable jTable = new JTable(model);

    private TableRowSorter<TableModel> rowSorter
            = new TableRowSorter<>(jTable.getModel());

    private JTextField jtfFilter = new JTextField();
    private JButton jbtFilter = new JButton("Filter");

    private String converter(int status) {
        String str = "";
        switch (status) {
            case 1:
                str = "Docked";
                break;
            case 2:
                str = "In Repair";
                break;
            case 3:
                str = "Driving";
                break;
            case 4:
                str = "Need repair";
                break;
            case 5:
                str = "In Storage";
                break;
        }
        return str;
    }

    public TableSortFilter() {
        for (int i = 0; i < bufferSize; i++) {
            if (bc.returnBike(bufferID.get(i)).getStatus() == 0) {
                continue;
            }
            Object[] data = {new Integer(bc.returnBike(bufferID.get(i)).getBike_id()), new Integer(bc.returnBike(bufferID.get(i)).getPrice()),
                    bc.returnBike(bufferID.get(i)).getReg_date(), bc.returnBike(bufferID.get(i)).getCharge(),
                    bc.returnBike(bufferID.get(i)).getAnt_rep(), bc.returnBike(bufferID.get(i)).getTot_km(),
                    bc.returnBike(bufferID.get(i)).getTot_trips(), bc.returnBike(bufferID.get(i)).getDocked(),
                    bc.returnBike(bufferID.get(i)).getProd_date(), Executor.idtoName(bc.returnBike(bufferID.get(i)).getType_id()),
                    converter(bc.returnBike(bufferID.get(i)).getStatus())};
            model.addRow(data);
        }
        jTable.setRowSorter(rowSorter);

        JPanel panel = new JPanel(new BorderLayout());
        panel.add(new JLabel("Specify search:"),
                BorderLayout.WEST);
        panel.add(jtfFilter, BorderLayout.CENTER);

        setLayout(new BorderLayout());
        add(panel, BorderLayout.SOUTH);
        add(new JScrollPane(jTable), BorderLayout.CENTER);

        //method for adding a search bar, that actively listens for input and updates table
        jtfFilter.getDocument().addDocumentListener(new DocumentListener() {

            @Override
            public void insertUpdate(DocumentEvent e) {
                String text = jtfFilter.getText();

                if (text.trim().length() == 0) {
                    rowSorter.setRowFilter(null);
                } else {
                    rowSorter.setRowFilter(RowFilter.regexFilter("(?i)" + text));
                }
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                String text = jtfFilter.getText();

                if (text.trim().length() == 0) {
                    rowSorter.setRowFilter(null);
                } else {
                    rowSorter.setRowFilter(RowFilter.regexFilter("(?i)" + text));
                }
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
    }
}
