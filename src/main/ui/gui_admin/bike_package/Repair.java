/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@Authors: Annebelle Almås, Mathias Braathen, William Chakroun Jacobsen & Terje Haugum
@Date submitted: 23.04.2018
@Summary: In Repair the admin can send a bike to repair.

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////**/
package main.ui.gui_admin.bike_package;
import main.database.Executor;
import main.engine.cache.BikeCache;

import com.toedter.calendar.JDateChooser;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import java.text.SimpleDateFormat;

public class Repair extends JFrame{
    private JPanel contentPane;
    private static Font plainFont = new Font("Avenir", Font.PLAIN, 30);
    private static Font plainFontSmall = new Font("Avenir", Font.PLAIN, 20);
    private static Font boldFontSmall = new Font("Avenir", Font.BOLD, 30);
    private static Font boldFont = new Font("Avenir", Font.BOLD, 50);
    private JPanel mal;
    private JPanel mal2;
    private JFrame frame;

    BikeCache bc = new BikeCache();


    public Repair(){
        frame = new JFrame();
        setTitle("Trondheim city bike");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        contentPane = new JPanel();
        contentPane.setLayout(null);


        mal2 = new JPanel();
        mal2.setLayout(null);
        mal2.setBackground(new Color(255,250,250));
        mal2.setBounds(0,0,1500,800);
        mal2.setVisible(true);

        mal = new JPanel();
        mal.setLayout(null);
        mal.setBackground(new Color(255,127,80));
        mal.setBounds(0,120,1500,5);
        mal.setVisible(true);

        //Heading---------------------------------------------------------------------
        JLabel headding = new JLabel("Trondheim Citybikes");
        headding.setBounds(40, 40, 800, 60);
        headding.setFont(boldFont);
        contentPane.add(headding);

        //Subheading---------------------------------------------------------------------
        JLabel subHeadding = new JLabel("BIKES");
        subHeadding.setBounds(800, 50, 600, 60);
        subHeadding.setFont(boldFont);
        contentPane.add(subHeadding);

        JLabel del = new JLabel("REPAIR BIKE:");
        del.setBounds(600,150,250,40);
        del.setFont(plainFont);
        contentPane.add(del);




        //Back to main page---------------------------------------------------------------------
        JButton back = new JButton("Back to bike");
        back.setBounds(50, 150, 200, 50);
        back.setFont(plainFont);

        back.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                new Bike();
                setVisible(false);
                dispose();
            }
        });

        contentPane.add(back);



        //Bike_id---------------------------------------------------------------------
        JLabel idText = new JLabel("Bike_id:");
        idText.setBounds(600,225,200,40);
        idText.setFont(plainFontSmall);
        contentPane.add(idText);

        int bufferSize =bc.getCache().getGroupKeys("bikeCache").size();
        List<Integer> bufferID = new ArrayList<>(bc.getCache().getGroupKeys("bikeCache"));

        Integer[] name = new Integer[bufferSize];
        for(int i = 0; i < bufferSize; i++){
            name[i] = bufferID.get(i);
        }

        JComboBox<Integer> box = new JComboBox<>(name);
        box.setFont(plainFontSmall);
        box.setSize(100,500);
        box.setBounds(800,225,150,40);
        Integer typ = (Integer)box.getSelectedItem();
        contentPane.add(box);

        //Repair from date---------------------------------------------------------------------
        JLabel dateText = new JLabel("From:");
        dateText.setBounds(600,275,200,40);
        dateText.setFont(plainFontSmall);
        contentPane.add(dateText);


        JDateChooser dateWrite = new JDateChooser();
        dateWrite.setBounds(800,275,150,40);
        contentPane.add(dateWrite);

        //Reason---------------------------------------------------------------------
        JLabel reasonText = new JLabel("Reason:");
        reasonText.setBounds(600,325,200,40);
        reasonText.setFont(plainFontSmall);
        contentPane.add(reasonText);


        JTextArea reasonWrite = new JTextArea();
        JScrollPane scroll = new JScrollPane(reasonWrite);
        scroll.setBounds(600,375,500,200);
        reasonWrite.setEditable(true);
        contentPane.add(scroll);



        //Repair bike skriv---------------------------------------------------------------------
        JButton commit = new JButton("Commit");
        commit.setBounds(600,600,150,40);
        commit.setFont(plainFontSmall);
        commit.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                //bikeid, fromdate, reason
                int id = (int)box.getSelectedItem();


                java.sql.Date fromDate = null;
                if(dateWrite.getDate() != null) {
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    String from = dateFormat.format(dateWrite.getDate());
                    fromDate = java.sql.Date.valueOf(from);
                }
                String reason = reasonWrite.getText();

                //This will only happen if all parameters are filled
                if(box.getItemCount() != 0 && dateWrite.getDate() != null && reason != null && !(reasonWrite.getText().isEmpty())){
                    int repairs= bc.returnBike(id).getAnt_rep()+1;
                    Executor.addRepairBike(id,fromDate,reason);
                    bc.returnBike(id).setAnt_rep(repairs);
                    new Bike();
                    setVisible(false);
                    dispose();
                }else{
                    JPanel panel = new JPanel();
                    panel.setLayout(null);
                    setLayout(new BorderLayout());
                    panel.setVisible(true);

                    JLabel text = new JLabel("You have not entered all fields or it doesn't exist any bikes");
                    text.setFont(plainFontSmall);
                    text.setBounds(20,20,900,50);
                    panel.add(text);

                    frame.setContentPane(panel);
                    frame.setBounds(350,300,540,125);
                    frame.setVisible(true);
                }
            }
        });
        contentPane.add(commit);

        RepairSortFilter test = new RepairSortFilter();
        test.setBounds(50, 225, 400, 400);
        test.setFont(plainFontSmall);
        contentPane.add(test);

        //Slutt-------------------------------------------------------------------------------------------------
        setContentPane(contentPane);
        setSize(1500,800);
        setVisible(true);
        contentPane.add(mal);
        contentPane.add(mal2);
    }

    //Main---------------------------------------------------------------------
    public static void main(String[]args){
        new Repair();
    }
}
class RepairSortFilter extends JPanel {

    private BikeCache bc = new BikeCache();


    private String[] statusType
            = {"Bike_id", "Status"};

    int bufferSize = bc.getCache().getGroupKeys("bikeCache").size();
    List<Integer> bufferID = new ArrayList<>(bc.getCache().getGroupKeys("bikeCache"));

    Object[][] data = {};
    private DefaultTableModel model = new DefaultTableModel(data, statusType) {
        public boolean isCellEditable(int row, int column) {
            return false;//This causes all cells to be not editable
        }

        @Override
        public Class getColumnClass(int column) {
            switch (column) {
                case 2:
                    return java.sql.Date.class;
                case 7:
                    return String.class;
                case 8:
                    return java.sql.Date.class;
                case 9:
                    return String.class;
                case 10:
                    return String.class;
                default:
                    return Integer.class;
            }
        }
    };
    private JTable jTable = new JTable(model);

    private TableRowSorter<TableModel> rowSorter
            = new TableRowSorter<>(jTable.getModel());

    private JTextField jtfFilter = new JTextField();
    private JButton jbtFilter = new JButton("Filter");

    private String converter(int status) {
        String str = "";
        switch (status) {
            case 1:
                str = "Docked";
                break;
            case 2:
                str = "In Repair";
                break;
            case 3:
                str = "Driving";
                break;
            case 4:
                str = "Need repair";
                break;
            case 5:
                str = "In Storage";
                break;
        }
        return str;
    }

    public RepairSortFilter() {
        for (int i = 0; i < Executor.getAllRepairs().size(); i++) {
            Object[] data = {Executor.getAllRepairs().get(i).getBike_id(), converter(Executor.getAllRepairs().get(i).getStatus())};
            model.addRow(data);
        }
        jTable.setRowSorter(rowSorter);

        JPanel panel = new JPanel(new BorderLayout());
        panel.add(new JLabel("Specify search:"),
                BorderLayout.WEST);
        panel.add(jtfFilter, BorderLayout.CENTER);

        setLayout(new BorderLayout());
        add(panel, BorderLayout.SOUTH);
        add(new JScrollPane(jTable), BorderLayout.CENTER);

        //method for adding a search bar, that actively listens for input and updates table
        jtfFilter.getDocument().addDocumentListener(new DocumentListener() {

            @Override
            public void insertUpdate(DocumentEvent e) {
                String text = jtfFilter.getText();

                if (text.trim().length() == 0) {
                    rowSorter.setRowFilter(null);
                } else {
                    rowSorter.setRowFilter(RowFilter.regexFilter("(?i)" + text));
                }
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                String text = jtfFilter.getText();

                if (text.trim().length() == 0) {
                    rowSorter.setRowFilter(null);
                } else {
                    rowSorter.setRowFilter(RowFilter.regexFilter("(?i)" + text));
                }
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
    }
}
