package main.simulator;

import main.objects.Customer;
import main.objects.Repair;

import java.time.LocalDate;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Class for generating nandom things used for simulation
 */
public class RandomGen extends Names {
    public RandomGen(){}

    private int lengthOfArray =  numGen(30);

    /**
     * For generating a random number
     * @param length        int indicating the maximum length for the number
     * @return              an int representing the random number generated
     */
    public static int numGen(int length){
        Random x = new Random();
        int num = x.nextInt(length-1)+1;
        return Math.abs(num);
    }

    /**
     * For generating a random number
     * @param length        int indicating the maximum length for the number
     * @return              an int representing the random number generated
     */
    public static int IndexGen(int length){
        Random x = new Random();
        int num = x.nextInt(length);

        return Math.abs(num);
    }


    /**
     * Makes a random price up to 5000
     * @return  an int representing the price generated
     */
    public static int priceGen(){
        Random x = new Random();
        int price = x.nextInt(5000)+1;
        return Math.abs(price);
    }

    /**
     * Card number generator
     * @return a number representing the the card number generated. Datatype is long
     */
    public static long cardGen(){
        Random rand = new Random();
        long cardnum = (rand.nextLong());
        return Math.abs(cardnum);
    }

    /**
     * This method is used to calculate the chance of bikes going out of repair.
     * @return true if a bike is finished, false if not
     */
    public boolean chance(){
        Random x = new Random();
        int i = x.nextInt(100);
        if(i < 12){
            return true;
        }
        return false;
    }


    /**
     * Random repair. If it needs repair it return true else it returns false.
     * @param bike_id   int representing the id of the bike to check if needs repair
     * @return          true if it need repair, false if not
     */
    public boolean repairGen(int bike_id){
        Random x = new Random();
        String request = "Bike needs repair";
        String description = "Bike has been repaired";
        int chance = x.nextInt(99) +1;
        if(chance > 70/*chance == 4 || chance == 82 || chance == 39*/){
            new Repair(bike_id, dateNow() ,request);
            return true;
        }
        return  false;
    }

    //

    /**
     * Create random customer
     * @return      a customer object with the randomly generated names and card number
     * @see Names
     */
    public Customer customerGen(){
        Customer c = new Customer(firstNameGen(), surnameGen(), (numGen(11)+8050));
        return c;
    }

    /**
     * Makes an array of random longitude coordinates
     * @param endLongdetude     double representing the longitude generated
     * @return                  an arraylist of the longitudes generated
     */
    public ArrayList<Double> longGen(double endLongdetude){
        Random x = new Random();
        ArrayList<Double> longdi = new ArrayList<>();
        for(int i = 0; i < lengthOfArray; i++){
            longdi.add(x.nextDouble()+10.05);
        }
        longdi.add(endLongdetude);
        return longdi;
    }

    /**
     * Makes an array of latitude coordinates
     * @param endLatitude       double representing the latitude generated
     * @return                  an arraylist of the latitudes generated
     */
    public ArrayList<Double> latGen(double endLatitude){
        Random x = new Random();
        ArrayList<Double> lati = new ArrayList<>();

        for(int i = 0; i <lengthOfArray; i++) {
            lati.add(x.nextDouble() + 63.05);
        }
        lati.add(endLatitude);
        return lati;
    }

    protected static double longGenD(){
        Random x = new Random();
        double longd = x.nextDouble() + 10;
        return longd;
    }

    protected static double latGenD(){
        Random x = new Random();
        double lat = x.nextDouble() + 63;
        return lat;
    }

    public Date dateNow(){
        Date date = new Date(Calendar.getInstance().getTime().getTime());
        return date;
    }
}
