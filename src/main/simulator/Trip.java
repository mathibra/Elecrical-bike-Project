/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@Authors: Annebelle Almås, Mathias Braathen, William Chakroun Jacobsen & Terje Haugum
@Date submitted: 23.04.2018
@Summary: The class is a part of the simulation.
It makes at trip, every trip wil also be in the tripRunner until it calls bookKeepingTripEnd.

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////**/

package main.simulator;


import main.objects.Bike;
import main.objects.Customer;
import main.objects.Docking;

import java.util.ArrayList;

/**
 * Class for simulating the trip when a bike is rented
 */
public class Trip {
    private ArrayList<Double> latet = new ArrayList<>();
    private ArrayList<Double> longe = new ArrayList<>();
    private BikeEmulator bikeEmulator;
    private SimulatorRunner simRun;
    private Docking dockEnd;
    private Bike bike;
    private Docking dockStart;
    private Customer customer;
    private int updateNr;

    public Trip(SimulatorRunner simRun, Docking dockStart, Docking dockEnd, Customer cust, Bike bike) {
        this.simRun = simRun;
        this.dockEnd = dockEnd;
        this.dockStart = dockStart;
        customer = cust;
        this.bike = bike;
        updateNr = 0;
        bikeEmulator = new BikeEmulator(dockStart, dockEnd, cust, bike);
        bikeEmulator.createpath();
        latet = bikeEmulator.getLatet();
        longe = bikeEmulator.getLonge();
    }

    /**
     * Makes a receipt of the trip, which wil be sent when the trip is over.
     * @return string with the receipt.
     */
    public String receipt(){
        String r = "\n\n----------------------------------------------------------------Receipt-----------------------------------------------------------------------------\n" +
                "Customer: " + customer.getFirstname() + " " + customer.getSurname() + ", Cardnr: "+ customer.getCardNr()+ "\n" +
                "Docking station start: " + dockStart + "" +
                "Docking station end: " + dockEnd + "" +
                "Bike used: " + bike + "\n" +
                "Paid: 200"+"\n" +
                "----------------------------------------------------------------------------------------------------------------------------------------------------\n";
        return r;
    }

    public Bike getBike() {
        return bike;
    }

    public Docking getDockEnd() {
        return dockEnd;
    }

    /**
     * his method is whats going to happen every time the bike sends an update.
     * The method send nr of km the bike have moved, sets a new charging level, sets the new latitude and longitude and prints the bikeinfo
     * when the path do not have more points the trip wil call bookKeepingEnd, which wil end the trip, and send a receipt.
     */
    public void update() {
        int km = bikeEmulator.kmToPoints(latet.get(updateNr), longe.get(updateNr), latet.get(updateNr + 1), longe.get(updateNr + 1));
        bike.addKm(km);
        simRun.deChargeBikeRun(bike);
        simRun.setLatLong(bike, latet.get(updateNr), longe.get(updateNr));
        simRun.printBikeInfo(bike);
        updateNr++;
        if (updateNr >= latet.size() - 1) {
            simRun.bookKeepingTripEnd(this);
            System.out.println(receipt());
        }
    }
}
