

//@Authors: Annebelle Almås, Mathias Braathen, William Chakroun Jacobsen & Terje Haugum
//@Date submitted: 23.04.2018
//@Summary: The class is a part of the simulation. The class makes an map so we can se the bikes move around in the simulation
//the class uses threads to update the map. The map wil be updated ever minute. The bikes bikes move around as they are supposed to do
//but the markers dosen't update. So the dock wil say it have more bikes then it have, and the charging level wil always show 100.


package main.simulator;
import main.engine.Maps;
import main.objects.Bike;
import main.objects.Docking;
import java.awt.*;
import javax.swing.*;
import javax.swing.JFrame;

/**
 *The class is a part of the simulation. The class makes an map so we can se the bikes move around in the simulation
 *the class uses threads to update the map. The map wil be updated ever minute. The bikes bikes move around as they are supposed to do
 *but the markers dosen't update. So the dock will say it have more bikes then it have, and the charging level wil always show 100
 */
public class Map extends JFrame implements Runnable{
    private JPanel contentPane;
    private Bike[] bikes;
    private Docking[] docks;
    private Maps theMap;
    private Thread t;

    public Map(Bike[] bikes, Docking[] docks){
        this.bikes = bikes;
        this.docks = docks;

        contentPane = new JPanel();
        contentPane.setLayout(null);

        //Make the map
        JPanel mapPane = new JPanel( new BorderLayout());
        mapPane.setBounds(0, 0, 1500, 800);
        contentPane.add(mapPane);
        theMap = new Maps(mapPane,0,0,1500,800);
        theMap.addBikes(this.bikes);
        theMap.addDocks(this.docks);

        //Slutt-------------------------------------------------------------------------------------------------
        setContentPane(contentPane);
        setSize(1500,800);
        setVisible(true);
        t = new Thread(this, "TripRunner");       
        t.start();
    }

    public void setBikes(Bike[] bikes){
           this.bikes = bikes;
    }
    public void setDocks(Docking[] docks){
        this.docks = docks;
    }

    /**
     * Every minute run the method, and move the bikes around on the map
     */
    public void run(){
        while (true){
            try{
                theMap.addDocks(docks);
                theMap.updateBikes(bikes);
                t.sleep(60000);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}

