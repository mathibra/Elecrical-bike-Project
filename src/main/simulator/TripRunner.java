/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@Authors: Annebelle Almås, Mathias Braathen, William Chakroun Jacobsen & Terje Haugum
@Date submitted: 23.04.2018
@Summary: The class is a part of the simulation.
the TripRunner implements runnable, and all the trips runs here. Every trip is put inside the Arraylist tripPool,
and every minute it updates all the trips, when the trip is over the trip wil be remover from the tripPool.

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////**/

package main.simulator;

import java.util.ArrayList;

/**
 * Class for running the trip simulator
 */
public class TripRunner implements Runnable {
    private Thread t;
    private ArrayList<Trip> tripPool;

    public TripRunner() {
        t = new Thread(this, "TripRunner");
        tripPool = new ArrayList<>();
        t.start();
    }

    /**
     * Add a trip to the tripPool.
     * @param trip      Trip object to add
     */
    public void addTrip(Trip trip) {
        tripPool.add(trip);
    }

    /**
     * Remove a trip from the tripPool.
     * @param trip      Trip object to remove
     */
    public void tripDone(Trip trip) {
        tripPool.remove(trip);
    }

    /**
     * Run all trips every minute.
     */
    public void run() {
        while (true) {
            try {
                System.out.println("\nBiks running: " + tripPool.size());
                for (int i = 0; i < tripPool.size(); i++) {
                    tripPool.get(i).update();
                }
                t.sleep(60000);
            } catch (Exception e) {
                System.out.println("Exception" + e);
            }
        }
    }
}
