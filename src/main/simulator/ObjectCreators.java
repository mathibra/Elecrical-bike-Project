/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@Authors: Annebelle Almås, Mathias Braathen, William Chakroun Jacobsen & Terje Haugum
@Date submitted: 23.04.2018
@Summary:

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////**/
package main.simulator;
import main.database.Executor;
import main.objects.Bike;
import main.objects.Customer;
import main.objects.Docking;
import main.objects.Type;

import java.util.ArrayList;

/**
 * Class for creating objects, used when testing program as data is needed
 */
public class ObjectCreators {

    public ObjectCreators(){}

    RandomGen rn = new RandomGen();
    Names nm = new Names();
    BikeEmulator bm = new BikeEmulator();
    private ArrayList<Type> types = new ArrayList<>();
    private ArrayList<Bike> bikes = new ArrayList<>();
    private ArrayList<Docking> docks = new ArrayList<>();
    private ArrayList<Customer> customers = new ArrayList<>();

    /**
     * Method for creating types
     * @return      an arraylist of type objects
     */
    public ArrayList<Type> createType() {
        for(int i = 0; i < nm.typenames.length-1; i++){
            types.add(new Type(nm.typenames[i]));
        }
        return types;
    }

    /**
     * Method for creating docks
     * @return      an arraylist of dock objects
     */
    public ArrayList<Docking> createDocks(){
        for(int i = 1; i< nm.dockingnames.length-1; i++){
            docks.add(new Docking(nm.dockingnames[i],rn.longGenD(),rn.latGenD()));
        }
        return docks;
    }

    /**
     * Method for creating customers
     * @return      an arraylist of customer objects
     */
    public ArrayList<Customer> createCust() {
        for(int i = 1; i<20; i++){
            customers.add(new Customer(nm.firstNameGen(),nm.surnameGen(),rn.cardGen()));
        }
        return customers;
    }

    /**
     * Method for creating bikes
     * @return      an arraylist of bike objects
     */
    private ArrayList<Bike> createBikes(){
        for(int i = 0; i<10; i++){
            bikes.add(new Bike(bm.dateNow(),rn.priceGen(),rn.numGen(Executor.getAllTypes().size())+1, bm.dateNow()));
        }
        return bikes;
    }

    /**
     * For adding bikes to database
     */
    public void addbikesToDatabase(){
        createBikes();
        for(Bike bikes: bikes){
            Executor.newBike(bikes.getReg_date(),bikes.getPrice(),bikes.getType_id(),bikes.getProd_date());
        }
    }

    /**
     * For adding docks to database
     */
    public void addDockingToDatabase(){
        createDocks();
        for(Docking docks: docks){
            Executor.newDocking(docks.getName(), docks.getLatitude(), docks.getLongitude());
        }
    }

    /**
     * For adding customers to database
     */
    public void addCustomerToDatabase(){
        createCust();
        for(Customer customer:customers){
            Executor.newCustomer(customer.getFirstname(),customer.getSurname(),customer.getCardNr());
        }
    }

    /**
     * For adding types to database
     */
    public void addTypesToDatabase(){
        createType();
        for (Type type: types){
            Executor.newType(type.getName());
        }
    }

    /**
     * For adding bikes to database
     */
    public void dockAllBikes(){
        for(Bike bike: Executor.getAllBikes()){
            Executor.dockingBike(bike.getBike_id(),Executor.getAllDockings().get(rn.numGen(Executor.getAllDockings().size())).getDock_id());
        }
    }



}
