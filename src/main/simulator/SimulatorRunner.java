/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@Authors: Annebelle Almås, Mathias Braathen, William Chakroun Jacobsen & Terje Haugum
@Date submitted: 23.04.2018
@Summary: The class is a part of the simulation, and implements runnable which is running a thread.
The simulatorRunner take out info from the database one time, and use
this information further, and update this in arraylists. This class have methods for making objects and update the objects.

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////**/
package main.simulator;
import main.database.Executor;
import main.objects.Bike;
import main.objects.Customer;
import main.objects.Docking;
import main.objects.Repair;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Class for running the simulator
 */
public class SimulatorRunner implements Runnable{
    private RandomGen random = new RandomGen();
    private ArrayList<Bike> allBikes = new ArrayList<>(Executor.getAllBikes());
    private ArrayList<Docking> allDocks = new ArrayList<>(Executor.getAllDockings());
    private ArrayList<Customer> customers = new ArrayList<>();
    private ArrayList<Bike> repairPool  = new ArrayList<>();
    private TripRunner tripRunner;
    private Repair repair = new Repair();
    private Thread t;
    private Map m;

    /**
     * The constructor updates the array with the information from the database.
     * Sets a connection to the triprunner. The triprunner  wil run every trip.
     * Sets up a connection to the map. The map wil run as long as the program does
     * Make at thread
     */
    public SimulatorRunner(){
       setConnectArrays();
       tripRunner = new TripRunner();
       m = new Map(getArrayOfBikes(),getArrayOfDocks());
       t = new Thread(this, "SimulatorRunner");
       t.start();
    }


    /**
     * Constructor used in JUnit testing. The only thing the parameter does is to make the constructors different
     * @param i int to separate the different constructors
     */
    public SimulatorRunner(int i){
       tripRunner = new TripRunner();
   }


    /**
     * Take out the information from the database and puts it into arraylists.
     * An array of all the bikes and an array of all the docks
     */
    private void setConnectArrays() {
        for(Bike dockBike: allBikes){
            dockBike.setDocked(Executor.retriveDock(dockBike.getBike_id()));
        }
        for(Docking dock: allDocks) {
            int dockID = dock.getDock_id();
            for (Bike bike : allBikes) {
                if (bike.getDocked() == dockID) {
                    dock.addBike(bike);
                }
            }
        }
    }

    /**
     * This method is used for JUnit testing. And sets the array for testing
     * @param bike      an arraylist of bike objects
     * @param dock      an arraylist of dock objects
     */
    public void setTestArray(ArrayList<Bike> bike, ArrayList<Docking> dock){
        allBikes = bike;
        allDocks = dock;
        allDocks.get(0).addBike(allBikes.get(0));
        allDocks.get(0).addBike(allBikes.get(1));
        allDocks.get(1).addBike(allBikes.get(2));
    }

    /**
     * Convert an arraylist of bikes to a normal array
     * @return      an array of bike objects
     */
    public Bike[] getArrayOfBikes(){
        Bike[] newArray = new Bike[allBikes.size()];
        for(int i = 0; i < allBikes.size(); i++){
            newArray[i] = allBikes.get(i);
        }
        return newArray;
    }

    /**
     * Convert an arraylist of docks to a normal array
     * @return      an array of dock objects
     */
    public Docking[] getArrayOfDocks(){
        Docking[] newArray = new Docking[allDocks.size()];
        for(int i = 0; i < allDocks.size(); i++){
            newArray[i] = allDocks.get(i);
        }
        return newArray;
    }

    public ArrayList<Docking> getAllDocks() {
        return allDocks;
    }

    /**
     * Thread for running docks, charge the bikes and update array for map.
     * It also check if a bike can go out of repair by checking if it exist bikes in
     * repairPool and check if the chance is true. If it is It take the first bike from the repairpool
     * and put it into a random dock, set the status to parked, call repairDone(), and take it out of the repairpool.
     * The thread runs every minute
     */
    public void run(){
        while(true) {
            try {
                //Updates the map
                m.setBikes(getArrayOfBikes());
                m.setDocks(getArrayOfDocks());
                for (Docking dock: allDocks) {
                    dock.chargeDockedBikes();
                }
                if(random.chance() == true && repairPool.size() != 0){
                    Docking docking = findStartStation();
                    docking.addBike(repairPool.get(0));
                    repairPool.get(0).setStatus(1);
                    repair.repairDone(300,"Repair done", dateNow(),repairPool.get(0).getBike_id());
                    repairPool.remove(repairPool.get(0));
                }
                t.sleep(60000);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public java.sql.Date dateNow(){
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        return date;
    }

    public void printBikeInfo(Bike bike){
        System.out.println("Bikeinfo: "+bike.toString());
    }


    /**
     * Finds a random dock where there are bikes. By using a random generator.
     *It finds a number between the nr of all the docks.
     *It also check if the status is 0 which means it is an active station
     * @return a random docking station
     */
    public Docking findStartStation(){
        Docking start = null;
        int numS;
        int counter = 0;
        while (counter == 0) {
            numS = random.IndexGen(allDocks.size());
            if (allDocks.get(numS).getAntDocked() > 0 && allDocks.get(numS).getStatus() == 0){
                start = allDocks.get(numS);
                counter++;
            }
        }
       return start;
    }

    /**
     * Finds a random dock where their are less then 50 bikes.By using a random generator.
     *The max limit of bikes in each docking station is 50
     *Finds a random dock where there are bikes. By using a random generator.
     *It finds a number between the nr of all the docks.
     *It also check if the status is 0 which means it is an active station.
     * @return a random docking station.
     */
    public Docking findEndStation(){
        Docking end = null;
        int numE;
        int counter = 0;
        while (counter == 0) {
            numE = random.IndexGen(allDocks.size());
            if (allDocks.get(numE).getAntDocked() < 50 && allDocks.get(numE).getStatus() == 0)/*sjekker at det er plass til sykkel der */ {
                end = allDocks.get(numE);
                counter++;
            }
        }
        return end;
    }

    /**
     * Makes a random generated customer
     * @return      the randomly generated customer object
     */
    public Customer makeACustomer(){
        Customer customer = random.customerGen();
        return customer;
    }

    /**
     * ake a payment, in the simulator we choose that all payments wil go through.
     * @return a boolean indicating if the payment went by well or not
     */
    public boolean payment(){
        return true;
    }


    /**
     * Decharge the bike every time the method is called the bike wil be decharged with 1.
     * @param bike      a bike objects to change the charge level of
     */
    public void deChargeBikeRun(Bike bike){
        int deCharge = -1;
        bike.changeChargeLevel(deCharge);
    }

    /**
     * Chooses the bike with the most charge
     * @param docking       dock object to choose from
     * @return bike object with highest charge in docking
     */
    public Bike chooseBike(Docking docking){
        ArrayList<Bike> bikeInDock = new ArrayList<>();
        bikeInDock = docking.sortDocked();
        int mostCharge = -1;
        int bikeindex = -1;
        for(int i = 0; i < bikeInDock.size(); i++){
            if(bikeInDock.get(i).getCharge() > mostCharge && bikeInDock.get(i).getStatus() == 1){
                mostCharge = bikeInDock.get(i).getCharge();
                bikeindex = i;
            }
        }
        return bikeInDock.get(bikeindex);
    }

    /**
     * Update bike status to running(3), and take the bike out from the docking station array list.
     * @param dockStart     dock object where the trip starts
     * @param bike          bike that is to start the trip
     */
    public void bookKeepingTripStart(Docking dockStart, Bike bike){
        // removes the bike from the docking
        bike.setStatus(3);
        dockStart.getBikes().remove(bike);
    }



    /**
     * Update bike at the end of the trip. It calls tripDone which take the bike out of the TripRunner
     * it checks if the bike needs a repair, and if it does the bike status will be set to repair(4).
     *If not the bike will be set back to parked status(1), and the bike are put into the array of the dock it ends on
     * Finally the trip number are put to one more
     * @param trip      trip object to end
     */
    public void bookKeepingTripEnd(Trip trip){
        tripRunner.tripDone(trip);
        //Check if it need repair
        if (random.repairGen(trip.getBike().getBike_id())) {
            trip.getBike().setStatus(2);
            System.out.println("-------------------- Repair needed on bike: " + trip.getBike().getBike_id()+ "--------------------");
            trip.getBike().newRepair();
            repairPool.add(trip.getBike());
        } else {
            // put the bike in the end dock
            trip.getBike().setStatus(1);
            trip.getDockEnd().getBikes().add(trip.getBike());
        }
        trip.getBike().makeATrip();
    }

    /**
     * Puts in latitude and longitude to bikes so we can update the map
     * @param bike      bike object to edit coordinates on
     * @param lati      double representing new latitude
     * @param longi     double representing new longitude
     */
    public void setLatLong(Bike bike, double lati, double longi){
        bike.setLatitude(lati);
        bike.setLongitude(longi);
    }

    /**
     * Makes a trip. Put it into tripRunner where it will be until the trip is done.
     * @param dockStart     Dock object where the trip starts
     * @param dockEnd       Dock object where the trip ends
     * @param cust          Customer object doing the trip
     * @param bike          Bike object being rented by the customer
     */
    public void ExecuteTrip(Docking dockStart, Docking dockEnd, Customer cust, Bike bike){
        bookKeepingTripStart(dockStart,bike);
        Trip trip = new Trip(this, dockStart,dockEnd,cust,bike);
        tripRunner.addTrip(trip);
    }
}
