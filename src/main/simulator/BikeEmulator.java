/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@Authors: Annebelle Almås, Mathias Braathen, William Chakroun Jacobsen & Terje Haugum
@Date submitted: 23.04.2018
@Summary: The class is a part of the simulation. It makes the path, and calculate the length.

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////**/

package main.simulator;
import main.objects.Bike;
import main.objects.Customer;
import main.objects.Docking;
import main.simulator.RandomGen;


import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Class for emulating trups for bike rentals
 */
public class BikeEmulator{
    private ArrayList<Double> latet = new ArrayList<>();
    private ArrayList<Double> longe = new ArrayList<>();
    private Docking dockStart;
    private Docking dockEnd;
    private Customer cust;
    private Bike bike;
    private RandomGen random = new RandomGen();

    /**
     * Constructor for creating an emulator object
     * @param dockStart     Dock object indicating the start dock
     * @param dockEnd       Dock object indicating the end dock
     * @param cust          customer object indicating what customer is renting the bike
     * @param bike          Bike object indicating which bike is being rented
     */
    public BikeEmulator(Docking dockStart, Docking dockEnd, Customer cust, Bike bike){
        this.dockStart = dockStart;
        this.dockEnd = dockEnd;
        this.cust = cust;
        this.bike = bike;
    }

    public BikeEmulator(){
    }
    public Bike getBike(){
        return bike;
    }


    /**
     * Create the path the bike vil take. The path can be up to 20 points(this can change).
     It wil set the arrayList of latitude and longitude, which is used to create a trip.
     */
    protected void createpath(){
        double dockendLat = dockEnd.getLatitude();
        double dockendLon = dockEnd.getLongitude();
        double lat = dockStart.getLatitude();
        double lon = dockStart.getLongitude();
        latet.add(lat);
        longe.add(lon);
        latet.addAll(random.latGen(dockendLat));
        longe.addAll(random.longGen(dockendLon));
    }

    public ArrayList<Double> getLatet() {
        return latet;
    }

    public ArrayList<Double> getLonge() {
        return longe;
    }

    /**
     * Finds km for the trip
     * Is not used because we wanted to send km the bike moved every time it updated.
     * But if we want to set this when the trip is over we can use this method.
     * @return number of km int int
     */
    public int tripInKm(){
        double totDistace = 0;
        double distace = 0;
        double between;
        if(latet.size() == longe.size()) {
            for (int i = 0; i < latet.size(); i++) {
                between = longe.get(i) - longe.get(i + 1);
                distace = Math.sin(deg2rad(latet.get(i))) * Math.sin(deg2rad(latet.get(i + 1))) + Math.cos(deg2rad(latet.get(i))) * Math.cos(deg2rad(latet.get(i + 1))) * Math.cos(deg2rad(between));
                distace = Math.acos(distace);
                distace = rad2deg(distace);
                distace = distace * 1.609344;
                totDistace += distace;
            }
        }
        int theDistance = (int)totDistace;
        return (theDistance);
    }

    /**
     * Finds km between to points and make it into an int.
     * @param latitude1     double indicating start latitude for the trip
     * @param longitude1    double indicating start longitude for the trip
     * @param latitude2     double indicating end latitude for the trip
     * @param longitude2    double indicating end longitude for the trip
     * @return number of km between to coordinate points in int.
     */
    public  int kmToPoints(double latitude1, double longitude1, double latitude2, double longitude2) {
        double between = longitude1 - longitude2;
        double distanceToPoints = Math.sin(deg2rad(latitude1)) * Math.sin(deg2rad(latitude2)) + Math.cos(deg2rad(latitude1)) * Math.cos(deg2rad(latitude2)) * Math.cos(deg2rad(between));
        distanceToPoints = Math.acos(distanceToPoints);
        distanceToPoints = rad2deg(distanceToPoints);
        distanceToPoints = distanceToPoints * 60 * 1.1515 * 1.609344;

        int distanceToPointsInt = (int)distanceToPoints;
        return (distanceToPointsInt);
    }

    /**
     * Calculate from degrees to radians
     * @param deg       double indicating degrees
     * @return radians in double
     */
    private static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }


    /**
     * Calculate from radians to degrees
     * @param rad       double indicating radians
     * @return degrees in double
     */
    private static double rad2deg(double rad) {
        return (rad * 180 / Math.PI);
    }


    public Date dateNow(){
        Date date = new Date(Calendar.getInstance().getTime().getTime());
        return date;
    }
}
