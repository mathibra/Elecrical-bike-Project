/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@Authors: Annebelle Almås, Mathias Braathen, William Chakroun Jacobsen & Terje Haugum
@Date submitted: 23.04.2018
@Summary: The class is a part of the simulation. The class makes names. It is used to create random customers in the class RandomGen.

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////**/

package main.simulator;
import java.sql.Date;
import java.util.Random;

/**
 * Class for generating random names
 */
public class Names {
    private String[] firstnames = {"Tommy", "Jenny", "Sandra", "Nora", "Josefine", "Martin", "Sander", "Magnus", "Sondre", "petter", "Anna"};
    private String[] surnames = {"Jacobsen", "Anderson", "Muhammed", "Nguynen", "Haugum", "Lien", "Hansen", "Karlsen", "Helland"};
    String[] dockingnames = {"Nidarosdomen", "Tyholttårnet", "prinsens kinosenter", "Kongens gate", "elgseter", "gløshaugen", "kalvskinnet", "togstationen", "voll studentby", "Lerkendal", "kvartalet"};
    public String[] typenames = {"racer", "citybike", "BMX", "jumper"};

    public Names() {
    }

    /**
     * Method for generating random first name
     * @return a String containing a random name
     */
    public String firstNameGen() {
        Random x = new Random();
        int fn = x.nextInt(firstnames.length - 1);
        String firstname = firstnames[fn];
        return firstname;
    }

    /**
     * Method for generating random surname
     * @return a String containing a random surname
     */
    public String surnameGen() {
        Random x = new Random();
        int sn = x.nextInt(surnames.length - 1);
        String surname = surnames[sn];
        return surname;
    }
}