/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@Authors: Annebelle Almås, Mathias Braathen, William Chakroun Jacobsen & Terje Haugum

@Date submitted: 23.04.2018

@Summary: The class is a part of the simulation. This is the main class, the class that runs the simulation.
It wil make an object of Customer, To Docking on the bike starts at and one it ends at and a bike at the start dock
After making the objects it wil call the method ExecuteTrip in simulatorRunner to start the trip.

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////**/
package main.simulator;
import main.objects.Bike;
import main.objects.Customer;
import main.objects.Docking;

/**
 * main.ui.gui_admin.Main class for running simulation
 */
public class MainSimulator {
    public static void main(String[] args){
        //Make a SimulatorRunner object
        SimulatorRunner simRun = new SimulatorRunner();
        //It wil make a new trip every 3 minute
        while (true){
            try {
                Customer customer = simRun.makeACustomer();
                if (simRun.payment()) {
                    Docking startDock = simRun.findStartStation();
                    Docking endDock = simRun.findEndStation();
                    Bike bike = simRun.chooseBike(startDock);
                    System.out.println("\n\n----------------------------------------------------------------New Rent----------------------------------------------------------------");
                    System.out.println("Customer: " + customer.getFirstname() + " "+ customer.getSurname());
                    System.out.println("StartDock: " + startDock.toString() + "Bike: " + bike.toString());
                    System.out.println("----------------------------------------------------------------------------------------------------------------------------------------\n");
                    simRun.ExecuteTrip(startDock, endDock, customer, bike);
                }
                Thread.sleep(180000);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
