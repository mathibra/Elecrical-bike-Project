
UPDATE Bike set all_repairs = 1 where type_id = 1;
UPDATE Bike set all_repairs = 2 where type_id = 2;
UPDATE Bike set all_repairs = 3 where type_id = 3;
INSERT INTO mathibra.Type(type_id, name) VALUES (1,'City_bike');
INSERT INTO mathibra.Type(type_id, name) VALUES (2,'Terrain');
INSERT INTO mathibra.Type(type_id, name) VALUES (3,'BMX');
INSERT INTO mathibra.Type(type_id, name) VALUES (4,'Speedster');
INSERT INTO mathibra.Type(type_id, name) VALUES (4,'vintage');

INSERT INTO mathibra.Bike (bike_id, date_registered, price, charge, produced, all_repairs, total_trips, total_km, type_id, status) VALUES (1,'2001-01-01',2000,98,'2000-10-10',2,100,310,1,3);

INSERT INTO mathibra.Docking_station (dock_id, name, latitude, longitude, power_usage) VALUES (1,'one' ,123.2,12.123,10.23);
INSERT INTO mathibra.Bike_docked(dock_id, bike_id, date_delivered) VALUES (1,2,'');

INSERT INTO mathibra.Bike_status(bike_status_id, latitude, longitude, charging_level, date, bike_id) VALUES (1,2.1,1,1,'',1);
INSERT INTO mathibra.banned_Customers(customer_id, reason) VALUES (1,'s');
INSERT INTO mathibra.User_has_bike(customer_id, bike_id, date_rented, start_dock) VALUES (1,1,'d',1);

INSERT INTO mathibra.Docking_station_history(date, name, power_usage, dock_id) VALUES (1,'',1,1);
INSERT INTO Repairs(repair_id, repair_request, date_sendt, price, repair_description, date_received, bike_id) VALUES (1, 'test', '2018-01-01', 200, 'test', '018-04-01', 1);
INSERT INTO Repairs VALUES (2, 'test', '2018-01-01', 500, 'test', '2018-05-01', 1);
INSERT INTO Repairs VALUES (3, 'test', '2018-01-01', 300, 'test', '2018-06-01', 1);

INSERT INTO User_has_bike VALUES (1, 1, '2018-05-01', 1);
INSERT INTO User_has_bike VALUES (1, 2, '2018-05-02', 1);
INSERT INTO User_has_bike VALUES (1, 3, '2018-05-03', 1);

INSERT INTO User_has_bike VALUES (1, 4, '2018-06-01', 1);
INSERT INTO User_has_bike VALUES (1, 5, '2018-06-02', 1);
INSERT INTO User_has_bike VALUES (1, 6, '2018-06-03', 1);
INSERT INTO User_has_bike VALUES (1, 7, '2018-06-04', 1);

INSERT INTO mathibra.Bike_docked (dock_id, bike_id, date_delivered) VALUES (1,511,'2018-04-23');

UPDATE Bike SET status = 1 WHERE bike_id = 513;
