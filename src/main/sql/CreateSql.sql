DROP TABLE IF EXISTS Docking_station_history;
DROP TABLE IF EXISTS Bike_docked;
DROP TABLE IF EXISTS Bike_History;
DROP TABLE IF EXISTS Admin;
DROP TABLE IF EXISTS User_has_bike;
DROP TABLE IF EXISTS Repairs;
DROP TABLE IF EXISTS Customer;
DROP TABLE IF EXISTS Bike;
DROP TABLE IF EXISTS Type;
DROP TABLE IF EXISTS Docking_station;


CREATE TABLE Type(
  type_id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(30) UNIQUE,
  status INT NOT NULL,
  CONSTRAINT type_pk PRIMARY KEY(type_id)
);

CREATE TABLE Bike(
  bike_id INT NOT NULL AUTO_INCREMENT,
  date_registered DATE,
  price INT,
  charge INT,
  produced DATE,
  all_repairs INT,
  total_trips INT,
  total_km INT,
  type_id INT NOT NULL,
  status INT NOT NULL,
  PRIMARY KEY(bike_id),
  FOREIGN KEY (type_id) REFERENCES Type(type_id));

CREATE TABLE Docking_station(
  dock_id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(30),
  latitude DOUBLE,
  longitude DOUBLE,
  status INT NOT NULL,
  CONSTRAINT docking_station_pk PRIMARY KEY(dock_id));


CREATE TABLE Repairs(
  repair_id INT NOT NULL AUTO_INCREMENT,
  repair_request VARCHAR(1000),
  date_sendt DATETIME,
  price INT,
  repair_description VARCHAR(1000),
  date_received DATETIME,
  bike_id INT,
  CONSTRAINT repair_pk PRIMARY KEY(repair_id),
  CONSTRAINT bike_id_fk FOREIGN KEY(bike_id) REFERENCES Bike(bike_id));

CREATE TABLE Customer(
  customer_id INT NOT NULL AUTO_INCREMENT,
  firstname VARCHAR(30),
  surname VARCHAR(30),
  card_number LONG,
  CONSTRAINT customer_pk PRIMARY KEY(customer_id)
);

CREATE TABLE User_has_bike(
  customer_id INT NOT NULL,
  bike_id INT NOT NULL,
  date_rented DATE NOT NULL,
  start_dock INT,
  CONSTRAINT user_has_bike_pk1 PRIMARY KEY (bike_id),
  CONSTRAINT user_has_bike_fk1 FOREIGN KEY (customer_id) REFERENCES Customer(customer_id),
  CONSTRAINT user_has_bike_fk2 FOREIGN KEY (bike_id) REFERENCES Bike(bike_id),
  CONSTRAINT user_has_bike_fk3 FOREIGN KEY (start_dock) REFERENCES  Docking_station(dock_id)
);

CREATE TABLE Admin(
  admin_id INT NOT NULL AUTO_INCREMENT,
  email VARCHAR(50) NOT NULL UNIQUE,
  password TEXT(60)NOT NULL,
  CONSTRAINT user_pk PRIMARY KEY(admin_id)
);


CREATE TABLE Bike_History(
  latitude DOUBLE,
  longitude DOUBLE,
  charging_level INT,
  date TIMESTAMP,
  bike_id INT,
  CONSTRAINT bike_status_pk PRIMARY KEY(bike_id,date),
  CONSTRAINT bike_fk FOREIGN KEY (bike_id) REFERENCES Bike(bike_id)
);

CREATE TABLE Bike_docked(
  dock_id INT NOT NULL,
  bike_id INT NOT NULL,
  date_delivered DATE,
  CONSTRAINT bike_docked_pk1 PRIMARY KEY(bike_id),
  CONSTRAINT bike_docked_fk1 FOREIGN KEY(dock_id) REFERENCES Docking_station(dock_id),
  CONSTRAINT bike_docked_fk2 FOREIGN KEY(bike_id) REFERENCES Bike(bike_id)
);

CREATE TABLE Docking_station_history(
  date TIMESTAMP,
  name VARCHAR(30),
  power_usage INT,
  dock_id INT NOT NULL,
  CONSTRAINT docking_station_history_pk PRIMARY KEY(dock_id,date),
  CONSTRAINT docking_station_history_fk FOREIGN KEY(dock_id) REFERENCES Docking_station(dock_id)
);
